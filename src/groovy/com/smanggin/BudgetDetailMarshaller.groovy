package com.smanggin
import grails.converters.JSON


/*
ini class untuk modify output JSON 
ketika class ini dipanggil "as JSON"
*/

class BudgetDetailMarshaller {
 
    void register() {
        JSON.registerObjectMarshaller( BudgetDetail ) { BudgetDetail it ->
            return [
				id : it.id,
				
				categoryAssetId:it.categoryAsset?.id, 
				categoryAssetName:it.categoryAsset?.name,

				budgetId:it.budget?.id,
				budgetNumber:it.budget?.number,
				coaId:it.coa?.id,
				coaCode: it.coa?.code,
				coaDescription : it.coa?.description,

				qty:it.qty,
				uom:it.uom,
				value:it.value,
				dateCreated : it.dateCreated,
				lastUpdated : it.lastUpdated,
				
			]
        }
    }
}