package com.smanggin
import grails.converters.JSON


/*
ini class untuk modify output JSON 
ketika class ini dipanggil "as JSON"
*/

class BudgetMarshaller {
 
    void register() {
        JSON.registerObjectMarshaller( Budget ) { Budget it ->
            return [
				id : it.id,
				number:it.number,
				dateCreated : it.dateCreated,
				lastUpdated : it.lastUpdated,
				
			]
        }
    }
}