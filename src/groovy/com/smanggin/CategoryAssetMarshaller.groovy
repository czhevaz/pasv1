package com.smanggin
import grails.converters.JSON


/*
ini class untuk modify output JSON 
ketika class ini dipanggil "as JSON"
*/

class CategoryAssetMarshaller {
 
    void register() {
        JSON.registerObjectMarshaller( CategoryAsset ) { CategoryAsset it ->
            return [
            	id:it.id,
				name:it.name,
				description:it.description,
				categoryAsset:it.categoryAsset,
				version     : it.version?:0,
			]
        }
    }
}
