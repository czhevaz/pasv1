package com.smanggin
import grails.converters.JSON


/*
ini class untuk modify output JSON 
ketika class ini dipanggil "as JSON"
*/

class FAPDetailMarshaller {
 
    void register() {
        JSON.registerObjectMarshaller( FAPDetail ) { FAPDetail it ->
            return [
				id : it.id,
				
				
				dateCreated:it.dateCreated,
				lastUpdated:it.lastUpdated,
	

				createdBy :it.createdBy, 
				updatedBy :it.updatedBy,
	
				fapId :it.fap?.id,
				fapNumber : it.fap?.number,
				budgetDetailId:it.budgetDetail?.id,
				budgetNumber:it.budgetDetail?.budget?.number,
				categoryAssetName:it.budgetDetail?.categoryAsset?.name,

				qty :it.qty,
				unitprice :it.unitprice,
				total :it.total,
				reasonOfInvestment:it.reasonOfInvestment,
				reasonOfReplacement:it.reasonOfReplacement, // Broken / Mutation 
				oldFAPId:it.oldFAP?.id,
				oldFAPnumber:it.oldFAP?.number,
				newCareTakerId: it.newCareTaker?.id,
				newCareTakerName: it.newCareTaker?.name,
				remarks :it.remarks,
				
			]
        }
    }
}