package com.smanggin



import org.junit.*
import grails.test.mixin.*

/**
 * InformationPeriodControllerTests
 * A unit test class is used to test individual methods or blocks of code without considering the surrounding infrastructure
 */
@TestFor(InformationPeriodController)
@Mock(InformationPeriod)
class InformationPeriodControllerTests {


    def populateValidParams(params) {
      assert params != null
      // TODO: Populate valid properties like...
      //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/informationPeriod/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.informationPeriodInstanceList.size() == 0
        assert model.informationPeriodInstanceTotal == 0
    }

    void testCreate() {
       def model = controller.create()

       assert model.informationPeriodInstance != null
    }

    void testSave() {
        controller.save()

        assert model.informationPeriodInstance != null
        assert view == '/informationPeriod/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/informationPeriod/show/1'
        assert controller.flash.message != null
        assert InformationPeriod.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/informationPeriod/list'


        populateValidParams(params)
        def informationPeriod = new InformationPeriod(params)

        assert informationPeriod.save() != null

        params.id = informationPeriod.id

        def model = controller.show()

        assert model.informationPeriodInstance == informationPeriod
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/informationPeriod/list'


        populateValidParams(params)
        def informationPeriod = new InformationPeriod(params)

        assert informationPeriod.save() != null

        params.id = informationPeriod.id

        def model = controller.edit()

        assert model.informationPeriodInstance == informationPeriod
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/informationPeriod/list'

        response.reset()


        populateValidParams(params)
        def informationPeriod = new InformationPeriod(params)

        assert informationPeriod.save() != null

        // test invalid parameters in update
        params.id = informationPeriod.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/informationPeriod/edit"
        assert model.informationPeriodInstance != null

        informationPeriod.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/informationPeriod/show/$informationPeriod.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        informationPeriod.clearErrors()

        populateValidParams(params)
        params.id = informationPeriod.id
        params.version = -1
        controller.update()

        assert view == "/informationPeriod/edit"
        assert model.informationPeriodInstance != null
        assert model.informationPeriodInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/informationPeriod/list'

        response.reset()

        populateValidParams(params)
        def informationPeriod = new InformationPeriod(params)

        assert informationPeriod.save() != null
        assert InformationPeriod.count() == 1

        params.id = informationPeriod.id

        controller.delete()

        assert InformationPeriod.count() == 0
        assert InformationPeriod.get(informationPeriod.id) == null
        assert response.redirectedUrl == '/informationPeriod/list'
    }
}
