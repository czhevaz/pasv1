package com.smanggin



import org.junit.*
import grails.test.mixin.*

/**
 * BudgetDetailControllerTests
 * A unit test class is used to test individual methods or blocks of code without considering the surrounding infrastructure
 */
@TestFor(BudgetDetailController)
@Mock(BudgetDetail)
class BudgetDetailControllerTests {


    def populateValidParams(params) {
      assert params != null
      // TODO: Populate valid properties like...
      //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/budgetDetail/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.budgetDetailInstanceList.size() == 0
        assert model.budgetDetailInstanceTotal == 0
    }

    void testCreate() {
       def model = controller.create()

       assert model.budgetDetailInstance != null
    }

    void testSave() {
        controller.save()

        assert model.budgetDetailInstance != null
        assert view == '/budgetDetail/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/budgetDetail/show/1'
        assert controller.flash.message != null
        assert BudgetDetail.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/budgetDetail/list'


        populateValidParams(params)
        def budgetDetail = new BudgetDetail(params)

        assert budgetDetail.save() != null

        params.id = budgetDetail.id

        def model = controller.show()

        assert model.budgetDetailInstance == budgetDetail
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/budgetDetail/list'


        populateValidParams(params)
        def budgetDetail = new BudgetDetail(params)

        assert budgetDetail.save() != null

        params.id = budgetDetail.id

        def model = controller.edit()

        assert model.budgetDetailInstance == budgetDetail
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/budgetDetail/list'

        response.reset()


        populateValidParams(params)
        def budgetDetail = new BudgetDetail(params)

        assert budgetDetail.save() != null

        // test invalid parameters in update
        params.id = budgetDetail.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/budgetDetail/edit"
        assert model.budgetDetailInstance != null

        budgetDetail.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/budgetDetail/show/$budgetDetail.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        budgetDetail.clearErrors()

        populateValidParams(params)
        params.id = budgetDetail.id
        params.version = -1
        controller.update()

        assert view == "/budgetDetail/edit"
        assert model.budgetDetailInstance != null
        assert model.budgetDetailInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/budgetDetail/list'

        response.reset()

        populateValidParams(params)
        def budgetDetail = new BudgetDetail(params)

        assert budgetDetail.save() != null
        assert BudgetDetail.count() == 1

        params.id = budgetDetail.id

        controller.delete()

        assert BudgetDetail.count() == 0
        assert BudgetDetail.get(budgetDetail.id) == null
        assert response.redirectedUrl == '/budgetDetail/list'
    }
}
