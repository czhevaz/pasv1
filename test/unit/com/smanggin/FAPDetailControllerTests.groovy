package com.smanggin



import org.junit.*
import grails.test.mixin.*

/**
 * FAPDetailControllerTests
 * A unit test class is used to test individual methods or blocks of code without considering the surrounding infrastructure
 */
@TestFor(FAPDetailController)
@Mock(FAPDetail)
class FAPDetailControllerTests {


    def populateValidParams(params) {
      assert params != null
      // TODO: Populate valid properties like...
      //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/FAPDetail/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.FAPDetailInstanceList.size() == 0
        assert model.FAPDetailInstanceTotal == 0
    }

    void testCreate() {
       def model = controller.create()

       assert model.FAPDetailInstance != null
    }

    void testSave() {
        controller.save()

        assert model.FAPDetailInstance != null
        assert view == '/FAPDetail/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/FAPDetail/show/1'
        assert controller.flash.message != null
        assert FAPDetail.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/FAPDetail/list'


        populateValidParams(params)
        def FAPDetail = new FAPDetail(params)

        assert FAPDetail.save() != null

        params.id = FAPDetail.id

        def model = controller.show()

        assert model.FAPDetailInstance == FAPDetail
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/FAPDetail/list'


        populateValidParams(params)
        def FAPDetail = new FAPDetail(params)

        assert FAPDetail.save() != null

        params.id = FAPDetail.id

        def model = controller.edit()

        assert model.FAPDetailInstance == FAPDetail
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/FAPDetail/list'

        response.reset()


        populateValidParams(params)
        def FAPDetail = new FAPDetail(params)

        assert FAPDetail.save() != null

        // test invalid parameters in update
        params.id = FAPDetail.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/FAPDetail/edit"
        assert model.FAPDetailInstance != null

        FAPDetail.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/FAPDetail/show/$FAPDetail.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        FAPDetail.clearErrors()

        populateValidParams(params)
        params.id = FAPDetail.id
        params.version = -1
        controller.update()

        assert view == "/FAPDetail/edit"
        assert model.FAPDetailInstance != null
        assert model.FAPDetailInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/FAPDetail/list'

        response.reset()

        populateValidParams(params)
        def FAPDetail = new FAPDetail(params)

        assert FAPDetail.save() != null
        assert FAPDetail.count() == 1

        params.id = FAPDetail.id

        controller.delete()

        assert FAPDetail.count() == 0
        assert FAPDetail.get(FAPDetail.id) == null
        assert response.redirectedUrl == '/FAPDetail/list'
    }
}
