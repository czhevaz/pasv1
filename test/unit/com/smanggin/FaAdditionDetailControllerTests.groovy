package com.smanggin



import org.junit.*
import grails.test.mixin.*

/**
 * FaAdditionDetailControllerTests
 * A unit test class is used to test individual methods or blocks of code without considering the surrounding infrastructure
 */
@TestFor(FaAdditionDetailController)
@Mock(FaAdditionDetail)
class FaAdditionDetailControllerTests {


    def populateValidParams(params) {
      assert params != null
      // TODO: Populate valid properties like...
      //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/faAdditionDetail/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.faAdditionDetailInstanceList.size() == 0
        assert model.faAdditionDetailInstanceTotal == 0
    }

    void testCreate() {
       def model = controller.create()

       assert model.faAdditionDetailInstance != null
    }

    void testSave() {
        controller.save()

        assert model.faAdditionDetailInstance != null
        assert view == '/faAdditionDetail/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/faAdditionDetail/show/1'
        assert controller.flash.message != null
        assert FaAdditionDetail.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/faAdditionDetail/list'


        populateValidParams(params)
        def faAdditionDetail = new FaAdditionDetail(params)

        assert faAdditionDetail.save() != null

        params.id = faAdditionDetail.id

        def model = controller.show()

        assert model.faAdditionDetailInstance == faAdditionDetail
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/faAdditionDetail/list'


        populateValidParams(params)
        def faAdditionDetail = new FaAdditionDetail(params)

        assert faAdditionDetail.save() != null

        params.id = faAdditionDetail.id

        def model = controller.edit()

        assert model.faAdditionDetailInstance == faAdditionDetail
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/faAdditionDetail/list'

        response.reset()


        populateValidParams(params)
        def faAdditionDetail = new FaAdditionDetail(params)

        assert faAdditionDetail.save() != null

        // test invalid parameters in update
        params.id = faAdditionDetail.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/faAdditionDetail/edit"
        assert model.faAdditionDetailInstance != null

        faAdditionDetail.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/faAdditionDetail/show/$faAdditionDetail.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        faAdditionDetail.clearErrors()

        populateValidParams(params)
        params.id = faAdditionDetail.id
        params.version = -1
        controller.update()

        assert view == "/faAdditionDetail/edit"
        assert model.faAdditionDetailInstance != null
        assert model.faAdditionDetailInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/faAdditionDetail/list'

        response.reset()

        populateValidParams(params)
        def faAdditionDetail = new FaAdditionDetail(params)

        assert faAdditionDetail.save() != null
        assert FaAdditionDetail.count() == 1

        params.id = faAdditionDetail.id

        controller.delete()

        assert FaAdditionDetail.count() == 0
        assert FaAdditionDetail.get(faAdditionDetail.id) == null
        assert response.redirectedUrl == '/faAdditionDetail/list'
    }
}
