package com.smanggin



import org.junit.*
import grails.test.mixin.*

/**
 * CategoryAssetControllerTests
 * A unit test class is used to test individual methods or blocks of code without considering the surrounding infrastructure
 */
@TestFor(CategoryAssetController)
@Mock(CategoryAsset)
class CategoryAssetControllerTests {


    def populateValidParams(params) {
      assert params != null
      // TODO: Populate valid properties like...
      //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/categoryAsset/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.categoryAssetInstanceList.size() == 0
        assert model.categoryAssetInstanceTotal == 0
    }

    void testCreate() {
       def model = controller.create()

       assert model.categoryAssetInstance != null
    }

    void testSave() {
        controller.save()

        assert model.categoryAssetInstance != null
        assert view == '/categoryAsset/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/categoryAsset/show/1'
        assert controller.flash.message != null
        assert CategoryAsset.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/categoryAsset/list'


        populateValidParams(params)
        def categoryAsset = new CategoryAsset(params)

        assert categoryAsset.save() != null

        params.id = categoryAsset.id

        def model = controller.show()

        assert model.categoryAssetInstance == categoryAsset
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/categoryAsset/list'


        populateValidParams(params)
        def categoryAsset = new CategoryAsset(params)

        assert categoryAsset.save() != null

        params.id = categoryAsset.id

        def model = controller.edit()

        assert model.categoryAssetInstance == categoryAsset
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/categoryAsset/list'

        response.reset()


        populateValidParams(params)
        def categoryAsset = new CategoryAsset(params)

        assert categoryAsset.save() != null

        // test invalid parameters in update
        params.id = categoryAsset.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/categoryAsset/edit"
        assert model.categoryAssetInstance != null

        categoryAsset.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/categoryAsset/show/$categoryAsset.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        categoryAsset.clearErrors()

        populateValidParams(params)
        params.id = categoryAsset.id
        params.version = -1
        controller.update()

        assert view == "/categoryAsset/edit"
        assert model.categoryAssetInstance != null
        assert model.categoryAssetInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/categoryAsset/list'

        response.reset()

        populateValidParams(params)
        def categoryAsset = new CategoryAsset(params)

        assert categoryAsset.save() != null
        assert CategoryAsset.count() == 1

        params.id = categoryAsset.id

        controller.delete()

        assert CategoryAsset.count() == 0
        assert CategoryAsset.get(categoryAsset.id) == null
        assert response.redirectedUrl == '/categoryAsset/list'
    }
}
