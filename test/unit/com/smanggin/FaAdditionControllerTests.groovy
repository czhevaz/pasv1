package com.smanggin



import org.junit.*
import grails.test.mixin.*

/**
 * FaAdditionControllerTests
 * A unit test class is used to test individual methods or blocks of code without considering the surrounding infrastructure
 */
@TestFor(FaAdditionController)
@Mock(FaAddition)
class FaAdditionControllerTests {


    def populateValidParams(params) {
      assert params != null
      // TODO: Populate valid properties like...
      //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/faAddition/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.faAdditionInstanceList.size() == 0
        assert model.faAdditionInstanceTotal == 0
    }

    void testCreate() {
       def model = controller.create()

       assert model.faAdditionInstance != null
    }

    void testSave() {
        controller.save()

        assert model.faAdditionInstance != null
        assert view == '/faAddition/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/faAddition/show/1'
        assert controller.flash.message != null
        assert FaAddition.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/faAddition/list'


        populateValidParams(params)
        def faAddition = new FaAddition(params)

        assert faAddition.save() != null

        params.id = faAddition.id

        def model = controller.show()

        assert model.faAdditionInstance == faAddition
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/faAddition/list'


        populateValidParams(params)
        def faAddition = new FaAddition(params)

        assert faAddition.save() != null

        params.id = faAddition.id

        def model = controller.edit()

        assert model.faAdditionInstance == faAddition
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/faAddition/list'

        response.reset()


        populateValidParams(params)
        def faAddition = new FaAddition(params)

        assert faAddition.save() != null

        // test invalid parameters in update
        params.id = faAddition.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/faAddition/edit"
        assert model.faAdditionInstance != null

        faAddition.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/faAddition/show/$faAddition.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        faAddition.clearErrors()

        populateValidParams(params)
        params.id = faAddition.id
        params.version = -1
        controller.update()

        assert view == "/faAddition/edit"
        assert model.faAdditionInstance != null
        assert model.faAdditionInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/faAddition/list'

        response.reset()

        populateValidParams(params)
        def faAddition = new FaAddition(params)

        assert faAddition.save() != null
        assert FaAddition.count() == 1

        params.id = faAddition.id

        controller.delete()

        assert FaAddition.count() == 0
        assert FaAddition.get(faAddition.id) == null
        assert response.redirectedUrl == '/faAddition/list'
    }
}
