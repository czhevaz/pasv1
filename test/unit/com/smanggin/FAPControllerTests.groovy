package com.smanggin



import org.junit.*
import grails.test.mixin.*

/**
 * FAPControllerTests
 * A unit test class is used to test individual methods or blocks of code without considering the surrounding infrastructure
 */
@TestFor(FAPController)
@Mock(FAP)
class FAPControllerTests {


    def populateValidParams(params) {
      assert params != null
      // TODO: Populate valid properties like...
      //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/FAP/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.FAPInstanceList.size() == 0
        assert model.FAPInstanceTotal == 0
    }

    void testCreate() {
       def model = controller.create()

       assert model.FAPInstance != null
    }

    void testSave() {
        controller.save()

        assert model.FAPInstance != null
        assert view == '/FAP/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/FAP/show/1'
        assert controller.flash.message != null
        assert FAP.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/FAP/list'


        populateValidParams(params)
        def FAP = new FAP(params)

        assert FAP.save() != null

        params.id = FAP.id

        def model = controller.show()

        assert model.FAPInstance == FAP
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/FAP/list'


        populateValidParams(params)
        def FAP = new FAP(params)

        assert FAP.save() != null

        params.id = FAP.id

        def model = controller.edit()

        assert model.FAPInstance == FAP
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/FAP/list'

        response.reset()


        populateValidParams(params)
        def FAP = new FAP(params)

        assert FAP.save() != null

        // test invalid parameters in update
        params.id = FAP.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/FAP/edit"
        assert model.FAPInstance != null

        FAP.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/FAP/show/$FAP.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        FAP.clearErrors()

        populateValidParams(params)
        params.id = FAP.id
        params.version = -1
        controller.update()

        assert view == "/FAP/edit"
        assert model.FAPInstance != null
        assert model.FAPInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/FAP/list'

        response.reset()

        populateValidParams(params)
        def FAP = new FAP(params)

        assert FAP.save() != null
        assert FAP.count() == 1

        params.id = FAP.id

        controller.delete()

        assert FAP.count() == 0
        assert FAP.get(FAP.id) == null
        assert response.redirectedUrl == '/FAP/list'
    }
}
