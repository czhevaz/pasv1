package com.smanggin



import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

/**
 * DepartmentController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */

class DepartmentController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        def results = Department.createCriteria().list(params){}
        [departmentInstanceList: results, departmentInstanceTotal: results.totalCount]
    }

    def create() {
        [departmentInstance: new Department(params)]
    }

    def save() {
        def departmentInstance = new Department(params)
        departmentInstance.createdBy = session.user
        if (!departmentInstance.save(flush: true)) {
            render(view: "create", model: [departmentInstance: departmentInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: [message(code: 'department.label', default: 'Department'), departmentInstance.id])
        redirect(action: "show", id: departmentInstance.id)
    }

    def show() {
        def departmentInstance = Department.get(params.id)

        if (!departmentInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'department.label', default: 'Department'), params.id])
            redirect(action: "list")
            return
        }

        [departmentInstance: departmentInstance]
    }

    def edit() {
        def departmentInstance = Department.get(params.id)
        if (!departmentInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'department.label', default: 'Department'), params.id])
            redirect(action: "list")
            return
        }

        [departmentInstance: departmentInstance]
    }

    def update() {
        def departmentInstance = Department.get(params.id)
        if (!departmentInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'department.label', default: 'Department'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (departmentInstance.version > version) {
                departmentInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'department.label', default: 'Department')] as Object[],
                          "Another user has updated this Department while you were editing")
                render(view: "edit", model: [departmentInstance: departmentInstance])
                return
            }
        }

        departmentInstance.properties = params

        if (!departmentInstance.save(flush: true)) {
            render(view: "edit", model: [departmentInstance: departmentInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: [message(code: 'department.label', default: 'Department'), departmentInstance.id])
        redirect(action: "show", id: departmentInstance.id)
    }

    def delete() {
        def departmentInstance = Department.get(params.id)
        if (!departmentInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'department.label', default: 'Department'), params.id])
            redirect(action: "list")
            return
        }

        try {
            departmentInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'department.label', default: 'Department'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'department.label', default: 'Department'), params.id])
            redirect(action: "show", id: params.id)
        }
    }

    def jsave() {
        def departmentInstance = (params.id) ? Department.get(params.id) : new Department()
        
        if (!departmentInstance) {                     
            def error = [message: message(code: 'default.not.found.message', args: [message(code: 'department.label', default: 'Department'), params.id])]
            render([success: false, messages: [errors:[error]] ] as JSON)       
            return
        }
        
        if (params.version)
        {
            def version = params.version.toLong()
            if (version != null) {
                if (departmentInstance.version > version) {
                    departmentInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                              [message(code: 'department.label', default: 'Department')] as Object[],
                              "Another user has updated this Department while you were editing")
                    render([success: false, messages: departmentInstance.errors] as JSON)
                    return
                }
            }            
        }
        
        departmentInstance.properties = params
                       
        if (!departmentInstance.save(flush: true)) {
            render([success: false, messages: departmentInstance.errors] as JSON)
            return
        }
                        
        render([success: true] as JSON)
    }

    def jlist() {
        if(params.masterField){
            def c = Department.createCriteria()
            def results = c.list {
                eq(params.masterField.name+'.id',params.masterField.id.toLong())    
            }
            render results as JSON

        }
        else
        {
            params.max = Math.min(params.max ? params.int('max') : 10, 100)
            render Department.list(params) as JSON           
        }
        
    }   

    def jdelete(Long id) {
        def departmentInstance = Department.get(id)
        if (!departmentInstance)
            render([success: false] as JSON)
        else {
            try {
                departmentInstance.delete(flush: true)             
                render([success: true] as JSON)
            }catch (DataIntegrityViolationException e) {
                render([success: false, error: e.message] as JSON)
            }
        }
    }

    def jshow = {
        def departmentInstance = Department.get(params.id)
        if (!departmentInstance) {
            render(
                message : "department.not.found",
            ) as JSON

        }
        else {
            render([departmentInstance : departmentInstance ] as JSON)
        }
    }
}
