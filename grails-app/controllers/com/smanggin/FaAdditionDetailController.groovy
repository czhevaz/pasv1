package com.smanggin



import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

/**
 * FaAdditionDetailController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */

class FaAdditionDetailController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        def results = FaAdditionDetail.createCriteria().list(params){}
        [faAdditionDetailInstanceList: results, faAdditionDetailInstanceTotal: results.totalCount]
    }

    def create() {
        [faAdditionDetailInstance: new FaAdditionDetail(params)]
    }

    def save() {
        def faAdditionDetailInstance = new FaAdditionDetail(params)
        if (!faAdditionDetailInstance.save(flush: true)) {
            render(view: "create", model: [faAdditionDetailInstance: faAdditionDetailInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: [message(code: 'faAdditionDetail.label', default: 'FaAdditionDetail'), faAdditionDetailInstance.id])
        redirect(action: "show", id: faAdditionDetailInstance.id)
    }

    def show() {
        def faAdditionDetailInstance = FaAdditionDetail.get(params.id)
        if (!faAdditionDetailInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'faAdditionDetail.label', default: 'FaAdditionDetail'), params.id])
            redirect(action: "list")
            return
        }

        [faAdditionDetailInstance: faAdditionDetailInstance]
    }

    def edit() {
        def faAdditionDetailInstance = FaAdditionDetail.get(params.id)
        if (!faAdditionDetailInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'faAdditionDetail.label', default: 'FaAdditionDetail'), params.id])
            redirect(action: "list")
            return
        }

        [faAdditionDetailInstance: faAdditionDetailInstance]
    }

    def update() {
        def faAdditionDetailInstance = FaAdditionDetail.get(params.id)
        if (!faAdditionDetailInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'faAdditionDetail.label', default: 'FaAdditionDetail'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (faAdditionDetailInstance.version > version) {
                faAdditionDetailInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'faAdditionDetail.label', default: 'FaAdditionDetail')] as Object[],
                          "Another user has updated this FaAdditionDetail while you were editing")
                render(view: "edit", model: [faAdditionDetailInstance: faAdditionDetailInstance])
                return
            }
        }

        faAdditionDetailInstance.properties = params

        if (!faAdditionDetailInstance.save(flush: true)) {
            render(view: "edit", model: [faAdditionDetailInstance: faAdditionDetailInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: [message(code: 'faAdditionDetail.label', default: 'FaAdditionDetail'), faAdditionDetailInstance.id])
        redirect(action: "show", id: faAdditionDetailInstance.id)
    }

    def delete() {
        def faAdditionDetailInstance = FaAdditionDetail.get(params.id)
        if (!faAdditionDetailInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'faAdditionDetail.label', default: 'FaAdditionDetail'), params.id])
            redirect(action: "list")
            return
        }

        try {
            faAdditionDetailInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'faAdditionDetail.label', default: 'FaAdditionDetail'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'faAdditionDetail.label', default: 'FaAdditionDetail'), params.id])
            redirect(action: "show", id: params.id)
        }
    }

    def jsave() {
        def faAdditionDetailInstance = (params.id) ? FaAdditionDetail.get(params.id) : new FaAdditionDetail()
        
        if (!faAdditionDetailInstance) {                     
            def error = [message: message(code: 'default.not.found.message', args: [message(code: 'faAdditionDetail.label', default: 'FaAdditionDetail'), params.id])]
            render([success: false, messages: [errors:[error]] ] as JSON)       
            return
        }
        
        if (params.version)
        {
            def version = params.version.toLong()
            if (version != null) {
                if (faAdditionDetailInstance.version > version) {
                    faAdditionDetailInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                              [message(code: 'faAdditionDetail.label', default: 'FaAdditionDetail')] as Object[],
                              "Another user has updated this FaAdditionDetail while you were editing")
                    render([success: false, messages: faAdditionDetailInstance.errors] as JSON)
                    return
                }
            }            
        }
        
        faAdditionDetailInstance.properties = params
                       
        if (!faAdditionDetailInstance.save(flush: true)) {
            render([success: false, messages: faAdditionDetailInstance.errors] as JSON)
            return
        }
                        
        render([success: true] as JSON)
    }

    def jlist() {
        if(params.masterField){
            def c = FaAdditionDetail.createCriteria()
            def results = c.list {
                eq(params.masterField.name+'.id',params.masterField.id.toLong())    
            }
            render results as JSON

        }
        else
        {
            params.max = Math.min(params.max ? params.int('max') : 10, 100)
            render FaAdditionDetail.list(params) as JSON           
        }
        
    }   

    def jdelete(Long id) {
        def faAdditionDetailInstance = FaAdditionDetail.get(id)
        if (!faAdditionDetailInstance)
            render([success: false] as JSON)
        else {
            try {
                faAdditionDetailInstance.delete(flush: true)             
                render([success: true] as JSON)
            }catch (DataIntegrityViolationException e) {
                render([success: false, error: e.message] as JSON)
            }
        }
    }

    def jshow = {
        def faAdditionDetailInstance = FaAdditionDetail.get(params.id)
        if (!faAdditionDetailInstance) {
            render(
                message : "faAdditionDetail.not.found",
            ) as JSON

        }
        else {
            render([faAdditionDetailInstance : faAdditionDetailInstance ] as JSON)
        }
    }
}
