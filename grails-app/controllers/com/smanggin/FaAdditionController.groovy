package com.smanggin



import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

/**
 * FaAdditionController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */

class FaAdditionController {
    def globalService
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        def results = FaAddition.createCriteria().list(params){}
        [faAdditionInstanceList: results, faAdditionInstanceTotal: results.totalCount]
    }

    def create() {
        [faAdditionInstance: new FaAddition(params)]
    }

    def save() {
        def faAdditionInstance = new FaAddition(params)

        faAdditionInstance.createdBy = session.user
        faAdditionInstance.state = 'Draft'
        
        def approvals = globalService.getApprovals(faAdditionInstance)        

        if(approvals){

            if (!faAdditionInstance.save(flush: true)) {
                println " errors  " +faAdditionInstance.errors
                render(view: "list", model: [faAdditionInstance: faAdditionInstance])
                return
            }
             /* insert Fa ADDition Approver*/    
            approvals.each{
                def fapApprover = new FaAdditionApprover()
                fapApprover.faAddition = faAdditionInstance
                fapApprover.noSeq = it.noSeq
                fapApprover.approver = it.approver
                fapApprover.country = Country.findByName(params?.country)
                fapApprover.status = 0
                fapApprover.approvalDetail = it
                fapApprover.save(flush:true)
            }

            flash.message = message(code: 'default.created.message', args: [message(code: 'faAddition.label', default: 'FA Addition'), faAdditionInstance.number])
            redirect(action: "show", id: faAdditionInstance.id)
        }else{
            flash.error = message(code: 'default.setApprover.message', args: [message(code: 'faAddition.label', default: 'FA Addition')])
            redirect(action: "List")
        }
       
    }

    def show() {
        def faAdditionInstance = FaAddition.get(params.id)
        if (!faAdditionInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'faAddition.label', default: 'FaAddition'), params.id])
            redirect(action: "list")
            return
        }

        [faAdditionInstance: faAdditionInstance]
    }

    def edit() {
        def faAdditionInstance = FaAddition.get(params.id)
        if (!faAdditionInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'faAddition.label', default: 'FaAddition'), params.id])
            redirect(action: "list")
            return
        }

        [faAdditionInstance: faAdditionInstance]
    }

    def update() {
        def faAdditionInstance = FaAddition.get(params.id)
        if (!faAdditionInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'faAddition.label', default: 'FaAddition'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (faAdditionInstance.version > version) {
                faAdditionInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'faAddition.label', default: 'FaAddition')] as Object[],
                          "Another user has updated this FaAddition while you were editing")
                render(view: "edit", model: [faAdditionInstance: faAdditionInstance])
                return
            }
        }

        faAdditionInstance.properties = params

        if (!faAdditionInstance.save(flush: true)) {
            render(view: "edit", model: [faAdditionInstance: faAdditionInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: [message(code: 'faAddition.label', default: 'FaAddition'), faAdditionInstance.id])
        redirect(action: "show", id: faAdditionInstance.id)
    }

    def delete() {
        def faAdditionInstance = FaAddition.get(params.id)
        if (!faAdditionInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'faAddition.label', default: 'FaAddition'), params.id])
            redirect(action: "list")
            return
        }

        try {
            faAdditionInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'faAddition.label', default: 'FaAddition'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'faAddition.label', default: 'FaAddition'), params.id])
            redirect(action: "show", id: params.id)
        }
    }

    def jsave() {
        def faAdditionInstance = (params.id) ? FaAddition.get(params.id) : new FaAddition()
        
        if (!faAdditionInstance) {                     
            def error = [message: message(code: 'default.not.found.message', args: [message(code: 'faAddition.label', default: 'FaAddition'), params.id])]
            render([success: false, messages: [errors:[error]] ] as JSON)       
            return
        }
        
        if (params.version)
        {
            def version = params.version.toLong()
            if (version != null) {
                if (faAdditionInstance.version > version) {
                    faAdditionInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                              [message(code: 'faAddition.label', default: 'FaAddition')] as Object[],
                              "Another user has updated this FaAddition while you were editing")
                    render([success: false, messages: faAdditionInstance.errors] as JSON)
                    return
                }
            }            
        }
        
        faAdditionInstance.properties = params
                       
        if (!faAdditionInstance.save(flush: true)) {
            render([success: false, messages: faAdditionInstance.errors] as JSON)
            return
        }
                        
        render([success: true] as JSON)
    }

    def jlist() {
        if(params.masterField){
            def c = FaAddition.createCriteria()
            def results = c.list {
                eq(params.masterField.name+'.id',params.masterField.id.toLong())    
            }
            render results as JSON

        }
        else
        {
            params.max = Math.min(params.max ? params.int('max') : 10, 100)
            render FaAddition.list(params) as JSON           
        }
        
    }   

    def jdelete(Long id) {
        def faAdditionInstance = FaAddition.get(id)
        if (!faAdditionInstance)
            render([success: false] as JSON)
        else {
            try {
                faAdditionInstance.delete(flush: true)             
                render([success: true] as JSON)
            }catch (DataIntegrityViolationException e) {
                render([success: false, error: e.message] as JSON)
            }
        }
    }

    def jshow = {
        def faAdditionInstance = FaAddition.get(params.id)
        if (!faAdditionInstance) {
            render(
                message : "faAddition.not.found",
            ) as JSON

        }
        else {
            render([faAdditionInstance : faAdditionInstance ] as JSON)
        }
    }

         /**
    Action Waiting Approve
    **/
    def actionWaitingApprove() {
        
        def faAdditionInstance = FaAddition.get(params.id)
        
        if (!faAdditionInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'faAddition.label', default: 'FA Addition'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (faAdditionInstance.version > version) {
                faAdditionInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'budget.label', default: 'FA Addition')] as Object[],
                          "Another user has updated this FA Addition while you were editing")
                render(view: "edit", model: [faAdditionInstance: faAdditionInstance])
                return
            }
        }

        if(faAdditionInstance.faAdditionDetails.size() == 0){
            flash.error = message(code: 'default.fillDetails.message', args: [message(code: 'faAddition.label', default: 'FA Addition'), faAdditionInstance.number])
                
            redirect(action: "show", id: faAdditionInstance.id)
        } else{
            def mustApprovedBy = globalService.getApprovalBySeq(faAdditionInstance,1)
         
            if(mustApprovedBy){
                faAdditionInstance.mustApprovedBy = mustApprovedBy[0]?.approver
            }
        
            faAdditionInstance.state = 'Waiting Approval'    

                
            if (!faAdditionInstance.save(flush: true)) {
                println faAdditionInstance.errors

                render(view: "edit", model: [faAdditionInstance: faAdditionInstance])
                return
            }

            /*saveNotif(budgetInstance,mustApprovedBy[0]?.approver)*//* --insert TO Notif */
            globalService.saveNotif(faAdditionInstance, mustApprovedBy[0]?.approver,session.user)       
            //insertTOPOBalance(rfpInstance)

                
            flash.message = message(code: 'default.waitingApproved.message', args: [message(code: 'faAddition.label', default: 'FA Addition'), faAdditionInstance.number])
                
            redirect(action: "show", id: faAdditionInstance.id)
        }
        
        
    }

    /**
    Action Approve
    **/
    def actionApprove() {
        def faAdditionInstance = FAP.get(params.id)
        if (!faAdditionInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'faAddition.label', default: 'FA Addition'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (faAdditionInstance.version > version) {
                faAdditionInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'faAddition.label', default: 'FA Addition')] as Object[],
                          "Another user has updated this FAP while you were editing")
                render(view: "edit", model: [faAdditionInstance: faAdditionInstance])
                return
            }
        }

        
        def user = User.findByLogin(session.user)

        
        if(globalService.getNextApproverObject('com.smanggin.FaAdditionApprover',faAdditionInstance,user)){
            faAdditionInstance.mustApprovedBy = globalService.getNextApproverObject('com.smanggin.FaAdditionApprover',faAdditionInstance,user)
        }

        def countFapApp = faAdditionInstance.fAPApprovers?.size()
        def countFapApproved= faAdditionInstance.findAllByFaAdditionAndStatus(faAdditionInstance,1).size()+1
    
        if(countFapApproved == countFapApp){
            faAdditionInstance.state = 'Approved'    

        }
        

        if (!faAdditionInstance.save(flush: true)) {
            println faAdditionInstance.errors
            render(view: "edit", model: [faAdditionInstance: faAdditionInstance])
            return
        }

       
        def fapApprover = FaAdditionApprover.findByFaAdditionAndApprover(faAdditionInstance,user)
        fapApprover.status = 1
        fapApprover.approverDate = new Date()
        
        if (!fapApprover.save(flush:true)) {
            println fapApprover.errors
        }    

        if(globalService.getNextApproverObject('com.smanggin.FaAdditionApprover',faAdditionInstance,user)){
            globalService.saveNotif(faAdditionInstance, faAdditionInstance.mustApprovedBy,session.user)/* --insert TO Notif */       
           // sendApproveEmail(rfpInstance)/* --Send Email */
        }

        flash.message = message(code: 'default.approved.message', args: [message(code: 'fap.label', default: 'FAP'), faAdditionInstance.number])
        redirect(action: "show", id: faAdditionInstance.id)

    }
}
