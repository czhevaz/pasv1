package com.smanggin



import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

/**
 * FAPController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */

class FAPController {
    
    def globalService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def baseCurrency = Currency.findByBaseCurrencyAndActive(true,'Yes')

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        if(params.trType){
            session.trType = params.trType    
        }

        def results = FAP.createCriteria().list(params){}
        [FAPInstanceList: results, FAPInstanceTotal: results.totalCount]
    }

    def create() {
        if(params.trType){
            session.trType = params.trType    
        }

        [FAPInstance: new FAP(params)]
    }

    def save() {
        def FAPInstance = new FAP(params)
        FAPInstance.country = params.country
      
        def date = new Date()
        def rate = Rate.createCriteria().list(params){
            
            ge('starDate',date)
            le('endDate',date)
 
            maxResults(1)
            
        }

        if(baseCurrency){
            def localCurrency = Currency.findByCodeAndActive(params.currency,'Yes')
            println 'localCurrency '+localCurrency
            FAPInstance.currency1=localCurrency
            FAPInstance.currency2=baseCurrency
            FAPInstance.rate = params.rate?params.rate.toFloat():1
            FAPInstance.rateDetail = RateDetail.get(params.rateDetail?.id)
        }

        FAPInstance.requestor = User.findByLogin(session.user)
        FAPInstance.createdBy = session.user
        FAPInstance.state = 'Draft'
		
        def approvals = globalService.getApprovals(FAPInstance)        

        if(approvals){

            if (!FAPInstance.save(flush: true)) {
                println " errors  " +FAPInstance.errors
                render(view: "list", model: [FAPInstance: FAPInstance])
                return
            }
             /* insert Rfp Approver*/    
            approvals.each{
                def fapApprover = new FAPApprover()
                fapApprover.fAP = FAPInstance
                fapApprover.noSeq = it.noSeq
                fapApprover.approver = it.approver
                fapApprover.country = Country.findByName(params?.country)
                fapApprover.status = 0
                fapApprover.approvalDetail = it
                fapApprover.save(flush:true)
            }

            flash.message = message(code: 'default.created.message', args: [message(code: 'FAP.label', default: 'FAP'), FAPInstance.id])
            redirect(action: "show", id: FAPInstance.id)

        }else{
            flash.error = message(code: 'default.setApprover.message', args: [message(code: 'budget.label', default: 'Budget')])
            redirect(action: "List")
        }
    }

    def show() {
        def FAPInstance = FAP.get(params.id)
        if (!FAPInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'FAP.label', default: 'FAP'), params.id])
            redirect(action: "list")
            return
        }

        [FAPInstance: FAPInstance]
    }

    def edit() {
        def FAPInstance = FAP.get(params.id)
        if (!FAPInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'FAP.label', default: 'FAP'), params.id])
            redirect(action: "list")
            return
        }

        [FAPInstance: FAPInstance]
    }

    def update() {
        def FAPInstance = FAP.get(params.id)
        if (!FAPInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'FAP.label', default: 'FAP'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (FAPInstance.version > version) {
                FAPInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'FAP.label', default: 'FAP')] as Object[],
                          "Another user has updated this FAP while you were editing")
                render(view: "edit", model: [FAPInstance: FAPInstance])
                return
            }
        }

        FAPInstance.properties = params

        if (!FAPInstance.save(flush: true)) {
            render(view: "edit", model: [FAPInstance: FAPInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: [message(code: 'FAP.label', default: 'FAP'), FAPInstance.id])
        redirect(action: "show", id: FAPInstance.id)
    }

    def delete() {
        def FAPInstance = FAP.get(params.id)
        if (!FAPInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'FAP.label', default: 'FAP'), params.id])
            redirect(action: "list")
            return
        }

        try {
            FAPInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'FAP.label', default: 'FAP'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'FAP.label', default: 'FAP'), params.id])
            redirect(action: "show", id: params.id)
        }
    }

    def jsave() {
        def FAPInstance = (params.id) ? FAP.get(params.id) : new FAP()
        
        if (!FAPInstance) {                     
            def error = [message: message(code: 'default.not.found.message', args: [message(code: 'FAP.label', default: 'FAP'), params.id])]
            render([success: false, messages: [errors:[error]] ] as JSON)       
            return
        }
        
        if (params.version)
        {
            def version = params.version.toLong()
            if (version != null) {
                if (FAPInstance.version > version) {
                    FAPInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                              [message(code: 'FAP.label', default: 'FAP')] as Object[],
                              "Another user has updated this FAP while you were editing")
                    render([success: false, messages: FAPInstance.errors] as JSON)
                    return
                }
            }            
        }
        
        FAPInstance.properties = params
                       
        if (!FAPInstance.save(flush: true)) {
            render([success: false, messages: FAPInstance.errors] as JSON)
            return
        }
                        
        render([success: true] as JSON)
    }

    def jlist() {
        if(params.masterField){
            def c = FAP.createCriteria()
            def results = c.list {
                eq(params.masterField.name+'.id',params.masterField.id.toLong())    
            }
            render results as JSON

        }
        else
        {
            params.max = Math.min(params.max ? params.int('max') : 10, 100)
            render FAP.list(params) as JSON           
        }
        
    }   

    def jdelete(Long id) {
        def FAPInstance = FAP.get(id)
        if (!FAPInstance)
            render([success: false] as JSON)
        else {
            try {
                FAPInstance.delete(flush: true)             
                render([success: true] as JSON)
            }catch (DataIntegrityViolationException e) {
                render([success: false, error: e.message] as JSON)
            }
        }
    }

    def jshow = {
        def FAPInstance = FAP.get(params.id)
        if (!FAPInstance) {
            render(
                message : "FAP.not.found",
            ) as JSON

        }
        else {
            render([FAPInstance : FAPInstance ] as JSON)
        }
    }


     /**
    Action Waiting Approve
    **/
    def actionWaitingApprove() {
        
        def FAPInstance = FAP.get(params.id)
        
        if (!FAPInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'budget.label', default: 'Budget'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (FAPInstance.version > version) {
                FAPInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'budget.label', default: 'RFP')] as Object[],
                          "Another user has updated this RFP while you were editing")
                render(view: "edit", model: [FAPInstance: FAPInstance])
                return
            }
        }

        if(FAPInstance.fapDetails.size() == 0){
            flash.error = message(code: 'default.fillDetails.message', args: [message(code: 'FAP.label', default: 'FAP'), FAPInstance.number])
                
            redirect(action: "show", id: FAPInstance.id)
        } else{
            def mustApprovedBy = globalService.getApprovalBySeq(FAPInstance,1)
         
            if(mustApprovedBy){
                FAPInstance.mustApprovedBy = mustApprovedBy[0]?.approver
            }
        
            FAPInstance.state = 'Waiting Approval'    

                
            if (!FAPInstance.save(flush: true)) {
                println FAPInstance.errors

                render(view: "edit", model: [FAPInstance: FAPInstance])
                return
            }

            //sendApproveEmail(rfpInstance)/* --Send Email */

            /*saveNotif(budgetInstance,mustApprovedBy[0]?.approver)*//* --insert TO Notif */
            globalService.saveNotif(FAPInstance, mustApprovedBy[0]?.approver,session.user)       
            //insertTOPOBalance(rfpInstance)

                
            flash.message = message(code: 'default.waitingApproved.message', args: [message(code: 'FAP.label', default: 'FAP'), FAPInstance.number])
                
            redirect(action: "show", id: FAPInstance.id)
        }
        
        
    }

    /**
    Action Approve
    **/
    def actionApprove() {
        def FAPInstance = FAP.get(params.id)
        if (!FAPInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'fap.label', default: 'FAP'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (FAPInstance.version > version) {
                FAPInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'fap.label', default: 'FAP')] as Object[],
                          "Another user has updated this FAP while you were editing")
                render(view: "edit", model: [FAPInstance: FAPInstance])
                return
            }
        }

        
        def user = User.findByLogin(session.user)

        
        if(globalService.getNextApproverObject('com.smanggin.FAPApprover',FAPInstance,user)){
            FAPInstance.mustApprovedBy = globalService.getNextApproverObject('com.smanggin.FAPApprover',FAPInstance,user)
        }

        def countFapApp = FAPInstance.fAPApprovers?.size()
        def countFapApproved= FAPApprover.findAllByFAPAndStatus(FAPInstance,1).size()+1
    
        if(countFapApproved == countFapApp){
            FAPInstance.state = 'Approved'    

        }
        

        if (!FAPInstance.save(flush: true)) {
            println FAPInstance.errors
            render(view: "edit", model: [FAPInstance: FAPInstance])
            return
        }

       
        def fapApprover = FAPApprover.findByFAPAndApprover(FAPInstance,user)
        fapApprover.status = 1
        fapApprover.approverDate = new Date()
        
        if (!fapApprover.save(flush:true)) {
            println fapApprover.errors
        }    

        if(globalService.getNextApproverObject('com.smanggin.FAPApprover',FAPInstance,user)){
            globalService.saveNotif(FAPInstance, FAPInstance.mustApprovedBy,session.user)/* --insert TO Notif */       
           // sendApproveEmail(rfpInstance)/* --Send Email */
        }

        flash.message = message(code: 'default.approved.message', args: [message(code: 'fap.label', default: 'FAP'), FAPInstance.number])
        redirect(action: "show", id: FAPInstance.id)

    }

}
