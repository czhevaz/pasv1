package com.smanggin



import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

/**
 * CategoryAssetController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */

class CategoryAssetController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        def results = CategoryAsset.createCriteria().list(params){}
        [categoryAssetInstanceList: results, categoryAssetInstanceTotal: results.totalCount]
    }

    def create() {
        [categoryAssetInstance: new CategoryAsset(params)]
    }

    def save() {
        def categoryAssetInstance = new CategoryAsset(params)
        categoryAssetInstance.createdBy = session.user
        if (!categoryAssetInstance.save(flush: true)) {
            render(view: "create", model: [categoryAssetInstance: categoryAssetInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: [message(code: 'categoryAsset.label', default: 'CategoryAsset'), categoryAssetInstance.id])
        redirect(action: "show", id: categoryAssetInstance.id)
    }

    def show() {
        def categoryAssetInstance = CategoryAsset.get(params.id)
        if (!categoryAssetInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'categoryAsset.label', default: 'CategoryAsset'), params.id])
            redirect(action: "list")
            return
        }

        [categoryAssetInstance: categoryAssetInstance]
    }

    def edit() {
        def categoryAssetInstance = CategoryAsset.get(params.id)
        if (!categoryAssetInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'categoryAsset.label', default: 'CategoryAsset'), params.id])
            redirect(action: "list")
            return
        }

        [categoryAssetInstance: categoryAssetInstance]
    }

    def update() {
        def categoryAssetInstance = CategoryAsset.get(params.id)
        if (!categoryAssetInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'categoryAsset.label', default: 'CategoryAsset'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (categoryAssetInstance.version > version) {
                categoryAssetInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'categoryAsset.label', default: 'CategoryAsset')] as Object[],
                          "Another user has updated this CategoryAsset while you were editing")
                render(view: "edit", model: [categoryAssetInstance: categoryAssetInstance])
                return
            }
        }

        categoryAssetInstance.properties = params

        if (!categoryAssetInstance.save(flush: true)) {
            render(view: "edit", model: [categoryAssetInstance: categoryAssetInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: [message(code: 'categoryAsset.label', default: 'CategoryAsset'), categoryAssetInstance.id])
        redirect(action: "show", id: categoryAssetInstance.id)
    }

    def delete() {
        def categoryAssetInstance = CategoryAsset.get(params.id)
        if (!categoryAssetInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'categoryAsset.label', default: 'CategoryAsset'), params.id])
            redirect(action: "list")
            return
        }

        try {
            categoryAssetInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'categoryAsset.label', default: 'CategoryAsset'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'categoryAsset.label', default: 'CategoryAsset'), params.id])
            redirect(action: "show", id: params.id)
        }
    }

    def jsave() {
        def categoryAssetInstance = (params.id) ? CategoryAsset.get(params.id) : new CategoryAsset()
        
        if (!categoryAssetInstance) {                     
            def error = [message: message(code: 'default.not.found.message', args: [message(code: 'categoryAsset.label', default: 'CategoryAsset'), params.id])]
            render([success: false, messages: [errors:[error]] ] as JSON)       
            return
        }
        
        if (params.version)
        {
            def version = params.version.toLong()
            if (version != null) {
                if (categoryAssetInstance.version > version) {
                    categoryAssetInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                              [message(code: 'categoryAsset.label', default: 'CategoryAsset')] as Object[],
                              "Another user has updated this CategoryAsset while you were editing")
                    render([success: false, messages: categoryAssetInstance.errors] as JSON)
                    return
                }
            }            
        }
        
        categoryAssetInstance.properties = params
                       
        if (!categoryAssetInstance.save(flush: true)) {
            render([success: false, messages: categoryAssetInstance.errors] as JSON)
            return
        }
                        
        render([success: true] as JSON)
    }

    def jlist() {
        if(params.masterField){
            def c = CategoryAsset.createCriteria()
            def results = c.list {
                eq(params.masterField.name+'.id',params.masterField.id.toLong())    
            }
            render results as JSON

        }
        else
        {
            def results = CategoryAsset.createCriteria().list(){
                isNotNull('categoryAsset')
            }
            render results  as JSON           
        }
        
    }   

    def jdelete(Long id) {
        def categoryAssetInstance = CategoryAsset.get(id)
        if (!categoryAssetInstance)
            render([success: false] as JSON)
        else {
            try {
                categoryAssetInstance.delete(flush: true)             
                render([success: true] as JSON)
            }catch (DataIntegrityViolationException e) {
                render([success: false, error: e.message] as JSON)
            }
        }
    }

    def jshow = {
        def categoryAssetInstance = CategoryAsset.get(params.id)
        if (!categoryAssetInstance) {
            render(
                message : "categoryAsset.not.found",
            ) as JSON

        }
        else {
            render([categoryAssetInstance : categoryAssetInstance ] as JSON)
        }
    }
}
