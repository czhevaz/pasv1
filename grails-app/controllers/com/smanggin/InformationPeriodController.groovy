package com.smanggin



import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.multipart.commons.CommonsMultipartFile


/**
 * InformationPeriodController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */

class InformationPeriodController {
    def fileUploadService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        def results = InformationPeriod.createCriteria().list(params){}
        [informationPeriodInstanceList: results, informationPeriodInstanceTotal: results.totalCount]
    }

    def create() {
        [informationPeriodInstance: new InformationPeriod(params)]
    }

    def save() {
        def informationPeriodInstance = new InformationPeriod(params)
        informationPeriodInstance.createdBy = session.user
        if (!informationPeriodInstance.save(flush: true)) {
            render(view: "create", model: [informationPeriodInstance: informationPeriodInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: [message(code: 'informationPeriod.label', default: 'InformationPeriod'), informationPeriodInstance.id])
        redirect(action: "show", id: informationPeriodInstance.id)
    }

    def show() {
        def informationPeriodInstance = InformationPeriod.get(params.id)
        if (!informationPeriodInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'informationPeriod.label', default: 'InformationPeriod'), params.id])
            redirect(action: "list")
            return
        }

        [informationPeriodInstance: informationPeriodInstance]
    }

    def edit() {
        def informationPeriodInstance = InformationPeriod.get(params.id)
        if (!informationPeriodInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'informationPeriod.label', default: 'InformationPeriod'), params.id])
            redirect(action: "list")
            return
        }

        [informationPeriodInstance: informationPeriodInstance]
    }

    def update() {
        def informationPeriodInstance = InformationPeriod.get(params.id)
        if (!informationPeriodInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'informationPeriod.label', default: 'InformationPeriod'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (informationPeriodInstance.version > version) {
                informationPeriodInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'informationPeriod.label', default: 'InformationPeriod')] as Object[],
                          "Another user has updated this InformationPeriod while you were editing")
                render(view: "edit", model: [informationPeriodInstance: informationPeriodInstance])
                return
            }
        }

        informationPeriodInstance.properties = params

        if (!informationPeriodInstance.save(flush: true)) {
            render(view: "edit", model: [informationPeriodInstance: informationPeriodInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: [message(code: 'informationPeriod.label', default: 'InformationPeriod'), informationPeriodInstance.id])
        redirect(action: "show", id: informationPeriodInstance.id)
    }

    def delete() {
        def informationPeriodInstance = InformationPeriod.get(params.id)
        if (!informationPeriodInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'informationPeriod.label', default: 'InformationPeriod'), params.id])
            redirect(action: "list")
            return
        }

        try {
            informationPeriodInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'informationPeriod.label', default: 'InformationPeriod'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'informationPeriod.label', default: 'InformationPeriod'), params.id])
            redirect(action: "show", id: params.id)
        }
    }

    def jsave() {
        def informationPeriodInstance = (params.id) ? InformationPeriod.get(params.id) : new InformationPeriod()
        
        if (!informationPeriodInstance) {                     
            def error = [message: message(code: 'default.not.found.message', args: [message(code: 'informationPeriod.label', default: 'InformationPeriod'), params.id])]
            render([success: false, messages: [errors:[error]] ] as JSON)       
            return
        }
        
        if (params.version)
        {
            def version = params.version.toLong()
            if (version != null) {
                if (informationPeriodInstance.version > version) {
                    informationPeriodInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                              [message(code: 'informationPeriod.label', default: 'InformationPeriod')] as Object[],
                              "Another user has updated this InformationPeriod while you were editing")
                    render([success: false, messages: informationPeriodInstance.errors] as JSON)
                    return
                }
            }            
        }
        
        informationPeriodInstance.properties = params
                       
        if (!informationPeriodInstance.save(flush: true)) {
            render([success: false, messages: informationPeriodInstance.errors] as JSON)
            return
        }
                        
        render([success: true] as JSON)
    }

    def jlist() {
        if(params.masterField){
            def c = InformationPeriod.createCriteria()
            def results = c.list {
                eq(params.masterField.name+'.id',params.masterField.id.toLong())    
            }
            render results as JSON

        }
        else
        {
            params.max = Math.min(params.max ? params.int('max') : 10, 100)
            render InformationPeriod.list(params) as JSON           
        }
        
    }   

    def jdelete(Long id) {
        def informationPeriodInstance = InformationPeriod.get(id)
        if (!informationPeriodInstance)
            render([success: false] as JSON)
        else {
            try {
                informationPeriodInstance.delete(flush: true)             
                render([success: true] as JSON)
            }catch (DataIntegrityViolationException e) {
                render([success: false, error: e.message] as JSON)
            }
        }
    }

    def jshow = {
        def informationPeriodInstance = InformationPeriod.get(params.id)
        if (!informationPeriodInstance) {
            render(
                message : "informationPeriod.not.found",
            ) as JSON

        }
        else {
            render([informationPeriodInstance : informationPeriodInstance ] as JSON)
        }
    }

    /**
    upload
    **/  
    def upload(){
        
        println "params>>> " + params

        MultipartHttpServletRequest mpr = (MultipartHttpServletRequest)request
        CommonsMultipartFile file =(CommonsMultipartFile) mpr.getFile("files")
        def dir= "upload/informationPeriod"
        
        def fileName = fileUploadService.uploadFileAjax2(file,dir,params.id,'InformationPeriod')
        
        def auth = auth.user()        
        int dot = fileName.lastIndexOf('.');
        def ext = fileName.substring(dot + 1);

         
        render([success: true] as JSON)
    }
}
