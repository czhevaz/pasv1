package com.smanggin



import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

/**
 * BudgetController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */

class BudgetController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def globalService       
    def baseCurrency = Currency.findByBaseCurrencyAndActive(true,'Yes')

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        
        if(params.trType){
            session.trType = params.trType    
        }

       // params.max = Math.min(params.max ? params.int('max') : 10, 100)
        def results = Budget.createCriteria().list(params){}
        [budgetInstanceList: results, budgetInstanceTotal: results.totalCount]
    }

    def create() {
        if(params.trType){
            session.trType = params.trType    
        }
        
        [budgetInstance: new Budget(params),baseCurrency:baseCurrency]
    }

    def save() {
        
        def budgetInstance = new Budget(params)

        budgetInstance.country = params.country
      
        def date = new Date()
        def rate = Rate.createCriteria().list(params){
            
            ge('starDate',date)
            le('endDate',date)
 
            maxResults(1)
            
        }

        if(baseCurrency){
            def localCurrency = Currency.findByCodeAndActive(params.currency,'Yes')

            budgetInstance.currency1=localCurrency
            budgetInstance.currency2=baseCurrency
            budgetInstance.rate = params.rate?params.rate.toFloat():1
            budgetInstance.rateDetail = RateDetail.get(params.rateDetail?.id)
        }

        budgetInstance.requestor = User.findByLogin(session.user)
        budgetInstance.createdBy = session.user
        budgetInstance.state = 'Draft'
        def approvals = globalService.getApprovals(budgetInstance)        

        if(approvals){

            if (!budgetInstance.save(flush: true)) {
                println " errors  " +budgetInstance.errors
                render(view: "list", model: [budgetInstance: budgetInstance])
                return
            }

             /* insert Rfp Approver*/    
            
            approvals.each{
                def budgetApprover = new BudgetApprover()
                budgetApprover.budget = budgetInstance
                budgetApprover.noSeq = it.noSeq
                budgetApprover.approver = it.approver
                budgetApprover.country = Country.findByName(params?.country)
                budgetApprover.status = 0
                budgetApprover.approvalDetail = it
                budgetApprover.save(flush:true)
            }

            flash.message = message(code: 'default.created.message', args: [message(code: 'budget.label', default: 'Budget'), budgetInstance.id])
            redirect(action: "show", id: budgetInstance.id)

        }else{
            flash.error = message(code: 'default.setApprover.message', args: [message(code: 'budget.label', default: 'Budget')])
            redirect(action: "List")
        }
        
    }

    def show() {
        def budgetInstance = Budget.get(params.id)

        globalService.getObjectApprovalSeq("com.smanggin.BudgetApprover",budgetInstance,User.findByLogin(session.user))
        if (!budgetInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'budget.label', default: 'Budget'), params.id])
            redirect(action: "list")
            return
        }

        [budgetInstance: budgetInstance]
    }

    def edit() {
        def budgetInstance = Budget.get(params.id)
        if (!budgetInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'budget.label', default: 'Budget'), params.id])
            redirect(action: "list")
            return
        }

        [budgetInstance: budgetInstance]
    }

    def update() {
        def budgetInstance = Budget.get(params.id)
        if (!budgetInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'budget.label', default: 'Budget'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (budgetInstance.version > version) {
                budgetInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'budget.label', default: 'Budget')] as Object[],
                          "Another user has updated this Budget while you were editing")
                render(view: "edit", model: [budgetInstance: budgetInstance])
                return
            }
        }

        budgetInstance.properties = params

        if (!budgetInstance.save(flush: true)) {
            render(view: "edit", model: [budgetInstance: budgetInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: [message(code: 'budget.label', default: 'Budget'), budgetInstance.id])
        redirect(action: "show", id: budgetInstance.id)
    }

    def delete() {
        def budgetInstance = Budget.get(params.id)
        if (!budgetInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'budget.label', default: 'Budget'), params.id])
            redirect(action: "list")
            return
        }

        try {
            budgetInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'budget.label', default: 'Budget'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'budget.label', default: 'Budget'), params.id])
            redirect(action: "show", id: params.id)
        }
    }

    def jsave() {
        def budgetInstance = (params.id) ? Budget.get(params.id) : new Budget()
        
        if (!budgetInstance) {                     
            def error = [message: message(code: 'default.not.found.message', args: [message(code: 'budget.label', default: 'Budget'), params.id])]
            render([success: false, messages: [errors:[error]] ] as JSON)       
            return
        }
        
        if (params.version)
        {
            def version = params.version.toLong()
            if (version != null) {
                if (budgetInstance.version > version) {
                    budgetInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                              [message(code: 'budget.label', default: 'Budget')] as Object[],
                              "Another user has updated this Budget while you were editing")
                    render([success: false, messages: budgetInstance.errors] as JSON)
                    return
                }
            }            
        }
        
        budgetInstance.properties = params
                       
        if (!budgetInstance.save(flush: true)) {
            render([success: false, messages: budgetInstance.errors] as JSON)
            return
        }
                        
        render([success: true] as JSON)
    }

    def jlist() {
        if(params.masterField){
            def c = Budget.createCriteria()
            def results = c.list {
                eq(params.masterField.name+'.id',params.masterField.id.toLong())    
            }
            render results as JSON

        }else if(params?.searchBudget){
            println "<<<<<<<<< searchBudget >>>>>>>>>> "
            /* filter PO for RFP detail */
            def c = Budget.createCriteria()
            
            def results = c.list {
                eq('country',params.countryBudget)
                currency1{
                    eq('code',params.currencyCode)
                }
                eq('state','Approved')
            }

            def listBudget = []
            results.each{ it ->
                def map = [:]
                    map.put('number',it.number)
                    map.put('budgetDate',it.budgetDate?.format('dd-MM-yyyy'))
                    map.put('budgetId',it.id)
                    map.put('createdBy',it.createdBy)
                    //map.put('total',it.total)
                    //map.put('total2',(it.total/it.rate).round(2))
                    //map.put('poRemain1',it.PORemain1)
                    //map.put('poRemain2',it.PORemain2)
                    map.put('country',it.country)
                    map.put('currency1',it.currency1?.code)

                    listBudget.push(map)
            }

            def list=[:]
            list.put('sEcho','')
            list.put('iTotalRecords',results?.size())
            list.put('iTotalDisplayRecords',results?.size())
            list.put('aaData',listBudget)

            println " list>>  " + list

            render list as JSON
        }    
        else
        {
            //params.max = Math.min(params.max ? params.int('max') : 10, 100)
            render Budget.list(params) as JSON           
        }
        
    }   

    def jdelete(Long id) {
        def budgetInstance = Budget.get(id)
        if (!budgetInstance)
            render([success: false] as JSON)
        else {
            try {
                budgetInstance.delete(flush: true)             
                render([success: true] as JSON)
            }catch (DataIntegrityViolationException e) {
                render([success: false, error: e.message] as JSON)
            }
        }
    }

    def jshow = {
        def budgetInstance = Budget.get(params.id)
        if (!budgetInstance) {
            render(
                message : "budget.not.found",
            ) as JSON

        }
        else {
            render([budgetInstance : budgetInstance ] as JSON)
        }
    }


    /**
    Action Waiting Approve
    **/
    def actionWaitingApprove() {
        
        def budgetInstance = Budget.get(params.id)
        
        if (!budgetInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'budget.label', default: 'Budget'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (budgetInstance.version > version) {
                budgetInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'budget.label', default: 'RFP')] as Object[],
                          "Another user has updated this RFP while you were editing")
                render(view: "edit", model: [budgetInstance: budgetInstance])
                return
            }
        }

        if(budgetInstance.budgetDetails.size() == 0){
            flash.error = message(code: 'default.fillDetails.message', args: [message(code: 'budget.label', default: 'Budget'), budgetInstance.number])
                
            redirect(action: "show", id: budgetInstance.id)
        } else{
            def mustApprovedBy = globalService.getApprovalBySeq(budgetInstance,1)
         
            if(mustApprovedBy){
                budgetInstance.mustApprovedBy = mustApprovedBy[0]?.approver
            }
        
            budgetInstance.state = 'Waiting Approval'    

                
            if (!budgetInstance.save(flush: true)) {
                println budgetInstance.errors

                render(view: "edit", model: [budgetInstance: budgetInstance])
                return
            }

            //sendApproveEmail(rfpInstance)/* --Send Email */

            /*saveNotif(budgetInstance,mustApprovedBy[0]?.approver)*//* --insert TO Notif */
            globalService.saveNotif(budgetInstance, mustApprovedBy[0]?.approver,session.user)       
            //insertTOPOBalance(rfpInstance)

                
            flash.message = message(code: 'default.waitingApproved.message', args: [message(code: 'budget.label', default: 'Budget'), budgetInstance.number])
                
            redirect(action: "show", id: budgetInstance.id)
        }
        
        
    }

    /**
    Action Approve
    **/
    def actionApprove() {
        def budgetInstance = Budget.get(params.id)
        if (!budgetInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'budget.label', default: 'Budget'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (budgetInstance.version > version) {
                budgetInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'budget.label', default: 'Budget')] as Object[],
                          "Another user has updated this Budget while you were editing")
                render(view: "edit", model: [budgetInstance: budgetInstance])
                return
            }
        }

        
        def user = User.findByLogin(session.user)

        
        if(globalService.getNextApproverObject('com.smanggin.BudgetApprover',budgetInstance,user)){
            budgetInstance.mustApprovedBy = globalService.getNextApproverObject('com.smanggin.BudgetApprover',budgetInstance,user)
        }

        def countBudgetApp = budgetInstance.budgetApprovers?.size()
        def countBudgetApproved= BudgetApprover.findAllByBudgetAndStatus(budgetInstance,1).size()+1
    
        if(countBudgetApproved == countBudgetApp){
            budgetInstance.state = 'Approved'    

        }
        

        if (!budgetInstance.save(flush: true)) {
            println budgetInstance.errors
            render(view: "edit", model: [budgetInstance: budgetInstance])
            return
        }

       
        def budgetApprover = BudgetApprover.findByBudgetAndApprover(budgetInstance,user)
        budgetApprover.status = 1
        budgetApprover.approverDate = new Date()
        
        if (!budgetApprover.save(flush:true)) {
            println budgetApprover.errors
        }    

        if(globalService.getNextApproverObject('com.smanggin.BudgetApprover',budgetInstance,user)){
            globalService.saveNotif(budgetInstance, budgetInstance.mustApprovedBy,session.user)/* --insert TO Notif */       
           // sendApproveEmail(rfpInstance)/* --Send Email */
        }

        flash.message = message(code: 'default.approved.message', args: [message(code: 'budget.label', default: 'Budget'), budgetInstance.number])
        redirect(action: "show", id: budgetInstance.id)
    }

     /**
    Action Reject
    **/
    def actionReject() {
        
        def budgetInstance = Rfp.get(params.id)
        
        if (!budgetInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'rfp.label', default: 'Rfp'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (rfpInstance.version > version) {
                rfpInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'purchaseOrder.label', default: 'Rfp')] as Object[],
                          "Another user has updated this Rfp while you were editing")
                render(view: "edit", model: [rfpInstance: rfpInstance])
                return
            }
        }
        
        
        budgetInstance.mustApprovedBy = null   
        budgetInstance.dateReject = new Date()
        budgetInstance.rejectedBy = session.user
       
        
        budgetInstance.state = 'Rejected'    
        
 

        if (!budgetInstance.save(flush: true)) {

            render(view: "edit", model: [rfpInstance: rfpInstance])
            return
        }

        /* update Po Approver*/
        def user = User.findByLogin(session.user)
        def budgetApprover = BudgetApprover.findByBudgetAndApprover(budgetInstance,user)
        
        if(rfpApprover){
            budgetApprover.status = 2
            budgetApprover.approverDate = new Date()
            budgetApprover.save(flush:true)

        }


        globalService.saveNotif(budgetInstance, budgetInstance.createdBy,session.user)/* --insert TO Notif */       
        
        flash.message = message(code: 'default.rejected.message', args: [message(code: 'budget.label', default: 'Budget'), budgetInstance.number])
        redirect(action: "show", id: budgetInstance.id)
    }

}
