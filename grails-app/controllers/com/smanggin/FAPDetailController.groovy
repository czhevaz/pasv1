package com.smanggin



import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

/**
 * FAPDetailController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */

class FAPDetailController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        def results = FAPDetail.createCriteria().list(params){}
        [FAPDetailInstanceList: results, FAPDetailInstanceTotal: results.totalCount]
    }

    def create() {
        [FAPDetailInstance: new FAPDetail(params)]
    }

    def save() {
        def FAPDetailInstance = new FAPDetail(params)
        if (!FAPDetailInstance.save(flush: true)) {
            render(view: "create", model: [FAPDetailInstance: FAPDetailInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: [message(code: 'FAPDetail.label', default: 'FAPDetail'), FAPDetailInstance.id])
        redirect(action: "show", id: FAPDetailInstance.id)
    }

    def show() {
        def FAPDetailInstance = FAPDetail.get(params.id)
        if (!FAPDetailInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'FAPDetail.label', default: 'FAPDetail'), params.id])
            redirect(action: "list")
            return
        }

        [FAPDetailInstance: FAPDetailInstance]
    }

    def edit() {
        def FAPDetailInstance = FAPDetail.get(params.id)
        if (!FAPDetailInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'FAPDetail.label', default: 'FAPDetail'), params.id])
            redirect(action: "list")
            return
        }

        [FAPDetailInstance: FAPDetailInstance]
    }

    def update() {
        def FAPDetailInstance = FAPDetail.get(params.id)
        if (!FAPDetailInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'FAPDetail.label', default: 'FAPDetail'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (FAPDetailInstance.version > version) {
                FAPDetailInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'FAPDetail.label', default: 'FAPDetail')] as Object[],
                          "Another user has updated this FAPDetail while you were editing")
                render(view: "edit", model: [FAPDetailInstance: FAPDetailInstance])
                return
            }
        }

        FAPDetailInstance.properties = params

        if (!FAPDetailInstance.save(flush: true)) {
            render(view: "edit", model: [FAPDetailInstance: FAPDetailInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: [message(code: 'FAPDetail.label', default: 'FAPDetail'), FAPDetailInstance.id])
        redirect(action: "show", id: FAPDetailInstance.id)
    }

    def delete() {
        def FAPDetailInstance = FAPDetail.get(params.id)
        if (!FAPDetailInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'FAPDetail.label', default: 'FAPDetail'), params.id])
            redirect(action: "list")
            return
        }

        try {
            FAPDetailInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'FAPDetail.label', default: 'FAPDetail'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'FAPDetail.label', default: 'FAPDetail'), params.id])
            redirect(action: "show", id: params.id)
        }
    }

    def jsave() {
        println "params " + params  
        def FAPDetailInstance = (params.id) ? FAPDetail.get(params.id) : new FAPDetail()
        
        if (!FAPDetailInstance) {                     
            def error = [message: message(code: 'default.not.found.message', args: [message(code: 'FAPDetail.label', default: 'FAPDetail'), params.id])]
            render([success: false, messages: [errors:[error]] ] as JSON)       
            return
        }
        
        if (params.version)
        {
            def version = params.version.toLong()
            if (version != null) {
                if (FAPDetailInstance.version > version) {
                    FAPDetailInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                              [message(code: 'FAPDetail.label', default: 'FAPDetail')] as Object[],
                              "Another user has updated this FAPDetail while you were editing")
                    render([success: false, messages: FAPDetailInstance.errors] as JSON)
                    return
                }
            }            
        }
        
        FAPDetailInstance.properties = params
        FAPDetailInstance.budgetDetail = BudgetDetail.get(params.budgetDetailId)
        FAPDetailInstance.fap = FAP.get(params.fapId)
        
                       
        if (!FAPDetailInstance.save(flush: true)) {
            render([success: false, messages: FAPDetailInstance.errors] as JSON)
            return
        }
                        
        render([success: true] as JSON)
    }

    def jlist() {
        if(params.masterField){
            def c = FAPDetail.createCriteria()
            def results = c.list {
                eq(params.masterField.name+'.id',params.masterField.id.toLong())    
            }
            render results as JSON

        }
        else
        {
            params.max = Math.min(params.max ? params.int('max') : 10, 100)
            render FAPDetail.list(params) as JSON           
        }
        
    }   

    def jdelete(Long id) {
        def FAPDetailInstance = FAPDetail.get(id)
        if (!FAPDetailInstance)
            render([success: false] as JSON)
        else {
            try {
                FAPDetailInstance.delete(flush: true)             
                render([success: true] as JSON)
            }catch (DataIntegrityViolationException e) {
                render([success: false, error: e.message] as JSON)
            }
        }
    }

    def jshow = {
        def FAPDetailInstance = FAPDetail.get(params.id)
        if (!FAPDetailInstance) {
            render(
                message : "FAPDetail.not.found",
            ) as JSON

        }
        else {
            render([FAPDetailInstance : FAPDetailInstance ] as JSON)
        }
    }
}
