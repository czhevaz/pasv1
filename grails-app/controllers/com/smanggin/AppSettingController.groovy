package com.smanggin



import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException
import java.util.regex.Pattern

/**
 * AppSettingController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */

class AppSettingController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        def results = AppSetting.createCriteria().list(params){}
        [appSettingInstanceList: results, appSettingInstanceTotal: results.totalCount]
    }

    def create() {
        [appSettingInstance: new AppSetting(params)]
    }

    def save() {
        def appSettingInstance = new AppSetting(params)
        if (!appSettingInstance.save(flush: true)) {
            render(view: "create", model: [appSettingInstance: appSettingInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: [message(code: 'appSetting.label', default: 'AppSetting'), appSettingInstance.id])
        redirect(action: "show", id: appSettingInstance.id)
    }

    def show() {
        def appSettingInstance = AppSetting.get(params.id)
        if (!appSettingInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'appSetting.label', default: 'AppSetting'), params.id])
            redirect(action: "list")
            return
        }

        [appSettingInstance: appSettingInstance]
    }

    def edit() {
        def appSettingInstance = AppSetting.get(params.id)
        if (!appSettingInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'appSetting.label', default: 'AppSetting'), params.id])
            redirect(action: "list")
            return
        }

        [appSettingInstance: appSettingInstance]
    }

    def update() {
        def appSettingInstance = AppSetting.get(params.id)
        if (!appSettingInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'appSetting.label', default: 'AppSetting'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (appSettingInstance.version > version) {
                appSettingInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'appSetting.label', default: 'AppSetting')] as Object[],
                          "Another user has updated this AppSetting while you were editing")
                render(view: "edit", model: [appSettingInstance: appSettingInstance])
                return
            }
        }

        appSettingInstance.properties = params
        


        if (!appSettingInstance.save(flush: true)) {
            render(view: "edit", model: [appSettingInstance: appSettingInstance])
            return
        }

        if (params.code == 'fa_code_format'){
            //save separator also
            def separatorInstance = AppSetting.get(params.separatorId)
            separatorInstance.val = params.separator
            separatorInstance.save(flush:true)
        }

		flash.message = message(code: 'default.updated.message', args: [message(code: 'appSetting.label', default: 'AppSetting'), appSettingInstance.id])
        redirect(action: "show", id: appSettingInstance.id)
    }

    def delete() {
        def appSettingInstance = AppSetting.get(params.id)
        if (!appSettingInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'appSetting.label', default: 'AppSetting'), params.id])
            redirect(action: "list")
            return
        }

        try {
            appSettingInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'appSetting.label', default: 'AppSetting'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'appSetting.label', default: 'AppSetting'), params.id])
            redirect(action: "show", id: params.id)
        }
    }

    def jsave() {
        def appSettingInstance = (params.id) ? AppSetting.get(params.id) : new AppSetting()
        
        if (!appSettingInstance) {                     
            def error = [message: message(code: 'default.not.found.message', args: [message(code: 'appSetting.label', default: 'AppSetting'), params.id])]
            render([success: false, messages: [errors:[error]] ] as JSON)       
            return
        }
        
        if (params.version)
        {
            def version = params.version.toLong()
            if (version != null) {
                if (appSettingInstance.version > version) {
                    appSettingInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                              [message(code: 'appSetting.label', default: 'AppSetting')] as Object[],
                              "Another user has updated this AppSetting while you were editing")
                    render([success: false, messages: appSettingInstance.errors] as JSON)
                    return
                }
            }            
        }
        
        appSettingInstance.properties = params
                       
        if (!appSettingInstance.save(flush: true)) {
            render([success: false, messages: appSettingInstance.errors] as JSON)
            return
        }
                        
        render([success: true] as JSON)
    }

    def jlist() {
        if(params.masterField){
            def c = AppSetting.createCriteria()
            def results = c.list {
                eq(params.masterField.name+'.id',params.masterField.id.toLong())    
            }
            render results as JSON

        }
        else
        {
            params.max = Math.min(params.max ? params.int('max') : 10, 100)
            render AppSetting.list(params) as JSON           
        }
        
    }   

    def jdelete(Long id) {
        def appSettingInstance = AppSetting.get(id)
        if (!appSettingInstance)
            render([success: false] as JSON)
        else {
            try {
                appSettingInstance.delete(flush: true)             
                render([success: true] as JSON)
            }catch (DataIntegrityViolationException e) {
                render([success: false, error: e.message] as JSON)
            }
        }
    }

    def jshow = {
        def appSettingInstance = AppSetting.get(params.id)
        if (!appSettingInstance) {
            render(
                message : "appSetting.not.found",
            ) as JSON

        }
        else {
            render([appSettingInstance : appSettingInstance ] as JSON)
        }
    }

    def faCodeSetting(){
        def faCodeFormatDefault = AppSetting.valueDefault('fa_code_format',' Please Setting')
        def facodeSepartorDefault = AppSetting.valueDefault('fa_code_separator','/')
        def faCodeFormat = AppSetting.findByCode('fa_code_format')
        def facodeSepartor = AppSetting.findByCode('fa_code_separator')
        def dataTypeMap = [runningNo:'runningNo',freeText:'freeText',timeStamps:'timeStamps',tables:'tables'] 
        def formatDate = [  "yyyyMMddHHmmss",
                            "yyyyMMddHHmm",
                            "yyyyMMddHH",
                            "yyyyMMdd",
                            "yyyyMM",
                            "yyyy",
                            "MM",
                            "dd",
                            "HH",
                            "mm",
                            "ss"
                        ] 

        def  tableMaster = [ 'Department',
                                  'TransactionGroup',
                                  'Country',
                                  'Lob',
                                  'Brand',
                                  'Supplier'
                                  ]

        def segments = faCodeFormat.val.split(Pattern.quote( facodeSepartor.val ) ) 
        
        def list=[]
        def i=1

        segments.each{ it->
            def segment = it.split(':')
            
            def map = [:]
            map.put('segment',segment[1])
            map.put('type',segment[0])
            map.put('val',it)

            if(segment[0] == 'freeText'){
                map.put('label',segment[2])
                map.put('dataType',segment[3])
                if(segment[3] == 'varchar'){
                    map.put('length',segment[4])
                }
            }else if(segment[0] == 'timeStamps'){
                map.put('formatDate',segment[2])
            }else if(segment[0] == 'runningNo'){
                map.put('length',segment[2])
            }

            list.push(map)

            i++
        }
        
        [
            faCodeFormat:faCodeFormat,
            facodeSepartor:facodeSepartor,
            separators : ['/','-'],
            dataTypeMap:dataTypeMap,
            formatDate:formatDate,
            tableMaster:tableMaster,
            listVal:list
        ]

    }

    def getClassFieldName(String tablesName){
        def tableName=""
        
        if(params.tabelsName){
              tableName=params.tabelsName
        }else{
             tableName=tablesName
        }

        def className="com.smanggin."+tableName
        def names = grailsApplication.getDomainClass(className).persistentProperties.collect { 
                if(!it.association){
                    it.name    
                }
            }
        println names
        if(params.tabelsName){
            if (!names) {
                render(
                    message : "name.not.found",
                ) as JSON

            }else{
                render([
                    success: true,
                    names : names ] as JSON)
            }
        }else{
            return names
        }
    }



}
