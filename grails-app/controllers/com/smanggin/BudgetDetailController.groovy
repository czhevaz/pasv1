package com.smanggin



import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

/**
 * BudgetDetailController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */

class BudgetDetailController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        def results = BudgetDetail.createCriteria().list(params){}
        [budgetDetailInstanceList: results, budgetDetailInstanceTotal: results.totalCount]
    }

    def create() {
        [budgetDetailInstance: new BudgetDetail(params)]
    }

    def save() {
        def budgetDetailInstance = new BudgetDetail(params)
        if (!budgetDetailInstance.save(flush: true)) {
            render(view: "create", model: [budgetDetailInstance: budgetDetailInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: [message(code: 'budgetDetail.label', default: 'BudgetDetail'), budgetDetailInstance.id])
        redirect(action: "show", id: budgetDetailInstance.id)
    }

    def show() {
        def budgetDetailInstance = BudgetDetail.get(params.id)
        if (!budgetDetailInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'budgetDetail.label', default: 'BudgetDetail'), params.id])
            redirect(action: "list")
            return
        }

        [budgetDetailInstance: budgetDetailInstance]
    }

    def edit() {
        def budgetDetailInstance = BudgetDetail.get(params.id)
        if (!budgetDetailInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'budgetDetail.label', default: 'BudgetDetail'), params.id])
            redirect(action: "list")
            return
        }

        [budgetDetailInstance: budgetDetailInstance]
    }

    def update() {
        def budgetDetailInstance = BudgetDetail.get(params.id)
        if (!budgetDetailInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'budgetDetail.label', default: 'BudgetDetail'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (budgetDetailInstance.version > version) {
                budgetDetailInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'budgetDetail.label', default: 'BudgetDetail')] as Object[],
                          "Another user has updated this BudgetDetail while you were editing")
                render(view: "edit", model: [budgetDetailInstance: budgetDetailInstance])
                return
            }
        }

        budgetDetailInstance.properties = params

        if (!budgetDetailInstance.save(flush: true)) {
            render(view: "edit", model: [budgetDetailInstance: budgetDetailInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: [message(code: 'budgetDetail.label', default: 'BudgetDetail'), budgetDetailInstance.id])
        redirect(action: "show", id: budgetDetailInstance.id)
    }

    def delete() {
        def budgetDetailInstance = BudgetDetail.get(params.id)
        if (!budgetDetailInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'budgetDetail.label', default: 'BudgetDetail'), params.id])
            redirect(action: "list")
            return
        }

        try {
            budgetDetailInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'budgetDetail.label', default: 'BudgetDetail'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'budgetDetail.label', default: 'BudgetDetail'), params.id])
            redirect(action: "show", id: params.id)
        }
    }

    def jsave() {
        def budgetDetailInstance = (params.id) ? BudgetDetail.get(params.id) : new BudgetDetail()
        
        if (!budgetDetailInstance) {                     
            def error = [message: message(code: 'default.not.found.message', args: [message(code: 'budgetDetail.label', default: 'BudgetDetail'), params.id])]
            render([success: false, messages: [errors:[error]] ] as JSON)       
            return
        }
        
        if (params.version)
        {
            def version = params.version.toLong()
            if (version != null) {
                if (budgetDetailInstance.version > version) {
                    budgetDetailInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                              [message(code: 'budgetDetail.label', default: 'BudgetDetail')] as Object[],
                              "Another user has updated this BudgetDetail while you were editing")
                    render([success: false, messages: budgetDetailInstance.errors] as JSON)
                    return
                }
            }            
        }
        
        budgetDetailInstance.properties = params
        budgetDetailInstance.coa = ChartOfAccount.findByCode(params?.coaId)
        budgetDetailInstance.budget = Budget.get(params.budgetId)
        budgetDetailInstance.categoryAsset = CategoryAsset.get(params.categoryAssetId)
        budgetDetailInstance.createdBy = session.user
                       
        if (!budgetDetailInstance.save(flush: true)) {
            render([success: false, messages: budgetDetailInstance.errors] as JSON)
            return
        }
                        
        render([success: true] as JSON)
    }

    def jlist() {
        if(params.masterField){
            def c = BudgetDetail.createCriteria()
            def results = c.list {
                eq(params.masterField.name+'.id',params.masterField.id.toLong())    
            }
            render results as JSON

        }else if(params?.searchBudget){
            println "<<<<<<<<< searchBudget >>>>>>>>>> "
            /* filter PO for RFP detail */
            def c = BudgetDetail.createCriteria()
            
            def results = c.list {
                budget{
                    eq('country',params.countryBudget)
                    currency1{
                        eq('code',params.currencyCode)
                    }
                    eq('state','Approved')    
                }
                
            }

            def listBudget = []
            results.each{ it ->
                def map = [:]
                    map.put('id',it.id)
                    map.put('categoryAssetName',it.categoryAsset?.name)
                    map.put('budgetNumber',it.budget?.number)
                    map.put('budgetDate',it.budget?.budgetDate?.format('dd-MM-yyyy'))
                    map.put('budgetId',it.budget?.id)
                    map.put('createdBy',it.budget?.createdBy)
                    map.put('qty',it.qty)
                    map.put('uom',it.uom)
                    map.put('total',it.value)
                    //map.put('total2',(it.value/it.rate).round(2))
                    //map.put('poRemain1',it.PORemain1)
                    //map.put('poRemain2',it.PORemain2)
                    map.put('country',it.budget?.country)
                    map.put('currency1',it.budget?.currency1?.code)

                    listBudget.push(map)
            }

            def list=[:]
            list.put('sEcho','')
            list.put('iTotalRecords',results?.size())
            list.put('iTotalDisplayRecords',results?.size())
            list.put('aaData',listBudget)

            println " list>>  " + list

            render list as JSON
        }
        else
        {
            params.max = Math.min(params.max ? params.int('max') : 10, 100)
            render BudgetDetail.list(params) as JSON           
        }
        
    }   

    def jdelete(Long id) {
        def budgetDetailInstance = BudgetDetail.get(id)
        if (!budgetDetailInstance)
            render([success: false] as JSON)
        else {
            try {
                budgetDetailInstance.delete(flush: true)             
                render([success: true] as JSON)
            }catch (DataIntegrityViolationException e) {
                render([success: false, error: e.message] as JSON)
            }
        }
    }

    def jshow = {
        def budgetDetailInstance = BudgetDetail.get(params.id)
        if (!budgetDetailInstance) {
            render(
                message : "budgetDetail.not.found",
            ) as JSON

        }
        else {
            render([budgetDetailInstance : budgetDetailInstance ] as JSON)
        }
    }
}
