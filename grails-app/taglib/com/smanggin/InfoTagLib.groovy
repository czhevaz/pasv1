package com.smanggin

/**
 * JqueryDateTagLib
 * A taglib library provides a set of reusable tags to help rendering the views.
 */
class InfoTagLib {
	def infoLogin={ 
		def out = out
		def appSetting=AppSetting.valueDefault('info_template','Your can Edit information in Application Setting ')
		out.println appSetting
	}
}