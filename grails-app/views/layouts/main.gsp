<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="${session.'org.springframework.web.servlet.i18n.SessionLocaleResolver.LOCALE'}"><!--<![endif]-->
	<head>
		<title><g:layoutTitle default="${meta(name:'app.title')}" /></title>
		
	    <meta charset="utf-8">
	    <meta name="viewport"		content="width=device-width, initial-scale=1.0">
	    <meta name="description"	content="">
	    <meta name="author"			content="">
	    
		<link rel="shortcut icon"		href="${resource(dir:'images',file:'favicon.ico')}" type="image/x-icon" />
		
		<link rel="apple-touch-icon"	href="assets/ico/apple-touch-icon.png">
	    <link rel="apple-touch-icon"	href="assets/ico/apple-touch-icon-72x72.png"	sizes="72x72">
	    <link rel="apple-touch-icon"	href="assets/ico/apple-touch-icon-114x114.png"	sizes="114x114">
		
		<%-- Manual switch for the skin can be found in /view/_menu/_config.gsp --%>
		<r:require modules="bootstrap"/>
		<r:require modules="bootstrap_utils"/>
		<r:require modules="font-awesome-css"/>

		<r:require modules="jquery"/>
		<r:require modules="jquery-ui"/>
		<r:require modules="application"/>	
		<r:require modules="fileUpload"/>	
		<r:require modules="datatables"/>
		<r:require modules="easyui_core"/>	
		
		<link rel="stylesheet" href="${resource(dir: 'css', file: 'my.css')}" type="text/css">

		
	  	<link rel="stylesheet" href="${resource(dir: 'js/chosen/docsupport', file: 'prism.css')} ">
	  	<link rel="stylesheet" href="${resource(dir: 'js/chosen', file: 'chosen.css')} ">

		

		<r:layoutResources />
		<g:layoutHead />

		<!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
		<!--[if lt IE 9]>
			<script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->

		<%-- For Javascript see end of body --%>
	</head>

	<body style="background-color:#cce9ee">
		<div id="custom-bootstrap-menu" class="navbar navbar-fixed-top  navbar-default " role="navigation">
		    <div class="container">
		        <div class="navbar-header"><a class="navbar-brand" href="#">${meta(name:'app.title')}</a>
		            %{-- <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-menubuilder"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
		            </button> --}%
		        </div>
		        %{-- <div class="collapse navbar-collapse navbar-menubuilder">
		            <ul class="nav navbar-nav navbar-left">
		                <li><a href="/">Home</a>
		                </li>
		                <li><a href="/products">Products</a>
		                </li>
		                <li><a href="/about-us">About Us</a>
		                </li>
		                <li><a href="/contact">Contact Us</a>
		                </li>
		            </ul>
		        </div> --}%
		    </div>
		</div>
		
		<div id="wrapper">
			<div class="content-wrapper" style="min-height:450px;">
				<div id="Content" class="container">
				<g:layoutBody/>
				</div>
			</div>	
		</div>
		
		%{-- <div class="footer" role="contentinfo">
			Powered by PT. Surya Manggala Informatika @ 2016
		</div>
		<div id="spinner" class="spinner" style="display:none;"><g:message code="spinner.alt" default="Loading&hellip;"/></div>
		 --}%
		<g:render template="/layouts/footer"/>														 
		<r:layoutResources />
	</body>
</html>
