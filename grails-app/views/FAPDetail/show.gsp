
<%@ page import="com.smanggin.FAPDetail" %>
<!doctype html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'FAPDetail.label', default: 'FAPDetail')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>

<section id="show-FAPDetail" class="first">
	<div class="row">
		<div class="col-lg-12">
			<div class="box box-primary">
				<div class="box-header with-border">
                  <h3 class="box-title"><g:message code="default.show.label" args="[entityName]" /></h3>
                </div><!--/.box-header with-border -->	
                <div class="box-body table-responsive">
					<table class="table table-striped">
						<tbody>
						
							<tr class="prop">
								<td valign="top" class="name"><g:message code="FAPDetail.reasonOfInvestment.label" default="Reason Of Investment" /></td>
								
								<td valign="top" class="value">${fieldValue(bean: FAPDetailInstance, field: "reasonOfInvestment")}</td>
								
							</tr>
						
							<tr class="prop">
								<td valign="top" class="name"><g:message code="FAPDetail.updatedBy.label" default="Updated By" /></td>
								
								<td valign="top" class="value">${fieldValue(bean: FAPDetailInstance, field: "updatedBy")}</td>
								
							</tr>
						
							<tr class="prop">
								<td valign="top" class="name"><g:message code="FAPDetail.oldFAP.label" default="Old FAP" /></td>
								
								<td valign="top" class="value"><g:link controller="FAP" action="show" id="${FAPDetailInstance?.oldFAP?.id}">${FAPDetailInstance?.oldFAP?.encodeAsHTML()}</g:link></td>
								
							</tr>
						
							<tr class="prop">
								<td valign="top" class="name"><g:message code="FAPDetail.newCareTaker.label" default="New Care Taker" /></td>
								
								<td valign="top" class="value"><g:link controller="user" action="show" id="${FAPDetailInstance?.newCareTaker?.id}">${FAPDetailInstance?.newCareTaker?.encodeAsHTML()}</g:link></td>
								
							</tr>
						
							<tr class="prop">
								<td valign="top" class="name"><g:message code="FAPDetail.remarks.label" default="Remarks" /></td>
								
								<td valign="top" class="value">${fieldValue(bean: FAPDetailInstance, field: "remarks")}</td>
								
							</tr>
						
							<tr class="prop">
								<td valign="top" class="name"><g:message code="FAPDetail.budget.label" default="Budget" /></td>
								
								<td valign="top" class="value"><g:link controller="budget" action="show" id="${FAPDetailInstance?.budget?.id}">${FAPDetailInstance?.budget?.encodeAsHTML()}</g:link></td>
								
							</tr>
						
							<tr class="prop">
								<td valign="top" class="name"><g:message code="FAPDetail.createdBy.label" default="Created By" /></td>
								
								<td valign="top" class="value">${fieldValue(bean: FAPDetailInstance, field: "createdBy")}</td>
								
							</tr>
						
							<tr class="prop">
								<td valign="top" class="name"><g:message code="FAPDetail.dateCreated.label" default="Date Created" /></td>
								
								<td valign="top" class="value"><g:formatDate date="${FAPDetailInstance?.dateCreated}" /></td>
								
							</tr>
						
							<tr class="prop">
								<td valign="top" class="name"><g:message code="FAPDetail.lastUpdated.label" default="Last Updated" /></td>
								
								<td valign="top" class="value"><g:formatDate date="${FAPDetailInstance?.lastUpdated}" /></td>
								
							</tr>
						
							<tr class="prop">
								<td valign="top" class="name"><g:message code="FAPDetail.qty.label" default="Qty" /></td>
								
								<td valign="top" class="value">${fieldValue(bean: FAPDetailInstance, field: "qty")}</td>
								
							</tr>
						
							<tr class="prop">
								<td valign="top" class="name"><g:message code="FAPDetail.reasonOfReplacement.label" default="Reason Of Replacement" /></td>
								
								<td valign="top" class="value"><g:formatBoolean boolean="${FAPDetailInstance?.reasonOfReplacement}" /></td>
								
							</tr>
						
							<tr class="prop">
								<td valign="top" class="name"><g:message code="FAPDetail.total.label" default="Total" /></td>
								
								<td valign="top" class="value">${fieldValue(bean: FAPDetailInstance, field: "total")}</td>
								
							</tr>
						
							<tr class="prop">
								<td valign="top" class="name"><g:message code="FAPDetail.unitprice.label" default="Unitprice" /></td>
								
								<td valign="top" class="value">${fieldValue(bean: FAPDetailInstance, field: "unitprice")}</td>
								
							</tr>
						
						</tbody>
					</table>
				</div><!--/.row -->
				<div class="box-footer clearfix">
						
				</div><!--/.box-footer clearfix -->
			</div><!--/.box-body table-responsive -->

			<g:render template="detail"/> 
		</div><!--/.box box-primary -->
	</div><!--/.row -->
</section>

</body>

</html>
