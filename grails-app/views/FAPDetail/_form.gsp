<%@ page import="com.smanggin.FAPDetail" %>



			<div class="form-group fieldcontain ${hasErrors(bean: FAPDetailInstance, field: 'reasonOfInvestment', 'error')} ">
				<label for="reasonOfInvestment" class="col-sm-3 control-label"><g:message code="FAPDetail.reasonOfInvestment.label" default="Reason Of Investment" /></label>
				<div class="col-sm-9">
					<g:textField name="reasonOfInvestment" class="form-control" value="${FAPDetailInstance?.reasonOfInvestment}"/>
					<span class="help-inline">${hasErrors(bean: FAPDetailInstance, field: 'reasonOfInvestment', 'error')}</span>
				</div>
			</div>

			<div class="form-group fieldcontain ${hasErrors(bean: FAPDetailInstance, field: 'updatedBy', 'error')} ">
				<label for="updatedBy" class="col-sm-3 control-label"><g:message code="FAPDetail.updatedBy.label" default="Updated By" /></label>
				<div class="col-sm-9">
					<g:textField name="updatedBy" class="form-control" value="${FAPDetailInstance?.updatedBy}"/>
					<span class="help-inline">${hasErrors(bean: FAPDetailInstance, field: 'updatedBy', 'error')}</span>
				</div>
			</div>

			<div class="form-group fieldcontain ${hasErrors(bean: FAPDetailInstance, field: 'oldFAP', 'error')} ">
				<label for="oldFAP" class="col-sm-3 control-label"><g:message code="FAPDetail.oldFAP.label" default="Old FAP" /></label>
				<div class="col-sm-9">
					<g:select id="oldFAP" name="oldFAP.id" from="${com.smanggin.FAP.list()}" optionKey="id" value="${FAPDetailInstance?.oldFAP?.id}" class="many-to-one form-control chosen-select" noSelection="['null': '']"/>
					<span class="help-inline">${hasErrors(bean: FAPDetailInstance, field: 'oldFAP', 'error')}</span>
				</div>
			</div>

			<div class="form-group fieldcontain ${hasErrors(bean: FAPDetailInstance, field: 'newCareTaker', 'error')} ">
				<label for="newCareTaker" class="col-sm-3 control-label"><g:message code="FAPDetail.newCareTaker.label" default="New Care Taker" /></label>
				<div class="col-sm-9">
					<g:select id="newCareTaker" name="newCareTaker.id" from="${com.smanggin.User.list()}" optionKey="id" value="${FAPDetailInstance?.newCareTaker?.id}" class="many-to-one form-control chosen-select" noSelection="['null': '']"/>
					<span class="help-inline">${hasErrors(bean: FAPDetailInstance, field: 'newCareTaker', 'error')}</span>
				</div>
			</div>

			<div class="form-group fieldcontain ${hasErrors(bean: FAPDetailInstance, field: 'remarks', 'error')} ">
				<label for="remarks" class="col-sm-3 control-label"><g:message code="FAPDetail.remarks.label" default="Remarks" /></label>
				<div class="col-sm-9">
					<g:textField name="remarks" class="form-control" value="${FAPDetailInstance?.remarks}"/>
					<span class="help-inline">${hasErrors(bean: FAPDetailInstance, field: 'remarks', 'error')}</span>
				</div>
			</div>

			<div class="form-group fieldcontain ${hasErrors(bean: FAPDetailInstance, field: 'budget', 'error')} required">
				<label for="budget" class="col-sm-3 control-label"><g:message code="FAPDetail.budget.label" default="Budget" /><span class="required-indicator">*</span></label>
				<div class="col-sm-9">
					<g:select id="budget" name="budget.id" from="${com.smanggin.Budget.list()}" optionKey="id" required="" value="${FAPDetailInstance?.budget?.id}" class="many-to-one form-control chosen-select"/>
					<span class="help-inline">${hasErrors(bean: FAPDetailInstance, field: 'budget', 'error')}</span>
				</div>
			</div>

			<div class="form-group fieldcontain ${hasErrors(bean: FAPDetailInstance, field: 'createdBy', 'error')} ">
				<label for="createdBy" class="col-sm-3 control-label"><g:message code="FAPDetail.createdBy.label" default="Created By" /></label>
				<div class="col-sm-9">
					<g:textField name="createdBy" class="form-control" value="${FAPDetailInstance?.createdBy}"/>
					<span class="help-inline">${hasErrors(bean: FAPDetailInstance, field: 'createdBy', 'error')}</span>
				</div>
			</div>

			<div class="form-group fieldcontain ${hasErrors(bean: FAPDetailInstance, field: 'qty', 'error')} required">
				<label for="qty" class="col-sm-3 control-label"><g:message code="FAPDetail.qty.label" default="Qty" /><span class="required-indicator">*</span></label>
				<div class="col-sm-9">
					<g:field type="number" name="qty" step="any" required="" value="${FAPDetailInstance.qty}"/>
					<span class="help-inline">${hasErrors(bean: FAPDetailInstance, field: 'qty', 'error')}</span>
				</div>
			</div>

			<div class="form-group fieldcontain ${hasErrors(bean: FAPDetailInstance, field: 'reasonOfReplacement', 'error')} ">
				<label for="reasonOfReplacement" class="col-sm-3 control-label"><g:message code="FAPDetail.reasonOfReplacement.label" default="Reason Of Replacement" /></label>
				<div class="col-sm-9">
					<bs:checkBox name="reasonOfReplacement" value="${FAPDetailInstance?.reasonOfReplacement}" />
					<span class="help-inline">${hasErrors(bean: FAPDetailInstance, field: 'reasonOfReplacement', 'error')}</span>
				</div>
			</div>

			<div class="form-group fieldcontain ${hasErrors(bean: FAPDetailInstance, field: 'total', 'error')} required">
				<label for="total" class="col-sm-3 control-label"><g:message code="FAPDetail.total.label" default="Total" /><span class="required-indicator">*</span></label>
				<div class="col-sm-9">
					<g:field type="number" name="total" step="any" required="" value="${FAPDetailInstance.total}"/>
					<span class="help-inline">${hasErrors(bean: FAPDetailInstance, field: 'total', 'error')}</span>
				</div>
			</div>

			<div class="form-group fieldcontain ${hasErrors(bean: FAPDetailInstance, field: 'unitprice', 'error')} required">
				<label for="unitprice" class="col-sm-3 control-label"><g:message code="FAPDetail.unitprice.label" default="Unitprice" /><span class="required-indicator">*</span></label>
				<div class="col-sm-9">
					<g:field type="number" name="unitprice" step="any" required="" value="${FAPDetailInstance.unitprice}"/>
					<span class="help-inline">${hasErrors(bean: FAPDetailInstance, field: 'unitprice', 'error')}</span>
				</div>
			</div>



