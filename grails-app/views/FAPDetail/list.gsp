
<%@ page import="com.smanggin.FAPDetail" %>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'FAPDetail.label', default: 'FAPDetail')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>
	
<section id="list-FAPDetail" class="first">
	<div class="row">
		<div class="col-lg-12">
			<div class="box box-primary">
				
				<div class="box-header with-border">
                  <h3 class="box-title"><g:message code="default.list.label" args="[entityName]" /></h3>
                </div><!--/.box-header with-border -->

				<div class="box-body table-responsive">	
					<table class="table table-bordered margin-top-medium">
						<thead>
							<tr>
							
								<g:sortableColumn property="reasonOfInvestment" title="${message(code: 'FAPDetail.reasonOfInvestment.label', default: 'Reason Of Investment')}" />
							
								<g:sortableColumn property="updatedBy" title="${message(code: 'FAPDetail.updatedBy.label', default: 'Updated By')}" />
							
								<th><g:message code="FAPDetail.oldFAP.label" default="Old FAP" /></th>
							
								<th><g:message code="FAPDetail.newCareTaker.label" default="New Care Taker" /></th>
							
								<g:sortableColumn property="remarks" title="${message(code: 'FAPDetail.remarks.label', default: 'Remarks')}" />
							
								<th><g:message code="FAPDetail.budget.label" default="Budget" /></th>
							
							</tr>
						</thead>
						<tbody>
						<g:each in="${FAPDetailInstanceList}" status="i" var="FAPDetailInstance">
							<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
							
								<td><g:link action="show" id="${FAPDetailInstance.id}">${fieldValue(bean: FAPDetailInstance, field: "reasonOfInvestment")}</g:link></td>
							
								<td>${fieldValue(bean: FAPDetailInstance, field: "updatedBy")}</td>
							
								<td>${fieldValue(bean: FAPDetailInstance, field: "oldFAP")}</td>
							
								<td>${fieldValue(bean: FAPDetailInstance, field: "newCareTaker")}</td>
							
								<td>${fieldValue(bean: FAPDetailInstance, field: "remarks")}</td>
							
								<td>${fieldValue(bean: FAPDetailInstance, field: "budget")}</td>
							
							</tr>
						</g:each>
						</tbody>
					</table>
				</div><!--/.box-body table-responsive -->

				<div class="box-footer clearfix">
					<bs:paginate total="${FAPDetailInstanceTotal}" />
				</div><!--/.box-footer clearfix -->
			</div><!--/.box box-primary -->	
		</div><!--/.col-lg-12 -->	
	</div><!--/.row -->			

</section>

</body>

</html>
