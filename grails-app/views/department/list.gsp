
<%@ page import="com.smanggin.Department" %>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'department.label', default: 'Department')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>
	
<section id="list-department" class="first">
	<div class="row">
		<div class="col-lg-12">
			<div class="box box-primary">
				
				<div class="box-header with-border">
                  <h3 class="box-title"><g:message code="default.list.label" args="[entityName]" /></h3>
                </div><!--/.box-header with-border -->

				<div class="box-body table-responsive">	
					<table class="table table-bordered margin-top-medium">
						<thead>
							<tr>
							
								<g:sortableColumn property="description" title="${message(code: 'department.description.label', default: 'Description')}" />
							
								<g:sortableColumn property="updateBy" title="${message(code: 'department.updateBy.label', default: 'Update By')}" />
							
								<g:sortableColumn property="createdBy" title="${message(code: 'department.createdBy.label', default: 'Created By')}" />
							
								<g:sortableColumn property="dateCreated" title="${message(code: 'department.dateCreated.label', default: 'Date Created')}" />
							
								<g:sortableColumn property="lastUpdated" title="${message(code: 'department.lastUpdated.label', default: 'Last Updated')}" />
							
								<g:sortableColumn property="name" title="${message(code: 'department.name.label', default: 'Name')}" />
							
							</tr>
						</thead>
						<tbody>
						<g:each in="${departmentInstanceList}" status="i" var="departmentInstance">
							<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
							
								<td><g:link action="show" id="${departmentInstance.id}">${fieldValue(bean: departmentInstance, field: "description")}</g:link></td>
							
								<td>${fieldValue(bean: departmentInstance, field: "updateBy")}</td>
							
								<td>${fieldValue(bean: departmentInstance, field: "createdBy")}</td>
							
								<td><g:formatDate date="${departmentInstance.dateCreated}" /></td>
							
								<td><g:formatDate date="${departmentInstance.lastUpdated}" /></td>
							
								<td>${fieldValue(bean: departmentInstance, field: "name")}</td>
							
							</tr>
						</g:each>
						</tbody>
					</table>
				</div><!--/.box-body table-responsive -->

				<div class="box-footer clearfix">
					<bs:paginate total="${departmentInstanceTotal}" />
				</div><!--/.box-footer clearfix -->
			</div><!--/.box box-primary -->	
		</div><!--/.col-lg-12 -->	
	</div><!--/.row -->			

</section>

</body>

</html>
