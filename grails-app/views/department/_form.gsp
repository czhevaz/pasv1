<%@ page import="com.smanggin.Department" %>

			<div class="form-group fieldcontain ${hasErrors(bean: departmentInstance, field: 'name', 'error')} ">
				<label for="name" class="col-sm-3 control-label"><g:message code="department.name.label" default="Name" /></label>
				<div class="col-sm-9">
					<g:textField name="name" class="form-control" value="${departmentInstance?.name}"/>
					<span class="help-inline">${hasErrors(bean: departmentInstance, field: 'name', 'error')}</span>
				</div>
			</div>


			<div class="form-group fieldcontain ${hasErrors(bean: departmentInstance, field: 'description', 'error')} ">
				<label for="description" class="col-sm-3 control-label"><g:message code="department.description.label" default="Description" /></label>
				<div class="col-sm-9">
					<g:textField name="description" class="form-control" value="${departmentInstance?.description}"/>
					<span class="help-inline">${hasErrors(bean: departmentInstance, field: 'description', 'error')}</span>
				</div>
			</div>





