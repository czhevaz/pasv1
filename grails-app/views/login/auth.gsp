<%@ page import="com.smanggin.ConnectDBService" %>
<%@ page import="com.smanggin.GlobalService" %>
<%
    def globalService = grailsApplication.classLoader.loadClass('com.smanggin.GlobalService').newInstance()
%>

<%
    def connectDB= grailsApplication.classLoader.loadClass('com.smanggin.ConnectDBService').newInstance()
    
%>

<html>
<head>
	<meta name='layout' content='main'/>
	<title><g:message code="springSecurity.login.title"/></title>
	<style type='text/css' media='screen'>
		.item{
		  margin: 3px;
		}
		.item img{
		  display: block;
		  width: 100%;
		  height: auto;
		  max-height: 450px;
		}
	</style>	
</head>

<body>
<div class="row">
	<div class="col-md-3">
		<g:if test="${!connectDB.getSqlProxyKalbeConnection(grailsApplication)}">
		 	<div class="alert alert-error alert-dismissible">
		        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		        <h4><i class="icon fa fa-ban"></i> Alert!</h4>
		        DB PROXY Can't Connect, Please Setting in Config! And Restart Tomcat
		    </div>
	    </g:if>
	    <g:else>
         <!-- Login box -->
        <div class="box box-primary">
        	%{-- <div class="box-header">
	    		<h6><g:message code="springSecurity.login.header"/></h6>
	    	</div> --}%
	        	 <!-- form start -->
            <auth:form authAction="login" success="[controller:'user', action:'postLogin']" 
				error="[controller:'login', action:'auth']" class="form-horizontal"  target="_parent" id='loginForm' class='cssform'>
			
	              <div class="box-body">
	              	<g:if test="${flash.authenticationFailure}">
						<div class="alert alert-error alert-dismissible">
                			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							Sorry, username and password not found.<br/>
						</div>
					</g:if>

					<g:if test="${flash.loginFormErrors}">
						<div class="alert alert-error alert-dismissible">
						<g:hasErrors bean="${flash.loginFormErrors}" field="login">
							<g:renderErrors bean="${flash.loginFormErrors}" as="list" field="login"/>
						</g:hasErrors>
						<g:hasErrors bean="${flash.loginFormErrors}" field="password">
							<g:renderErrors bean="${flash.loginFormErrors}" as="list" field="password"/>
						</g:hasErrors>
						</div>
					</g:if>
	                <div class="form-group">
	                  <label for="username"><g:message code="springSecurity.login.username.label"/></label>
	                  <input type="text" class="form-control" id="username" placeholder="Enter User ID" name="login">
	                </div>
	                <div class="form-group">
	                  <label for="password"><g:message code="springSecurity.login.password.label"/></label>
	                  <input type="password" class="form-control" id="password" placeholder="Password" name='password'>
	                </div>
	                
	              </div>
	              <!-- /.box-body -->

	              <div class="box-footer">
	              	<input type='submit' class="btn btn-primary" id="submit" value='${message(code:"springSecurity.login.button")}'/>       
	              </div>

            </auth:form>
            <!-- /.form -->
        </div>
        <!-- /.box -->
        </g:else>    
    </div> 
    <div class="col-md-9">
		<div class="row">
	        <div class="col-lg-12">
	          <div class="box box-success box-solid">
	            <div class="box-header with-border">
	              <h3 class="box-title">Information</h3>
	              <!-- /.box-tools -->
	            </div>
	            <!-- /.box-header -->
	            <div class="box-body">
	            <g:if test="${globalService.getInformation(true)?.size() == 1 }">
	            	<g:each in="${globalService.getInformation(true) as List}" var="content" status="i">
	              	
					  <div class="item"> <img src="${resource( dir: 'upload/informationPeriod',file: content.fileName)}" class="img-fluid" alt="Responsive image"></div>
					</g:each>  

	            </g:if>
	            <g:else>
	            
	              	<div id="owl-demo"  class="owl-carousel">
	              
	              	<g:each in="${globalService.getInformation(true) as List}" var="content" status="i">
	              	
					  <div class="item"> <img src="${resource( dir: 'upload/informationPeriod',file: content.fileName)}" class="img-fluid" alt="Responsive image"></div>
					</g:each>  
					 
					</div>
				</g:else>	
	            </div>

	            <!-- /.box-body -->
	          </div>
	          <!-- /.box -->
	        </div>
	    </div>
	    <div class="row">
	        <div class="col-lg-12">
	          <div class="box box-success box-solid">
	            
	            <!-- /.box-header -->
	            <div class="box-body">
	            <g:if test="${globalService.getInformation(false)?.size() == 1 }">
	            	<g:each in="${globalService.getInformation(false) as List}" var="content" status="i">
	              	
					  <div class="item"> <img src="${resource( dir: 'upload/informationPeriod',file: content.fileName)}" class="img" alt="Responsive image"></div>
					</g:each>  

	            </g:if>
	            <g:else>
	            
	              	<div id="owl-demo"  class="owl-carousel">
	              
	              	<g:each in="${globalService.getInformation(false) as List}" var="content" status="i">
	              	
					  <div class="item"> <img src="${resource( dir: 'upload/informationPeriod',file: content.fileName)}" class="img-fluid" alt="Responsive image"></div>
					</g:each>  
					 
					</div>
				</g:else>	
	            </div>

	            <!-- /.box-body -->
	          </div>
	          <!-- /.box -->
	        </div>
	    </div>
	</div>
</div>

%{-- <div class='inner'>
	<div class="box box-primary box-solid">
        <div class="box-header">
          <h3 class="box-title">Information</h3>
        </div>
        <div class="box-body" id="chat-box">
        <g:infoLogin  />
        
        </div>
        <!-- /.box-body -->
        <!-- Loading (remove the following to stop the loading)-->
        <div class="overlay" style="display:none;">
          <i class="fa fa-refresh fa-spin"></i>
        </div>
        <!-- end loading -->
    </div>
</div> --}%
<r:script>
	<!--
	(function() {
		document.forms['loginForm'].elements['login'].focus();
	})();
	// -->
	$(document).ready(function(){
	 // $(".owl-carousel").owlCarousel();
	  var owl = $('.owl-carousel');
              owl.owlCarousel({
                items: 1,
                singleItem: true,
                lazyLoad : true,
                loop: true,
                margin: 10,
                autoplay: true,
                autoplayTimeout: 6000,
                autoplayHoverPause: true
              });
	});
	
</r:script>
</body>
</html>
