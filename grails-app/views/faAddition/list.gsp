
<%@ page import="com.smanggin.FaAddition" %>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'faAddition.label', default: 'FaAddition')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>
	
<section id="list-faAddition" class="first">
	<div class="row">
		<div class="col-lg-12">
			<div class="box box-primary">
				
				<div class="box-header with-border">
                  <h3 class="box-title"><g:message code="default.list.label" args="[entityName]" /></h3>
                </div><!--/.box-header with-border -->

				<div class="box-body table-responsive">	
					<table class="table table-bordered margin-top-medium">
						<thead>
							<tr>
							
								<g:sortableColumn property="country" title="${message(code: 'faAddition.country.label', default: 'Country')}" />
							
								<g:sortableColumn property="createdBy" title="${message(code: 'faAddition.createdBy.label', default: 'Created By')}" />
							
								<g:sortableColumn property="dateCreated" title="${message(code: 'faAddition.dateCreated.label', default: 'Date Created')}" />
							
								<g:sortableColumn property="fAAddDate" title="${message(code: 'faAddition.fAAddDate.label', default: 'FAA dd Date')}" />
							
								<g:sortableColumn property="lastUpdated" title="${message(code: 'faAddition.lastUpdated.label', default: 'Last Updated')}" />
							
								<g:sortableColumn property="month" title="${message(code: 'faAddition.month.label', default: 'Month')}" />
							
							</tr>
						</thead>
						<tbody>
						<g:each in="${faAdditionInstanceList}" status="i" var="faAdditionInstance">
							<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
							
								<td><g:link action="show" id="${faAdditionInstance.id}">${fieldValue(bean: faAdditionInstance, field: "country")}</g:link></td>
							
								<td>${fieldValue(bean: faAdditionInstance, field: "createdBy")}</td>
							
								<td><g:formatDate date="${faAdditionInstance.dateCreated}" /></td>
							
								<td><g:formatDate date="${faAdditionInstance.fAAddDate}" /></td>
							
								<td><g:formatDate date="${faAdditionInstance.lastUpdated}" /></td>
							
								<td>${fieldValue(bean: faAdditionInstance, field: "month")}</td>
							
							</tr>
						</g:each>
						</tbody>
					</table>
				</div><!--/.box-body table-responsive -->

				<div class="box-footer clearfix">
					<bs:paginate total="${faAdditionInstanceTotal}" />
				</div><!--/.box-footer clearfix -->
			</div><!--/.box box-primary -->	
		</div><!--/.col-lg-12 -->	
	</div><!--/.row -->			

</section>

</body>

</html>
