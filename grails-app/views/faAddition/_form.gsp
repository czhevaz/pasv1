<%@ page import="com.smanggin.FaAddition" %>


			<div class="form-group fieldcontain ${hasErrors(bean: faAdditionInstance, field: 'country', 'error')} ">
				<label for="country" class="col-sm-3 control-label"><g:message code="faAddition.country.label" default="Country" /></label>
				<div class="col-sm-3">
					<g:select id="country" name="country" from="${com.smanggin.Country.list()}" optionKey="name" required="" value="${faAdditionInstance?.country}" class="many-to-one form-control chosen-select" />
					<span class="help-inline">${hasErrors(bean: faAdditionInstance, field: 'country', 'error')}</span>
				</div>
			</div>

			<div class="form-group fieldcontain ${hasErrors(bean: faAdditionInstance, field: 'transactionGroup', 'error')} required">
				<label for="transactionGroup" class="col-sm-3 control-label"><g:message code="faAddition.transactionGroup.label" default="Transaction Group" /><span class="required-indicator">*</span></label>
				<div class="col-sm-3">
					<g:select id="transactionGroup" name="transactionGroup.id" from="${com.smanggin.TransactionGroup.list()}" optionKey="id" required="" value="${faAdditionInstance?.transactionGroup?.id}" class="many-to-one form-control chosen-select"/>
					<span class="help-inline">${hasErrors(bean: faAdditionInstance, field: 'transactionGroup', 'error')}</span>
				</div>
			</div>

			<div class="form-group fieldcontain ${hasErrors(bean: faAdditionInstance, field: 'number', 'error')} ">
				<label for="number" class="col-sm-3 control-label"><g:message code="faAddition.number.label" default="Number" /></label>
				<div class="col-sm-3">
					<g:textField name="number" class="form-control" value="${faAdditionInstance?.number}" readonly="true"/>
					<span class="help-inline">${hasErrors(bean: faAdditionInstance, field: 'number', 'error')}</span>
				</div>
			</div>

			<div class="form-group fieldcontain ${hasErrors(bean: faAdditionInstance, field: 'fAAddDate', 'error')} required">
				<label for="fAAddDate" class="col-sm-3 control-label"><g:message code="faAddition.fAAddDate.label" default="FAA dd Date" /><span class="required-indicator">*</span></label>
				
					<g:jqDatePicker name="faAddDate" precision="day"  value="${faAdditionInstance?.faAddDate}" data-date-format="yyyy-mm-dd" />
					<span class="help-inline">${hasErrors(bean: faAdditionInstance, field: 'fAAddDate', 'error')}</span>
				
			</div>

			
			
			<div class="form-group fieldcontain ${hasErrors(bean: faAdditionInstance, field: 'purchaseOrder', 'error')} required">
				<label for="purchaseOrder" class="col-sm-3 control-label"><g:message code="faAddition.purchaseOrder.label" default="Purchase Order" /><span class="required-indicator">*</span></label>
				<div class="col-sm-3">
					<g:select id="purchaseOrder" name="purchaseOrder.id" from="${com.smanggin.PurchaseOrder.list()}" optionKey="id" required="" value="${faAdditionInstance?.purchaseOrder?.id}" class="many-to-one form-control chosen-select"/>
					<span class="help-inline">${hasErrors(bean: faAdditionInstance, field: 'purchaseOrder', 'error')}</span>
				</div>
			</div>

			
			


