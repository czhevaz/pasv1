
<%@ page import="com.smanggin.FaAddition" %>
<!doctype html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'faAddition.label', default: 'FaAddition')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>

<section id="show-faAddition" class="first">
	<div class="row">
		<div class="col-lg-12">
			<div class="box box-primary">
				<div class="box-header with-border">
                  <h3 class="box-title"><g:message code="default.show.label" args="[entityName]" /></h3>
                </div><!--/.box-header with-border -->	
                <div class="box-body table-responsive">
					<table class="table table-striped">
						<tbody>
						
							<tr class="prop">
								<td valign="top" class="name"><g:message code="faAddition.country.label" default="Country" /></td>
								
								<td valign="top" class="value">${fieldValue(bean: faAdditionInstance, field: "country")}</td>
								
							</tr>

							<tr class="prop">
								<td valign="top" class="name"><g:message code="faAddition.transactionGroup.label" default="Transaction Group" /></td>
								
								<td valign="top" class="value"><g:link controller="transactionGroup" action="show" id="${faAdditionInstance?.transactionGroup?.id}">${faAdditionInstance?.transactionGroup?.encodeAsHTML()}</g:link></td>
								
							</tr>


							<tr class="prop">
								<td valign="top" class="name"><g:message code="faAddition.number.label" default="Number" /></td>
								
								<td valign="top" class="value">${fieldValue(bean: faAdditionInstance, field: "number")}</td>
								
							</tr>


							<tr class="prop">
								<td valign="top" class="name"><g:message code="faAddition.fAAddDate.label" default="FA Addition Date" /></td>
								
								<td valign="top" class="value"><g:formatDate date="${faAdditionInstance?.faAddDate}" /></td>
								
							</tr>

							<tr class="prop">
								<td valign="top" class="name"><g:message code="faAddition.purchaseOrder.label" default="Purchase Order" /></td>
								
								<td valign="top" class="value"><g:link controller="purchaseOrder" action="show" id="${faAdditionInstance?.purchaseOrder?.id}">${faAdditionInstance?.purchaseOrder?.encodeAsHTML()}</g:link></td>
								
							</tr>
						
							
							<tr class="prop">
								<td valign="top" class="name"><g:message code="faAddition.createdBy.label" default="Created By" /></td>
								
								<td valign="top" class="value">${fieldValue(bean: faAdditionInstance, field: "createdBy")}</td>
								
							</tr>
						
							<tr class="prop">
								<td valign="top" class="name"><g:message code="faAddition.dateCreated.label" default="Date Created" /></td>
								
								<td valign="top" class="value"><g:formatDate date="${faAdditionInstance?.dateCreated}" /></td>
								
							</tr>
						
							<tr class="prop">
								<td valign="top" class="name"><g:message code="faAddition.updatedBy.label" default="Updated By" /></td>
								
								<td valign="top" class="value">${fieldValue(bean: faAdditionInstance, field: "updatedBy")}</td>
								
							</tr>


							<tr class="prop">
								<td valign="top" class="name"><g:message code="faAddition.lastUpdated.label" default="Last Updated" /></td>
								
								<td valign="top" class="value"><g:formatDate date="${faAdditionInstance?.lastUpdated}" /></td>
								
							</tr>
						

						
						</tbody>
					</table>
				</div><!--/.row -->
				<div class="box-footer clearfix">
						
				</div><!--/.box-footer clearfix -->
			</div><!--/.box-body table-responsive -->

			<g:render template="detail"/> 
		</div><!--/.box box-primary -->
	</div><!--/.row -->
</section>

</body>

</html>
