<%@ page import="com.smanggin.FaAddition" %>

				<%
if(actionName=='edit' || actionName=='show') { 
%>
<div class="easyui-tabs table" style="height:300px">
    
        <div title='<g:message code="faAddition.faAdditionDetails.label" default="Fa Addition Details" />' style="padding:10px">

            <table id="dg-faAdditionDetails" class="easyui-datagrid" style="height:240px"
            data-options="singleSelect:true, 
            collapsible:true, 
            onClickRow: faAdditionDetailsOnClickRow,
            toolbar: '#tb-faAdditionDetails',
            url:'/${meta(name:'app.name')}/faAdditionDetail/jlist?masterField.name=faAddition&masterField.id=${faAdditionInstance?.id}'">
                <thead>
                    <tr>
                    
                        
                        <th data-options="field:'updatedBy',width:200,editor:'text'">Updated By</th>
                        

                    
                        
                        <th data-options="field:'createdBy',width:200,editor:'text'">Created By</th>
                        

                    
                        <th data-options="field:'faAdditionId',hidden:true">Fa Addition</th>
                                 
                        
                        <th data-options="field:'faName',width:200,editor:'text'">Fa Name</th>
                        

                    
                        
                        <th data-options="field:'faNumber',width:200,editor:'text'">Fa Number</th>
                        

                    
                        <th data-options="field:'purchaseOrderDetailId',width:200,
                            formatter:function(value,row){
                                return row.purchaseOrderDetailName;
                            },
                            editor:{
                                type:'combobox',
                                options:{
                                    valueField:'id',
                                    textField:'name',
                                    url:'/${meta(name:'app.name')}/purchaseOrderDetail/jlist',
                                    required:true,
                                }
                        }">Purchase Order Detail</th>
                                    
                        
                        <th data-options="field:'qty',align:'right',formatter:formatNumber,  width:100,editor:{type:'numberbox',options:{precision:2}}">Qty</th>
                        

                    
                    </tr>
                </thead>
            </table>
        </div> 

        
    
        
    
        
    
        
    
        
    
        
    
        
    
</div>
    

        <div id="tb-faAdditionDetails" style="height:auto">
            <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:false" onclick="faAdditionDetailsAppend()">Add</a>
            <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:false" onclick="faAdditionDetailsRemoveit()">Remove</a>
            <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-save',plain:false" onclick="faAdditionDetailsAccept()">Save</a>
            <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-reload',plain:false" onclick="faAdditionDetailsRefresh()">Refresh</a>
        </div>
            
        <r:script>     
            var editIndex = undefined;
            function faAdditionDetailsEndEditing(){
                if (editIndex == undefined){return true}
                if ($('#dg-faAdditionDetails').datagrid('validateRow', editIndex)){

        
                    //purchaseOrderDetailName
                    var ed = $('#dg-faAdditionDetails').datagrid('getEditor', {index:editIndex,field:'purchaseOrderDetailId'});
                    var purchaseOrderDetailName = $(ed.target).combobox('getText');
                    $('#dg-faAdditionDetails').datagrid('getRows')[editIndex]['purchaseOrderDetailName'] = purchaseOrderDetailName;
                    
        

                    $('#dg-faAdditionDetails').datagrid('endEdit', editIndex);
                    var row = $('#dg-faAdditionDetails').datagrid('getRows')[editIndex]
                    $.ajax({
                      type: "POST",
                      url: "/${meta(name:'app.name')}/faAdditionDetail/jsave",
                      data: row,
                      success: function(data){ 
                          if(!data.success)
                          {
                            alert(data.messages.errors[0].message)
                          }
                      },
                      dataType: 'json'
                    });
                    editIndex = undefined;
                    return true;
                } else {
                    return false;
                }
            }
            function faAdditionDetailsOnClickRow(index){
                if (editIndex != index){
                    if (faAdditionDetailsEndEditing()){
                        $('#dg-faAdditionDetails').datagrid('selectRow', index)
                                .datagrid('beginEdit', index);
                        editIndex = index;
                    } else {
                        $('#dg-faAdditionDetails').datagrid('selectRow', editIndex);
                    }
                }
            }
            function faAdditionDetailsAppend(){
                if (faAdditionDetailsEndEditing()){
                    $('#dg-faAdditionDetails').datagrid('appendRow',
                    {faAdditionId: ${faAdditionInstance.id? faAdditionInstance.id : 0} });
                    editIndex = $('#dg-faAdditionDetails').datagrid('getRows').length-1;
                    $('#dg-faAdditionDetails').datagrid('selectRow', editIndex).datagrid('beginEdit', editIndex);
                }
            }
            function faAdditionDetailsRemoveit(){
                if (editIndex == undefined){return}
                if (!confirm('Are you sure to delete this record?')){ return }

                var row = $('#dg-faAdditionDetails').datagrid('getRows')[editIndex]
                console.log(row)

                $('#dg-faAdditionDetails').datagrid('cancelEdit', editIndex)
                        .datagrid('deleteRow', editIndex);

                $.ajax({
                  type: "POST",
                  url: "/${meta(name:'app.name')}/faAdditionDetail/jdelete/" + row['id'],
                  data: row,
                  success: function(data){ 
                      if(!data.success)
                      {
                            alert(data.messages)
                      }
                  },
                  dataType: 'json'
                });             
                editIndex = undefined;
            }
            function faAdditionDetailsAccept(){
                if (faAdditionDetailsEndEditing()){
                    $('#dg-faAdditionDetails').datagrid('acceptChanges');
                }
            }

            function faAdditionDetailsRefresh(){
                $('#dg-faAdditionDetails').datagrid('reload');
                editIndex = undefined;
            }
        </r:script>  
    

    

    

    

    

    

    


<%
}
%>
