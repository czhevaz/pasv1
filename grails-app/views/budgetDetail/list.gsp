
<%@ page import="com.smanggin.BudgetDetail" %>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'budgetDetail.label', default: 'BudgetDetail')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>
	
<section id="list-budgetDetail" class="first">
	<div class="row">
		<div class="col-lg-12">
			<div class="box box-primary">
				
				<div class="box-header with-border">
                  <h3 class="box-title"><g:message code="default.list.label" args="[entityName]" /></h3>
                </div><!--/.box-header with-border -->

				<div class="box-body table-responsive">	
					<table class="table table-bordered margin-top-medium">
						<thead>
							<tr>
							
								<g:sortableColumn property="updatedBy" title="${message(code: 'budgetDetail.updatedBy.label', default: 'Updated By')}" />
							
								<th><g:message code="budgetDetail.budget.label" default="Budget" /></th>
							
								<th><g:message code="budgetDetail.categoryAsset.label" default="Category Asset" /></th>
							
								<th><g:message code="budgetDetail.coa.label" default="Coa" /></th>
							
								<g:sortableColumn property="createdBy" title="${message(code: 'budgetDetail.createdBy.label', default: 'Created By')}" />
							
								<g:sortableColumn property="dateCreated" title="${message(code: 'budgetDetail.dateCreated.label', default: 'Date Created')}" />
							
							</tr>
						</thead>
						<tbody>
						<g:each in="${budgetDetailInstanceList}" status="i" var="budgetDetailInstance">
							<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
							
								<td><g:link action="show" id="${budgetDetailInstance.id}">${fieldValue(bean: budgetDetailInstance, field: "updatedBy")}</g:link></td>
							
								<td>${fieldValue(bean: budgetDetailInstance, field: "budget")}</td>
							
								<td>${fieldValue(bean: budgetDetailInstance, field: "categoryAsset")}</td>
							
								<td>${fieldValue(bean: budgetDetailInstance, field: "coa")}</td>
							
								<td>${fieldValue(bean: budgetDetailInstance, field: "createdBy")}</td>
							
								<td><g:formatDate date="${budgetDetailInstance.dateCreated}" /></td>
							
							</tr>
						</g:each>
						</tbody>
					</table>
				</div><!--/.box-body table-responsive -->

				<div class="box-footer clearfix">
					<bs:paginate total="${budgetDetailInstanceTotal}" />
				</div><!--/.box-footer clearfix -->
			</div><!--/.box box-primary -->	
		</div><!--/.col-lg-12 -->	
	</div><!--/.row -->			

</section>

</body>

</html>
