
<%@ page import="com.smanggin.BudgetDetail" %>
<!doctype html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'budgetDetail.label', default: 'BudgetDetail')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>

<section id="show-budgetDetail" class="first">
	<div class="row">
		<div class="col-lg-12">
			<div class="box box-primary">
				<div class="box-header with-border">
                  <h3 class="box-title"><g:message code="default.show.label" args="[entityName]" /></h3>
                </div><!--/.box-header with-border -->	
                <div class="box-body table-responsive">
					<table class="table table-striped">
						<tbody>
						
							<tr class="prop">
								<td valign="top" class="name"><g:message code="budgetDetail.updatedBy.label" default="Updated By" /></td>
								
								<td valign="top" class="value">${fieldValue(bean: budgetDetailInstance, field: "updatedBy")}</td>
								
							</tr>
						
							<tr class="prop">
								<td valign="top" class="name"><g:message code="budgetDetail.budget.label" default="Budget" /></td>
								
								<td valign="top" class="value"><g:link controller="budget" action="show" id="${budgetDetailInstance?.budget?.id}">${budgetDetailInstance?.budget?.encodeAsHTML()}</g:link></td>
								
							</tr>
						
							<tr class="prop">
								<td valign="top" class="name"><g:message code="budgetDetail.categoryAsset.label" default="Category Asset" /></td>
								
								<td valign="top" class="value"><g:link controller="categoryAsset" action="show" id="${budgetDetailInstance?.categoryAsset?.id}">${budgetDetailInstance?.categoryAsset?.encodeAsHTML()}</g:link></td>
								
							</tr>
						
							<tr class="prop">
								<td valign="top" class="name"><g:message code="budgetDetail.coa.label" default="Coa" /></td>
								
								<td valign="top" class="value"><g:link controller="chartOfAccount" action="show" id="${budgetDetailInstance?.coa?.id}">${budgetDetailInstance?.coa?.encodeAsHTML()}</g:link></td>
								
							</tr>
						
							<tr class="prop">
								<td valign="top" class="name"><g:message code="budgetDetail.createdBy.label" default="Created By" /></td>
								
								<td valign="top" class="value">${fieldValue(bean: budgetDetailInstance, field: "createdBy")}</td>
								
							</tr>
						
							<tr class="prop">
								<td valign="top" class="name"><g:message code="budgetDetail.dateCreated.label" default="Date Created" /></td>
								
								<td valign="top" class="value"><g:formatDate date="${budgetDetailInstance?.dateCreated}" /></td>
								
							</tr>
						
							<tr class="prop">
								<td valign="top" class="name"><g:message code="budgetDetail.lastUpdated.label" default="Last Updated" /></td>
								
								<td valign="top" class="value"><g:formatDate date="${budgetDetailInstance?.lastUpdated}" /></td>
								
							</tr>
						
							<tr class="prop">
								<td valign="top" class="name"><g:message code="budgetDetail.qty.label" default="Qty" /></td>
								
								<td valign="top" class="value">${fieldValue(bean: budgetDetailInstance, field: "qty")}</td>
								
							</tr>
						
							<tr class="prop">
								<td valign="top" class="name"><g:message code="budgetDetail.uom.label" default="Uom" /></td>
								
								<td valign="top" class="value">${fieldValue(bean: budgetDetailInstance, field: "uom")}</td>
								
							</tr>
						
							<tr class="prop">
								<td valign="top" class="name"><g:message code="budgetDetail.value.label" default="Value" /></td>
								
								<td valign="top" class="value">${fieldValue(bean: budgetDetailInstance, field: "value")}</td>
								
							</tr>
						
						</tbody>
					</table>
				</div><!--/.row -->
				<div class="box-footer clearfix">
						
				</div><!--/.box-footer clearfix -->
			</div><!--/.box-body table-responsive -->

			<g:render template="detail"/> 
		</div><!--/.box box-primary -->
	</div><!--/.row -->
</section>

</body>

</html>
