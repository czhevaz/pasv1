<%@ page import="com.smanggin.BudgetDetail" %>



			<div class="form-group fieldcontain ${hasErrors(bean: budgetDetailInstance, field: 'updatedBy', 'error')} ">
				<label for="updatedBy" class="col-sm-3 control-label"><g:message code="budgetDetail.updatedBy.label" default="Updated By" /></label>
				<div class="col-sm-9">
					<g:textField name="updatedBy" class="form-control" value="${budgetDetailInstance?.updatedBy}"/>
					<span class="help-inline">${hasErrors(bean: budgetDetailInstance, field: 'updatedBy', 'error')}</span>
				</div>
			</div>

			<div class="form-group fieldcontain ${hasErrors(bean: budgetDetailInstance, field: 'budget', 'error')} required">
				<label for="budget" class="col-sm-3 control-label"><g:message code="budgetDetail.budget.label" default="Budget" /><span class="required-indicator">*</span></label>
				<div class="col-sm-9">
					<g:select id="budget" name="budget.id" from="${com.smanggin.Budget.list()}" optionKey="id" required="" value="${budgetDetailInstance?.budget?.id}" class="many-to-one form-control chosen-select"/>
					<span class="help-inline">${hasErrors(bean: budgetDetailInstance, field: 'budget', 'error')}</span>
				</div>
			</div>

			<div class="form-group fieldcontain ${hasErrors(bean: budgetDetailInstance, field: 'categoryAsset', 'error')} required">
				<label for="categoryAsset" class="col-sm-3 control-label"><g:message code="budgetDetail.categoryAsset.label" default="Category Asset" /><span class="required-indicator">*</span></label>
				<div class="col-sm-9">
					<g:select id="categoryAsset" name="categoryAsset.id" from="${com.smanggin.CategoryAsset.list()}" optionKey="id" required="" value="${budgetDetailInstance?.categoryAsset?.id}" class="many-to-one form-control chosen-select"/>
					<span class="help-inline">${hasErrors(bean: budgetDetailInstance, field: 'categoryAsset', 'error')}</span>
				</div>
			</div>

			<div class="form-group fieldcontain ${hasErrors(bean: budgetDetailInstance, field: 'coa', 'error')} required">
				<label for="coa" class="col-sm-3 control-label"><g:message code="budgetDetail.coa.label" default="Coa" /><span class="required-indicator">*</span></label>
				<div class="col-sm-9">
					<g:select id="coa" name="coa.id" from="${com.smanggin.ChartOfAccount.list()}" optionKey="id" required="" value="${budgetDetailInstance?.coa?.id}" class="many-to-one form-control chosen-select"/>
					<span class="help-inline">${hasErrors(bean: budgetDetailInstance, field: 'coa', 'error')}</span>
				</div>
			</div>

			<div class="form-group fieldcontain ${hasErrors(bean: budgetDetailInstance, field: 'createdBy', 'error')} ">
				<label for="createdBy" class="col-sm-3 control-label"><g:message code="budgetDetail.createdBy.label" default="Created By" /></label>
				<div class="col-sm-9">
					<g:textField name="createdBy" class="form-control" value="${budgetDetailInstance?.createdBy}"/>
					<span class="help-inline">${hasErrors(bean: budgetDetailInstance, field: 'createdBy', 'error')}</span>
				</div>
			</div>

			<div class="form-group fieldcontain ${hasErrors(bean: budgetDetailInstance, field: 'qty', 'error')} required">
				<label for="qty" class="col-sm-3 control-label"><g:message code="budgetDetail.qty.label" default="Qty" /><span class="required-indicator">*</span></label>
				<div class="col-sm-9">
					<g:field type="number" name="qty" step="any" required="" value="${budgetDetailInstance.qty}"/>
					<span class="help-inline">${hasErrors(bean: budgetDetailInstance, field: 'qty', 'error')}</span>
				</div>
			</div>

			<div class="form-group fieldcontain ${hasErrors(bean: budgetDetailInstance, field: 'uom', 'error')} ">
				<label for="uom" class="col-sm-3 control-label"><g:message code="budgetDetail.uom.label" default="Uom" /></label>
				<div class="col-sm-9">
					<g:textField name="uom" class="form-control" value="${budgetDetailInstance?.uom}"/>
					<span class="help-inline">${hasErrors(bean: budgetDetailInstance, field: 'uom', 'error')}</span>
				</div>
			</div>

			<div class="form-group fieldcontain ${hasErrors(bean: budgetDetailInstance, field: 'value', 'error')} required">
				<label for="value" class="col-sm-3 control-label"><g:message code="budgetDetail.value.label" default="Value" /><span class="required-indicator">*</span></label>
				<div class="col-sm-9">
					<g:field type="number" name="value" step="any" required="" value="${budgetDetailInstance.value}"/>
					<span class="help-inline">${hasErrors(bean: budgetDetailInstance, field: 'value', 'error')}</span>
				</div>
			</div>



