<%@ page import="com.smanggin.AppSetting" %>
<!doctype html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'faCodeSetting.label', default: 'AppSetting')}" />
	<title><g:message code="default.create.label" args="[entityName]" /></title>
</head>

<body>

<section id="create-appSetting" class="first">
	<div class="row">
		<div class="col-lg-12">
			<div class="box box-primary">
				<div class="box-header with-border">
    				<h3 class="box-title">FA Code Setting</h3>

    			</div><!--/.box-header with-border -->	
				
				<g:form action="update" class="form-horizontal" >
					
					<div class="box-body">
					<g:textField name="val" class="form-control" readonly="true" style="width:98.8%;font-size:20px;height:60px" value="${faCodeFormat?.val}"/>	
							
						<br/>
						<br/>
							<g:hiddenField name="id" value="${faCodeFormat?.id}" />
							<g:hiddenField name="code" value="${faCodeFormat?.code}"/>
							<g:hiddenField name="separatorId" value="${facodeSepartor?.id}" />

						<fieldset class="form">
							<div class="form-group fieldcontain  ">
								<label for="separator" class="col-sm-2 control-label"><g:message code="appSettingInstance.separator.label" default="Separator" /></label>
								<div class="col-sm-1">
								<g:select id="separator" name="separator" from="${separators}" value="${facodeSepartor?.val}" class="many-to-one form-control" onchange="updateNumberFormat()" />
								</div>
							</div>
							<g:each in="${(1..5)}" var="i" >
								<div class="form-group fieldcontain  ">
									<label for="segment1" class="col-sm-2 control-label"><g:message code="appSettingInstance.segment1.label" default="Segment ${i}" /></label>
									<div class="row">
										<div class="col-sm-2">
											<g:select id="segment_${i}" name="segment_${i}" from="${dataTypeMap}" optionKey="key" optionValue="value"  noSelection="${['':'']}"  class="many-to-one form-control faCodeFormat" onkeyup="freeTextEach(${i})" value="${listVal[i-1]?.type}"/>
										</div>
										
										<div class="col-sm-2 " style="display:${listVal[i-1]?.type == 'freeText'?'block':'none'}" id="freeText_label_div_${i}">
											<g:textField name="freeText_label_${i}"  id="freeText_label_${i}" value=""  class="form-control" placeholder="Label Name"   onkeyup="freeTextEach(${i})"  value ="${listVal[i-1]?.type == 'freeText'?listVal[i-1]?.label:''}"/>
										</div >
										<div class="col-sm-2" style="display:${listVal[i-1]?.type == 'freeText'?'block':'none'}" id="freeText_dataType_div_${i}">
											<g:select id="freeText_dataType_${i}" name="freeText_dataType_${i}" from="${['varchar','numeric']}"   noSelection="${['':'']}" class="many-to-one form-control dataType"  onchange="freeTextEach(${i})" value="${listVal[i-1]?.type == 'freeText'?listVal[i-1]?.dataType:''}"/>
										</div>
				            			<div class="col-sm-2" style="display:${listVal[i-1]?.type == 'freeText' && listVal[i-1]?.dataType == 'varchar'?'block':'none'}" id="freeText_length_div_${i}">
				            				<g:field type="number" name="freeText_length_${i}"  id="freeText_length_${i}" value="${listVal[i-1]?.type == 'freeText'?listVal[i-1]?.length:''}"  class="form-control" placeholder="Max Length"  onkeyup="freeTextEach(${i})" />
				            			</div>
				            			<div class="col-sm-2" style="display:${listVal[i-1]?.type == 'timeStamps'?'block':'none'}" id="timeStamps_div_${i}">
											<g:select id="timeStamps_${i}" name="timeStamps_${i}" from="${formatDate}"  noSelection="${['':'']}"  class="many-to-one form-control" onchange="timeStampsEach(${i})" value="${listVal[i-1]?.type == 'timeStamps'?listVal[i-1]?.formatDate:''}" />
										</div>
										
				            			<div class="col-sm-2" style="display:${listVal[i-1]?.type == 'runningNo'?'block':'none'}" id="runningNo_length_div_${i}">
				            				<g:field type="number" name="runningNo_length_${i}"  id="runningNo_length_${i}" value=""  class="form-control" placeholder="Width" onkeyup="runningNoEach(${i})" value="${listVal[i-1]?.type == 'runningNo'?listVal[i-1]?.length:''}" />
				            			</div>

				            			<div class="col-sm-2" style="display:${listVal[i-1]?.type == 'runningNo'?'block':'none'}" id="runningNo_length_div_${i}">
				            				<g:field type="number" name="runningNo_length_${i}"  id="runningNo_length_${i}" value=""  class="form-control" placeholder="Width" onkeyup="runningNoEach(${i})" value="${listVal[i-1]?.type == 'runningNo'?listVal[i-1]?.length:''}" />
				            			</div>

				            			<div class="col-sm-2" style="display:${listVal[i-1]?.type == 'tables'?'block':'none'}" id="tables_div_${i}">
											<g:select id="tables_${i}" name="tables_${i}" from="${tableMaster}"  noSelection="${['':'']}"  class="many-to-one form-control"  value="" onchange="tablesEach(${i})" />
										</div>

										<div class="col-sm-2" style="display:none" id="fieldTable_div_${i}">
											<g:select id="fieldTable_${i}" name="fieldTable_${i}" from="[]"  noSelection="${['':'']}"  class="many-to-one form-control"  value=""  />
										</div>

				            			<g:hiddenField name="formatField_${i}"  id="formatField_${i}" value="${listVal[i-1]?.val}"/>
									</div>
								</div>

							</g:each>
						</fieldset>
					</div><!--/.box-body -->
					<div class="box-footer">
						<div class="form-actions">
							<g:submitButton name="create" class="btn btn-primary" value="${message(code: 'default.button.create.label', default: 'Create')}" />
				            <button class="btn" type="reset"><g:message code="default.button.reset.label" default="Reset" /></button>
						</div>
					</div><!--/.box-footer -->	
				</g:form>
			</div><!--/.box box-primary -->	
		</div><!--/.col-lg-12 -->	
	</div><!--/.row -->			
</section>
<r:script>
	$('.faCodeFormat').on("change",function(){
		var id=$(this).attr('id');
		var clazz=id.split('_');
		formatField($(this).val(),clazz[1]);
		
	});

	function formatField(segment,id){

		if(segment=="freeText"){
			freeText(id);
		}else{
			freeTextRemove(id);
		}

		if(segment=="timeStamps"){
			timeStamps(id);
		}else{
			timeStampsRemove(id);
		}

		if(segment=="runningNo"){
			runningNo(id);
		}else{
			runningNoRemove(id);
		}

		if(segment=="tables"){
			tables(id);
		}else{
			tablesRemove(id);
		}

		if(segment==""){
			$('#formatField_'+id).val("");
			updateNumberFormat(id);
		}
	}

	/**
	freeText
	**/
	$('.dataType').on("change",function(){
		var id=$(this).attr('id');
		var clazz=id.split('_');
		var value=$(this).val();

		if(value=="varchar"){
			$('#freeText_length_div_'+clazz[2]).show();
		}else{
			$('#freeText_length_div_'+clazz[2]).hide();
		}	
		
	});

	function freeText(id){
		$('#freeText_label_div_'+id).show();
		$('#freeText_label_'+id).attr("required", true);
		$('#freeText_dataType_div_'+id).show();
		$('#freeText_dataType_'+id).attr("required", true);
	}

	function freeTextRemove(id){
		$('#freeText_label_div_'+id).hide();
		$('#freeText_label_'+id).empty();
		$('#freeText_dataType_div_'+id).hide();
		$('#freeText_dataType_'+id).val();
		$('#freeText_length_div_'+id).hide();
		$('#freeText_length_'+id).empty();
		$('#freeText_label_'+id).attr("required", false);
		$('#freeText_dataType_'+id).attr("required", false);
		$('#freeText_length_'+id).attr("required", false);
	}

	function freeTextEach(id){
		var it= Number(id)
		var manual=$('#freeText_label_'+id).val();
		var manualDateType=$('#freeText_dataType_'+id).val();
		var manualLength=$('#freeText_length_'+id).val();

		$('#formatField_'+id).val($('#segment_'+id).val()+":"+it+":"+manual+":"+manualDateType+":"+manualLength);
		updateNumberFormat(id);
	
	}

	/* Time Stamps */ 
	function timeStamps(id){
		$('#timeStamps_div_'+id).show();
		$('#timeStamps_'+id).attr("required", true);
	}

	function timeStampsRemove(id){
		$('#timeStamps_div_'+id).hide();
		$('#timeStamps_'+id).val();
		$('#timeStamps_'+id).attr("required", false);
	}

	function timeStampsEach(id){
		var it= Number(id)
		var timeStamps=$('#timeStamps_'+id).val();
		$('#formatField_'+id).val($('#segment_'+id).val()+":"+it+":"+timeStamps);
		updateNumberFormat(id);
		
	}

	/* Running No */
	function runningNo(id){
		$('#runningNo_length_div_'+id).show();
		$('#runningNo_length_'+id).attr("required", true);	
	}

	function runningNoRemove(id){
		$('#runningNo_length_div_'+id).hide();
		$('#runningNo_length_'+id).empty();
		$('#runningNo_length_'+id).attr("required", false);	
	}

	function runningNoEach(id){
		var it= Number(id)
		var runningNo=$('#runningNo_length_'+id).val();
		$('#formatField_'+id).val($('#segment_'+id).val()+":"+it+":"+runningNo);
		updateNumberFormat(id);
		
	}

	/* Master Table */
	function tables(id){
		$('#tables_div_'+id).show();
		//$('#fieldTable_'+id).show();

	}
	function tablesRemove(id){
		$('#tables_div_'+id).hide();
		$('#tables_'+id).val();
		$('#fieldTable_'+id).hide();
		$('#fieldTable_'+id).val();
	}

	function tablesEach(id){
		var value=$('#tables_'+id).val();
		$('#fieldTable_div_'+id).show();
		if(value!=""){
			$.ajax({
		         type: "GET",
		         url: "/${meta(name:'app.name')}/appSetting/getClassFieldName",
		         data: {tabelsName:value},
		         success: function(data){ 
		         	console.log(data);
		          	Object.size = function(obj) {
								    var size = 0, key;
								    for (key in obj) {
								        if (obj.hasOwnProperty(key)) size++;
								    }
								    return size;
								};
				                var j = Object.size(data.names);
		          	var html = '';
		          	//html += '<select id="fieldTable_'+id+'" name="fieldTable_'+id+'" onchange="valueEach('+id+')"> ';
		          	$('#fieldTable_'+id).empty();
		          	html+='<option value=""></option>';
		          	for(i=0;j > i;i++)
		           	{
		           		
		           		html+='<option value='+data.names[i]+'>'+data.names[i]+'</option>';
		           		
		           	}
		           	html += '</select>';
		            var segment= $('#fieldTable_'+id).append(html);
		            console.log("---------segment----------");
		            console.log(segment);
		          },
		          error:function(data){
		            alert(data.statusText);
		          },
		          dataType: 'json'
	        });
		}

	}

	function updateNumberFormat(id){
	
		var segment0 = $('#formatField_1').val();
		var segment1 = $('#formatField_2').val();
		var segment2 = $('#formatField_3').val();
		var segment3 = $('#formatField_4').val();
		var segment4 = $('#formatField_5').val();
		var separator = $('#separator').val();
		var newVal = segment0 + separator + segment1 + separator + segment2 + separator + segment3 + separator + segment4 ;

		$('#val').val(newVal);
	}



</r:script>		
</body>

</html>
