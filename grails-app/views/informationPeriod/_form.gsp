<%@ page import="com.smanggin.InformationPeriod" %>

	
			<div class="form-group fieldcontain ${hasErrors(bean: informationPeriodInstance, field: 'name', 'error')} ">
				<label for="name" class="col-sm-3 control-label"><g:message code="informationPeriod.name.label" default="Name" /></label>
				<div class="col-sm-9">
					<g:textField name="name" class="form-control" value="${informationPeriodInstance?.name}"/>
					<span class="help-inline">${hasErrors(bean: informationPeriodInstance, field: 'name', 'error')}</span>
				</div>
			</div>

			<div class="form-group fieldcontain ${hasErrors(bean: informationPeriodInstance, field: 'startDate', 'error')} required">
				<label for="startDate" class="col-sm-3 control-label"><g:message code="informationPeriod.startDate.label" default="Start Date" /><span class="required-indicator">*</span></label>
				
				<g:jqDatePicker name="startDate" precision="day"  value="${informationPeriodInstance?.startDate}" data-date-format="yyyy-mm-dd" />
					
				<span class="help-inline">${hasErrors(bean: informationPeriodInstance, field: 'startDate', 'error')}</span>
				
			</div>
			
			<div class="form-group fieldcontain ${hasErrors(bean: informationPeriodInstance, field: 'endDate', 'error')} required">
				<label for="endDate" class="col-sm-3 control-label"><g:message code="informationPeriod.endDate.label" default="End Date" /><span class="required-indicator">*</span></label>
				
					
					<g:jqDatePicker name="endDate" precision="day"  value="${informationPeriodInstance?.endDate}" data-date-format="yyyy-mm-dd" />
					<span class="help-inline">${hasErrors(bean: informationPeriodInstance, field: 'endDate', 'error')}</span>
				
			</div>

		

			<div class="form-group fieldcontain ${hasErrors(bean: informationPeriodInstance, field: 'position', 'error')} ">
				<label for="position" class="col-sm-3 control-label"><g:message code="informationPeriod.position.label" default="Position" /></label>
				<div class="col-sm-9">
					<bs:checkBox name="position" value="${informationPeriodInstance?.position}"  onLabel="Top" offLabel="Bottom"/>
					<span class="help-inline">${hasErrors(bean: informationPeriodInstance, field: 'position', 'error')}</span>
				</div>
			</div>

			



