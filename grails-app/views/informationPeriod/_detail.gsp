<%@ page import="com.smanggin.InformationPeriod" %>

				<%
if(actionName=='edit' || actionName=='show') { 
%>
<div class="easyui-tabs table" style="height:700px">
    
    <!-- Attachment -->
        <div title='<g:message code="informationPeriod.attachMent.label" default="Attachment"/>'  style="padding:10px">
	       <table id="dg-attachments" class="easyui-datagrid"  style="height:680px"
	                data-options="
	                view:cardview,
	                singleSelect:true, 
	                
	                toolbar: '#tb-attachment',   
                    onClickRow:attachmentsOnClickRow ,    
	                url:'/${meta(name:'app.name')}/attachment/jlist?className=informationPeriod&classId=${informationPeriodInstance?.id}'">
	        
	            <thead>
	                <tr>
	                    <th data-options="field:'originalName',width:200">File Name</th> 
	                    <th data-options="field:'fileTypesName',width:100">File Types </th>          
                        <th data-options="field:'id',width:200,formatter:formatButton">Download</th> 
	                </tr>
	            </thead>    
	        </table>        

	    </div><!-- /.Attachment -->
</div>
<g:render template="attachment"/> 
<!-- Toolbar Attachment -->
<div id="tb-attachment" style="height:auto">
    <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:false" onclick="attachmentsUpload()">upload</a>
    <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:false" onclick="attachmentsRemoveit()">Remove</a>
    %{-- <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-reload',plain:false" onclick="attachmentsDownloadit()">Download</a> --}%
</div><!-- /.Toolbar Attachment -->

<r:script>
	function attachmentsUpload(){
		$('#uploadPO').modal('show');
	}

	var editIndex = undefined;
    function attachmentsEndEditing(){
        if (editIndex == undefined){return true}
        if ($('#dg-attachments').datagrid('validateRow', editIndex)){
            $('#dg-attachments').datagrid('endEdit', editIndex);
            var row = $('#dg-attachments').datagrid('getRows')[editIndex]
            $.ajax({
              type: "POST",
              url: "/${meta(name:'app.name')}/attachment/jsave",
              data: row,
              success: function(data){ 
                 attachmentsRefresh();
                  if(!data.success)
                  {
                    if(data.limit){
                        alert(data.messages);     
                    }else{
                        alert(data.messages.errors[0].message);
                    }
                    
                  }

              },
              dataType: 'json'
            });
            editIndex = undefined;
            return true;
        } else {
            return false;
        }
    }

    function attachmentsOnClickRow(index){
    	console.log(index);
    	if (editIndex != index){
            if (attachmentsEndEditing()){
                $('#dg-attachments').datagrid('selectRow', index)
                        .datagrid('beginEdit', index);
                editIndex = index;
            } else {
                $('#dg-attachments').datagrid('selectRow', editIndex);
            }
        }
        
    }
    
    function attachmentsRemoveit(){
        if (editIndex == undefined){return}
        if (!confirm('Are you sure to delete this record?')){ return }
        
        var row = $('#dg-attachments').datagrid('getRows')[editIndex]
        

        $('#dg-attachments').datagrid('cancelEdit', editIndex)
                .datagrid('deleteRow', editIndex);

        $.ajax({
          type: "POST",
          url: "/${meta(name:'app.name')}/attachment/jdelete/" + row['id'],
          data: row,
          success: function(data){ 
            attachmentsRefresh();
              if(!data.success)
              {
                    alert(data.messages);
              }
          },
          dataType: 'json'
        });             
        editIndex = undefined;
    }

    
    function attachmentsRefresh(){
        $('#dg-attachments').datagrid('reload');
        editIndex = undefined;
      
       
    }

    function attachmentsDownloadit(){
        if (editIndex == undefined){return}
        var row = $('#dg-attachments').datagrid('getRows')[editIndex]
        

        $('#dg-attachments').datagrid('cancelEdit', editIndex)
                .datagrid('deleteRow', editIndex);

        $.ajax({
          type: "POST",
          url: "/${meta(name:'app.name')}/attachment/downloadFile/" + row['id'],
          data: row,
          success: function(data){ 
            attachmentsRefresh();
              if(!data.success)
              {
                    alert(data.messages);
              }
          },
          dataType: 'json'
        });             
        editIndex = undefined;
    }

    function formatButton(value,rows){
        return  '<a href="/${meta(name:'app.name')}/attachment/downloadFile?id='+rows.id+'"> Click Here </a>'
    }

    var cardview = $.extend({}, $.fn.datagrid.defaults.view, {
    renderRow: function(target, fields, frozen, rowIndex, rowData){
    	
        var cc = [];
        //if(rowData.length > 0){
        cc.push('<td colspan=' + fields.length + ' style="padding:10px 5px;border:0;">');
        if (!frozen){
            /*var aa = rowData.itemid.split('-');*/
            var img = rowData.fileName;
            if(img != undefined){
            	cc.push('<img src="/${meta(name:'app.name')}/upload/informationPeriod/' + img + '" style="width:150px;float:left">');
	            //cc.push('<img src="${resource( dir: "upload/informationPeriod",file: '+img+')}" style="width:150px;float:left">');
	            console.log(rowData)
	            cc.push('<div style="float:left;margin-left:20px;">');
	            for(var i=0; i<fields.length; i++){
	                var copts = $(target).datagrid('getColumnOption', fields[i]);
	                if(copts.title ==  'Download'){
	                	/*cc.push('<p><span class="c-label">' + copts.title + ':</span> ' + formatButton(rowIndex,rowData) + '</p>');	*/
	                }else{
	                	cc.push('<p><span class="c-label">' + copts.title + ':</span> ' + rowData[fields[i]] + '</p>');	
	                }
	                
	            }	
            }
            
            cc.push('</div>');
        }
        cc.push('</td>');
    	//}
        return cc.join('');

    }
	});

	

</r:script>

<%
}
%>
