
<%@ page import="com.smanggin.InformationPeriod" %>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'informationPeriod.label', default: 'InformationPeriod')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
	<g:set var="canCreate" value="true" scope="request" />
</head>

<body>
	
<section id="list-informationPeriod" class="first">
	<div class="row">
		<div class="col-lg-12">
			<div class="box box-primary">
				
				<div class="box-header with-border">
                  <h3 class="box-title"><g:message code="default.list.label" args="[entityName]" /></h3>
                </div><!--/.box-header with-border -->

				<div class="box-body table-responsive">	
					<table class="table table-bordered margin-top-medium">
						<thead>
							<tr>
							
								<g:sortableColumn property="name" title="${message(code: 'informationPeriod.name.label', default: 'Name')}" />

								<g:sortableColumn property="startDate" title="${message(code: 'informationPeriod.startDate.label', default: 'Date Created')}" />
							
								<g:sortableColumn property="endDate" title="${message(code: 'informationPeriod.endDate.label', default: 'End Date')}" />
							
								
								<g:sortableColumn property="createdBy" title="${message(code: 'informationPeriod.createdBy.label', default: 'Created By')}" />
							
							
								
							
							</tr>
						</thead>
						<tbody>
						<g:each in="${informationPeriodInstanceList}" status="i" var="informationPeriodInstance">
							<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
							
								<td><g:link action="show" id="${informationPeriodInstance.id}">${fieldValue(bean: informationPeriodInstance, field: "name")}</g:link></td>

								<td><g:formatDate date="${informationPeriodInstance.startDate}" /></td>
							
								<td><g:formatDate date="${informationPeriodInstance.endDate}" /></td>
							
								
								<td>${fieldValue(bean: informationPeriodInstance, field: "createdBy")}</td>
							
								
							
							</tr>
						</g:each>
						</tbody>
					</table>
				</div><!--/.box-body table-responsive -->

				<div class="box-footer clearfix">
					<bs:paginate total="${informationPeriodInstanceTotal}" />
				</div><!--/.box-footer clearfix -->
			</div><!--/.box box-primary -->	
		</div><!--/.col-lg-12 -->	
	</div><!--/.row -->			

</section>

</body>

</html>
