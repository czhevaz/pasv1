<%@ page import="com.smanggin.CategoryAsset" %>

			<div class="form-group fieldcontain ${hasErrors(bean: categoryAssetInstance, field: 'name', 'error')} ">
				<label for="name" class="col-sm-3 control-label"><g:message code="categoryAsset.name.label" default="Name" /></label>
				<div class="col-sm-9">
					<g:textField name="name" class="form-control" value="${categoryAssetInstance?.name}"/>
					<span class="help-inline">${hasErrors(bean: categoryAssetInstance, field: 'name', 'error')}</span>
				</div>
			</div>


			<div class="form-group fieldcontain ${hasErrors(bean: categoryAssetInstance, field: 'description', 'error')} ">
				<label for="description" class="col-sm-3 control-label"><g:message code="categoryAsset.description.label" default="Description" /></label>
				<div class="col-sm-9">
					<g:textField name="description" class="form-control" value="${categoryAssetInstance?.description}"/>
					<span class="help-inline">${hasErrors(bean: categoryAssetInstance, field: 'description', 'error')}</span>
				</div>
			</div>

			<div class="form-group fieldcontain ${hasErrors(bean: categoryAssetInstance, field: 'categoryAsset', 'error')} ">
				<label for="categoryAsset" class="col-sm-3 control-label"><g:message code="categoryAsset.categoryAsset.label" default="Category Asset" /></label>
				<div class="col-sm-9">
					<g:select id="categoryAsset" name="categoryAsset.id" from="${com.smanggin.CategoryAsset.list()}" optionKey="id" optionValue="name" value="${categoryAssetInstance?.categoryAsset?.id}" class="many-to-one form-control chosen-select" noSelection="['null': '']"/>
					<span class="help-inline">${hasErrors(bean: categoryAssetInstance, field: 'categoryAsset', 'error')}</span>
				</div>
			</div>






