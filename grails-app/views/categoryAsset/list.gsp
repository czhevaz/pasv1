
<%@ page import="com.smanggin.CategoryAsset" %>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'categoryAsset.label', default: 'CategoryAsset')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>
	
<section id="list-categoryAsset" class="first">
	<div class="row">
		<div class="col-lg-12">
			<div class="box box-primary">
				
				<div class="box-header with-border">
                  <h3 class="box-title"><g:message code="default.list.label" args="[entityName]" /></h3>
                </div><!--/.box-header with-border -->

				<div class="box-body table-responsive">	
					<table class="table table-bordered margin-top-medium">
						<thead>
							<tr>
							
								<g:sortableColumn property="description" title="${message(code: 'categoryAsset.description.label', default: 'Description')}" />
							
								<th><g:message code="categoryAsset.categoryAsset.label" default="Category Asset" /></th>
							
								<g:sortableColumn property="createdBy" title="${message(code: 'categoryAsset.createdBy.label', default: 'Created By')}" />
							
								<g:sortableColumn property="dateCreated" title="${message(code: 'categoryAsset.dateCreated.label', default: 'Date Created')}" />
							
								<g:sortableColumn property="lastUpdated" title="${message(code: 'categoryAsset.lastUpdated.label', default: 'Last Updated')}" />
							
								<g:sortableColumn property="name" title="${message(code: 'categoryAsset.name.label', default: 'Name')}" />
							
							</tr>
						</thead>
						<tbody>
						<g:each in="${categoryAssetInstanceList}" status="i" var="categoryAssetInstance">
							<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
							
								<td><g:link action="show" id="${categoryAssetInstance.id}">${fieldValue(bean: categoryAssetInstance, field: "description")}</g:link></td>
							
								<td>${fieldValue(bean: categoryAssetInstance, field: "categoryAsset")}</td>
							
								<td>${fieldValue(bean: categoryAssetInstance, field: "createdBy")}</td>
							
								<td><g:formatDate date="${categoryAssetInstance.dateCreated}" /></td>
							
								<td><g:formatDate date="${categoryAssetInstance.lastUpdated}" /></td>
							
								<td>${fieldValue(bean: categoryAssetInstance, field: "name")}</td>
							
							</tr>
						</g:each>
						</tbody>
					</table>
				</div><!--/.box-body table-responsive -->

				<div class="box-footer clearfix">
					<bs:paginate total="${categoryAssetInstanceTotal}" />
				</div><!--/.box-footer clearfix -->
			</div><!--/.box box-primary -->	
		</div><!--/.col-lg-12 -->	
	</div><!--/.row -->			

</section>

</body>

</html>
