<%@ page import="com.smanggin.Budget" %>

				<%
if(actionName=='edit' || actionName=='show') { 
%>
<div class="easyui-tabs table" style="height:300px">
    
    
        <div title='<g:message code="budget.budgetDetails.label" default="Budget Details" />' style="padding:10px">

            <table id="dg-budgetDetails" class="easyui-datagrid" style="height:240px"
            data-options="singleSelect:true, 
            collapsible:true, 
            onClickRow: budgetDetailsOnClickRow,
            toolbar: '#tb-budgetDetails',
            url:'/${meta(name:'app.name')}/budgetDetail/jlist?masterField.name=budget&masterField.id=${budgetInstance?.id}'">
                <thead>
                    <tr>
                    
                        <th data-options="field:'budgetId',hidden:true">Budget</th>
                                 
                        <th data-options="field:'categoryAssetId',width:200,
                            formatter:function(value,row){
                                return row.categoryAssetName;
                            },
                            editor:{
                                type:'combobox',
                                options:{
                                    valueField:'id',
                                    textField:'name',
                                    url:'/${meta(name:'app.name')}/categoryAsset/jlist',
                                    required:true,
                                }
                        }">Category Asset</th>
                                    
                        <th data-options="field:'coaId',width:200,
                            formatter:function(value,row){
                                return row.coaCode;
                            },
                            editor:{
                                type:'combobox',
                                options:{
                                    valueField:'code',
                                    textField:'code',
                                    url:'/${meta(name:'app.name')}/ChartOfAccount/jlist',
                                    required:true,
                                }
                        }">Coa</th>
                                    
                        
                        <th data-options="field:'coaDescription',width:500,editor:{type:'textbox'}">COA Description</th>         

                        <th data-options="field:'qty',align:'right',formatter:formatNumber,  width:100,editor:{type:'numberbox',options:{precision:2}}">Qty</th>
                        

                    
                        
                        <th data-options="field:'uom',width:200,editor:'text'">Uom</th>
                        

                    
                        <th data-options="field:'value',align:'right',formatter:formatNumber,  width:100,editor:{type:'numberbox',options:{precision:2}}">Value</th>
                        

                    
                    </tr>
                </thead>
            </table>
        </div>     
</div>
        <div id="tb-budgetDetails" style="height:auto">
            <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:false" onclick="budgetDetailsAppend()">Add</a>
            <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:false" onclick="budgetDetailsRemoveit()">Remove</a>
            <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-save',plain:false" onclick="budgetDetailsAccept()">Save</a>
            <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-reload',plain:false" onclick="budgetDetailsRefresh()">Refresh</a>
        </div>
            
        <r:script>     
            var editIndex = undefined;
            function budgetDetailsEndEditing(){
                if (editIndex == undefined){return true}
                if ($('#dg-budgetDetails').datagrid('validateRow', editIndex)){

        
                    //categoryAssetName
                    var ed = $('#dg-budgetDetails').datagrid('getEditor', {index:editIndex,field:'categoryAssetId'});
                    var categoryAssetName = $(ed.target).combobox('getText');
                    $('#dg-budgetDetails').datagrid('getRows')[editIndex]['categoryAssetName'] = categoryAssetName;
                    
        
                    //coaName
                    var ed = $('#dg-budgetDetails').datagrid('getEditor', {index:editIndex,field:'coaId'});
                    var coaName = $(ed.target).combobox('getText');
                    $('#dg-budgetDetails').datagrid('getRows')[editIndex]['coaCode'] = coaName;
                    
        

                    $('#dg-budgetDetails').datagrid('endEdit', editIndex);
                    var row = $('#dg-budgetDetails').datagrid('getRows')[editIndex]
                    $.ajax({
                      type: "POST",
                      url: "/${meta(name:'app.name')}/budgetDetail/jsave",
                      data: row,
                      success: function(data){ 
                          if(!data.success)
                          {
                            alert(data.messages.errors[0].message)
                          }
                      },
                      dataType: 'json'
                    });
                    editIndex = undefined;
                    return true;
                } else {
                    return false;
                }
            }
            function budgetDetailsOnClickRow(index){
                if (editIndex != index){
                    if (budgetDetailsEndEditing()){
                        $('#dg-budgetDetails').datagrid('selectRow', index)
                                .datagrid('beginEdit', index);
                        editIndex = index;
                    } else {
                        $('#dg-budgetDetails').datagrid('selectRow', editIndex);
                    }
                }
            }
            function budgetDetailsAppend(){
                if (budgetDetailsEndEditing()){
                    $('#dg-budgetDetails').datagrid('appendRow',
                    {budgetId: ${budgetInstance.id? budgetInstance.id : 0} });
                    editIndex = $('#dg-budgetDetails').datagrid('getRows').length-1;
                    $('#dg-budgetDetails').datagrid('selectRow', editIndex).datagrid('beginEdit', editIndex);
                }
            }
            function budgetDetailsRemoveit(){
                if (editIndex == undefined){return}
                if (!confirm('Are you sure to delete this record?')){ return }

                var row = $('#dg-budgetDetails').datagrid('getRows')[editIndex]
                console.log(row)

                $('#dg-budgetDetails').datagrid('cancelEdit', editIndex)
                        .datagrid('deleteRow', editIndex);

                $.ajax({
                  type: "POST",
                  url: "/${meta(name:'app.name')}/budgetDetail/jdelete/" + row['id'],
                  data: row,
                  success: function(data){ 
                      if(!data.success)
                      {
                            alert(data.messages)
                      }
                  },
                  dataType: 'json'
                });             
                editIndex = undefined;
            }
            function budgetDetailsAccept(){
                if (budgetDetailsEndEditing()){
                    $('#dg-budgetDetails').datagrid('acceptChanges');
                }
            }

            function budgetDetailsRefresh(){
                $('#dg-budgetDetails').datagrid('reload');
                editIndex = undefined;
            }
        </r:script>  
    

    

    

    

    

    

    

    

    

    

    

    

    


<%
}
%>
