
<%@ page import="com.smanggin.Budget" %>
<!doctype html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'budget.label', default: 'Budget')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>

<section id="show-budget" class="first">
	<div class="row">
		<div class="col-lg-12">
			<div class="box box-primary">
				<g:render template="headerTittle"/>
                <div class="box-body table-responsive">
					<table class="table table-striped">
						<tbody>
							<tr class="prop">
								<td valign="top" class="name"><g:message code="budget.number.label" default="Number" /></td>
								
								<td valign="top" class="value">${fieldValue(bean: budgetInstance, field: "number")}</td>
								
							</tr>

							<tr class="prop">
								<td valign="top" class="name"><g:message code="budget.country.label" default="Country" /></td>
								
								<td valign="top" class="value">${fieldValue(bean: budgetInstance, field: "country")}</td>
								
							</tr>
							
							<tr class="prop">
								<td valign="top" class="name"><g:message code="budget.transactionGroup.label" default="Transaction Group" /></td>
								
								<td valign="top" class="value"><g:link controller="transactionGroup" action="show" id="${budgetInstance?.transactionGroup?.id}">${budgetInstance?.transactionGroup?.encodeAsHTML()}</g:link></td>
								
							</tr>

							<tr class="prop">
								<td valign="top" class="name"><g:message code="budget.budgetDate.label" default="Budget Date" /></td>
								
								<td valign="top" class="value"><g:formatDate date="${budgetInstance?.budgetDate}" /></td>
								
							</tr>
						
							<tr class="prop">
								<td valign="top" class="name"><g:message code="budget.currency1.label" default="Currency1" /></td>
								
								<td valign="top" class="value"><g:link controller="currency" action="show" id="${budgetInstance?.currency1?.id}">${budgetInstance?.currency1?.encodeAsHTML()}</g:link></td>
								
							</tr>
						
							<tr class="prop">
								<td valign="top" class="name"><g:message code="budget.currency2.label" default="Currency2" /></td>
								
								<td valign="top" class="value"><g:link controller="currency" action="show" id="${budgetInstance?.currency2?.id}">${budgetInstance?.currency2?.encodeAsHTML()}</g:link></td>
								
							</tr>
												
							<tr class="prop">
								<td valign="top" class="name"><g:message code="budget.rate.label" default="Rate" /></td>
								
								<td valign="top" class="value">${fieldValue(bean: budgetInstance, field: "rate")}</td>
								
							</tr>
						
							<tr class="prop">
								<td valign="top" class="name"><g:message code="budget.requestor.label" default="Requestor" /></td>
								
								<td valign="top" class="value"><g:link controller="user" action="show" id="${budgetInstance?.requestor?.id}">${budgetInstance?.requestor?.encodeAsHTML()}</g:link></td>
								
							</tr>
						
							<tr class="prop">
								<td valign="top" class="name"><g:message code="budget.state.label" default="State" /></td>
								
								<td valign="top" class="value">${fieldValue(bean: budgetInstance, field: "state")}</td>
								
							</tr>
							
							<tr class="prop">
								<td valign="top" class="name"><g:message code="budget.createdBy.label" default="Created By" /></td>
								
								<td valign="top" class="value">${fieldValue(bean: budgetInstance, field: "createdBy")}</td>
								
							</tr>
						
						
							<tr class="prop">
								<td valign="top" class="name"><g:message code="budget.updatedBy.label" default="Updated By" /></td>
								
								<td valign="top" class="value">${fieldValue(bean: budgetInstance, field: "updatedBy")}</td>
								
							</tr>

														<tr class="prop">
								<td valign="top" class="name"><g:message code="budget.dateCreated.label" default="Date Created" /></td>
								
								<td valign="top" class="value"><g:formatDate date="${budgetInstance?.dateCreated}" /></td>
								
							</tr>
						
							<tr class="prop">
								<td valign="top" class="name"><g:message code="budget.lastUpdated.label" default="Last Updated" /></td>
								
								<td valign="top" class="value"><g:formatDate date="${budgetInstance?.lastUpdated}" /></td>
								
							</tr>

						
						
						</tbody>
					</table>
				</div><!--/.row -->
				<div class="box-footer clearfix">
					<g:form method="post" class="form-horizontal" >
						<g:hiddenField name="id" value="${budgetInstance?.id}" />
						<g:hiddenField name="version" value="${budgetInstance?.version}" />
						<g:hiddenField name="updatedBy" value="${session.user}"/>
						<g:hiddenField id ="rejectNotes" name="rejectNotes" value="${budgetInstance?.rejectNotes}" />

						<div class="form-actions">
					
							<g:if test="${budgetInstance?.state=='Draft' }">
								<g:if test="${budgetInstance?.createdBy == session.user}">
									<g:actionSubmit class="btn btn-primary btn-sm" action="actionWaitingApprove" value="${message(code: 'default.button.approve.label', default: 'Send To Approver')}" />
									
								</g:if>	
							</g:if>
							<g:if test="${budgetInstance?.state=='Waiting Approval'}">
								<g:if test="${budgetInstance?.mustApprovedBy == session.user}">
									<g:actionSubmit class="btn btn-primary btn-sm" action="actionApprove" value="${message(code: 'default.button.approve.label', default: 'Approve')}" />
									
									<g:actionSubmit id="reject" class="btn btn-primary btn-sm" action="actionReject" value="${message(code: 'default.button.rejected.label', default: 'Rejected')}" />

								</g:if>	
							</g:if>
							<g:if test="${budgetInstance?.state=='Approved'}">
								<g:if test="${budgetInstance?.mustApprovedBy == session.user}">
									<g:actionSubmit id="void" class="btn btn-primary btn-sm" action="actionVoid" value="${message(code: 'default.button.void.label', default: 'Void')}" />
								</g:if>	
							</g:if>
						</div>
					</g:form>	
				</div><!--/.box-footer clearfix -->
			</div><!--/.box-body table-responsive -->

			<g:render template="detail"/> 
		</div><!--/.box box-primary -->
	</div><!--/.row -->
</section>

</body>

</html>
