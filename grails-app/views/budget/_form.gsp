<%@ page import="com.smanggin.Budget" %>



			

			<div class="form-group fieldcontain ${hasErrors(bean: budgetInstance, field: 'country', 'error')} ">
				<label for="country" class="col-sm-3 control-label"><g:message code="budget.country.label" default="Country" /></label>
				<div class="col-sm-3">
				<g:select id="country" name="country" from="${com.smanggin.Country.list()}" optionKey="name" required="" value="${purchaseOrderInstance?.country}" class="many-to-one form-control chosen-select" />
					<span class="help-inline">${hasErrors(bean: budgetInstance, field: 'country', 'error')}</span>
				</div>
			</div>

			<div class="form-group fieldcontain ${hasErrors(bean: budgetInstance, field: 'transactionGroup', 'error')} required">
				<label for="transactionGroup" class="col-sm-3 control-label"><g:message code="budget.transactionGroup.label" default="Transaction Group" /><span class="required-indicator">*</span></label>
				<div class="col-sm-3">
					<g:select id="transactionGroup" name="transactionGroup.id" from="${com.smanggin.TransactionGroup.list()}" optionKey="id" required="" value="${budgetInstance?.transactionGroup?.id}" class="many-to-one form-control chosen-select"/>
					<span class="help-inline">${hasErrors(bean: budgetInstance, field: 'transactionGroup', 'error')}</span>
				</div>
			</div>


			<div class="form-group fieldcontain ${hasErrors(bean: budgetInstance, field: 'number', 'error')} ">
				<label for="number" class="col-sm-3 control-label"><g:message code="budget.number.label" default="Number" /></label>
				<div class="col-sm-3">
					<g:textField name="number" class="form-control" value="${budgetInstance?.number}" readonly="true"/>
					<span class="help-inline">${hasErrors(bean: budgetInstance, field: 'number', 'error')}</span>
				</div>
			</div>

			<div class="form-group fieldcontain ${hasErrors(bean: budgetInstance, field: 'budgetDate', 'error')} required">
				<label for="budgetDate" class="col-sm-3 control-label"><g:message code="budget.budgetDate.label" default="Budget Date" /><span class="required-indicator">*</span></label>
				
					<g:jqDatePicker name="budgetDate" precision="day"  value="${budgetInstance?.budgetDate}" data-date-format="yyyy-mm-dd" />
					
					<span class="help-inline">${hasErrors(bean: budgetInstance, field: 'budgetDate', 'error')}</span>
				
			</div>


			<div class="form-group fieldcontain ${hasErrors(bean: budgetInstance, field: 'currency1', 'error')} required">
				<label for="currency1" class="col-sm-3 control-label"><g:message code="budget.currency1.label" default="Currency1" /><span class="required-indicator">*</span></label>
				<div class="col-sm-3">
					
					<g:select id="currency1" name="currency" from="${com.smanggin.Currency.list()}" optionKey="code" value="${budgetInstance?.currency1?.code}" class="many-to-one form-control chosen-select" noSelection="['null': '']"/>
					<span class="help-inline">${hasErrors(bean: budgetInstance, field: 'currency1', 'error')}</span>
				</div>
			</div>

			<div class="form-group fieldcontain ${hasErrors(bean: budgetInstance, field: 'rate', 'error')} required">
				<label for="rate" class="col-sm-3 control-label"><g:message code="budget.rate.label" default="Rate" /><span class="required-indicator">*</span></label>
				<div class="col-sm-3">
					<g:field type="number" name="rate" class="form-control" step="any" value="${budgetInstance.rate}"/>
					<g:field type="hidden" id ="rateDetailId" name="rateDetail.id" value="${budgetInstance.rateDetail?.id}"/> 
						
					<span class="help-inline">${hasErrors(bean: budgetInstance, field: 'rate', 'error')}</span>
				</div>
			</div>

<!-- Js Script --> 
<r:script>
	
	var country = $('#country').val();
    var date = $('#budgetDate_year').val() + "-" + $('#budgetDate_month').val() + "-" + $('#budgetDate_day').val()
    $(document).ready(function () {
		
		<%
		if(actionName=='create') { 
		%>
		$('#transactionGroup').empty();
		<% 
		}
		%>
		$('#transactionGroup').chosen();
		<g:if test="${session.country}" >
			country ='${session.country}';
			$('#country').val(country);	
			$('#country option:not(:selected)').prop('disabled', true).trigger('chosen:updated');
			<%
            if(actionName=='create') { 
            %>
            
		    getCurrency(country);
			getTrGroup(country);
			
            <% 
            }
            %>
            
		</g:if>
		
	});	

	$("#currency1").on('change', function() {
    	
        $.ajax({
            url: "/${meta(name:'app.name')}/currency/jlist?code="+$(this).val()+"&date="+date,
            type: "POST",
            success: function (data) {
				
            	$("#rate").val(data.value);
            },
            error: function (xhr, status, error) {
                alert("fail");
            }
        });
    });
	
	$("#country").on('change', function() {
		country = $(this).val();
		
		getTrGroup(urlGroup);			
		getCurrency(urlCurrency);
		
	});		


	function getTrGroup(country){
        $.ajax({
            url: "/${meta(name:'app.name')}/transactionGroup/jlist?login=${session.user}&country="+country,
        
            type: "POST",
            success: function (data) {
                
                if(data.length > 0){
                    
                    $('#transactionGroup').empty();
                     $('#transactionGroup').prepend("<option value='' >&nbsp;</option>")
                    $.each(data, function(a, b){
                         var opt = "<option value='"+b.id+"'> "+ b.description +" </option>";
                        $('#transactionGroup').append(opt);
                        
                    });

                    $('#transactionGroup').trigger('chosen:updated');
                    $('#transactionGroup').chosen();
                 
                }else{
                    $('#transactionGroup').chosen("destroy");
                    $('#transactionGroup').chosen();    
                }
            },
            error: function (xhr, status, error) {
                alert("fail");
            }
        });
    }/*-- end getTrgroup  --*/

    function getCurrency(country){
      
        //console.log("date");
        //console.log(date);
        $.ajax({
            url: "/${meta(name:'app.name')}/currency/jlist?country="+country+"&date="+date+"&countryCode="+country,
            type: "POST",
            success: function (data) {
                console.log(data);
                $("#currency1").val(data.code);
                $('#currency1').trigger('chosen:updated');
                   
                $("#rate").val(data.value);     
                $("#rateDetailId").val(data.rateDetailId)
            },
            error: function (xhr, status, error) {
                alert("fail");
            }
        });
    }/*-- end getCurrency  --*/


</r:script>

