
<%@ page import="com.smanggin.Budget" %>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'budget.label', default: 'Budget')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>
	
<section id="list-budget" class="first">
	<div class="row">
		<div class="col-lg-12">
			<div class="box box-primary">
				
				<div class="box-header with-border">
                  <h3 class="box-title"><g:message code="default.list.label" args="[entityName]" /></h3>
                </div><!--/.box-header with-border -->

				<div class="box-body table-responsive">	
					<table class="table table-bordered margin-top-medium">
						<thead>
							<tr>
							
								<g:sortableColumn property="budgetDate" title="${message(code: 'budget.budgetDate.label', default: 'Budget Date')}" />
							
								<g:sortableColumn property="country" title="${message(code: 'budget.country.label', default: 'Country')}" />
							
								<g:sortableColumn property="createdBy" title="${message(code: 'budget.createdBy.label', default: 'Created By')}" />
							
								<th><g:message code="budget.currency1.label" default="Currency1" /></th>
							
								<th><g:message code="budget.currency2.label" default="Currency2" /></th>
							
								<g:sortableColumn property="dateCreated" title="${message(code: 'budget.dateCreated.label', default: 'Date Created')}" />
							
							</tr>
						</thead>
						<tbody>
						<g:each in="${budgetInstanceList}" status="i" var="budgetInstance">
							<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
							
								<td><g:link action="show" id="${budgetInstance.id}">${fieldValue(bean: budgetInstance, field: "budgetDate")}</g:link></td>
							
								<td>${fieldValue(bean: budgetInstance, field: "country")}</td>
							
								<td>${fieldValue(bean: budgetInstance, field: "createdBy")}</td>
							
								<td>${fieldValue(bean: budgetInstance, field: "currency1")}</td>
							
								<td>${fieldValue(bean: budgetInstance, field: "currency2")}</td>
							
								<td><g:formatDate date="${budgetInstance.dateCreated}" /></td>
							
							</tr>
						</g:each>
						</tbody>
					</table>
				</div><!--/.box-body table-responsive -->

				<div class="box-footer clearfix">
					<bs:paginate total="${budgetInstanceTotal}" />
				</div><!--/.box-footer clearfix -->
			</div><!--/.box box-primary -->	
		</div><!--/.col-lg-12 -->	
	</div><!--/.row -->			

</section>

</body>

</html>
