<div id="searchBudget" class="modal fade  " tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
            		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        			<h4 class="modal-title" id="myModalLabel">Finding Budget</h4>
            </div>

            <div class="modal-body">
        		<div class="row">
		        	<div class="col-lg-12">
					   <div class="box box-primary ">
                            <div id="table-content" class=" box-body table-responsive" style="overflow-y:auto">
                                <table id="budgetContent" class="table table-bordered  table-striped  table-hover margin-top-medium " style="width:100%;">
                                    <thead>
                                        <tr>
                                            <th><g:message code="budget.categroyAsset.label" default="Asset Category" /></th>

                                            <th><g:message code="budget.number.label" default="Budget Number" /></th>
                                            
                                            <th><g:message code="budget.country.label" default="Country" /></th>

                                            <th><g:message code="budget.qty.label" default="Qty" /></th>

                                            <th><g:message code="budget.uom.label" default="uom" /></th>

                                            <th><g:message code="budget.currency.label" default="currency" /></th>

                                            <th><g:message code="budget.total.label" default="Total" /></th>

                                            <th><g:message code="budget.outstanding.label" default="Outstanding" /></th>

                                            <th><g:message code="budget.budgetDate.label" default="Budget Date" /></th>

                                            <th><g:message code="budget.createdBy.label" default="createdBy" /></th>
                                                                    
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                    </tbody>
                                </table>
                            </div>    
		              </div><!--/.box box-primary -->   
                    </div><!--/.col-lg-12 -->   
                </div>
                    
            </div><!-- /.modal-body -->
        </div>    
    </div>  
</div>

<style  type="text/css" >
 
.modal-content {
    width: 1100px;
    margin-left: -250px;
}

</style>
<r:script>
    $('#budgetContent tfoot th').each( function () {
        var title = $('#budgetContent thead th').eq( $(this).index() ).text();
        console.log($(this).index())
        
            $(this).html( '<input type="text" placeholder="Search '+title+'" />' ); 
        
        
    });

    var table = $('#budgetContent').DataTable({
        "paging": true,
         "lengthChange": false,
         "searching": true,
         "ordering": true,
         "info": true,
         "autoWidth": true,
         "scrollX": true
    });

    // Apply the search
    table = table.columns().eq( 0 );
    if(table){
        table.columns().eq( 0 ).each( function ( colIdx ) {
            
            
                $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () {
                table
                    .column( colIdx )
                    .search( this.value )
                    .draw();
                } );
            
        });
    }



    $('#budgetContent tbody').on('click', 'tr', function () {
        var data = table.row( this ).data();
        console.log(data);
        var r = confirm(" Are you sure to Add this budget !");
        if (r == true) {
            addToFap(data); 
        } else {
            
        }
        
       
    });

    function addToFap(data){
        $('#searchBudget').modal('hide');
        console.log(data);
       
        if (fAPDetailsEndEditing()){
        
            $('#dg-fAPDetails').datagrid('appendRow',
            {fapId: ${FAPInstance.id? FAPInstance.id : 0},createdBy:"${session.user}",budgetDetailId:data.id,categoryAssetName:data.categoryAssetName,qty:data.qty,budgetNumber:data.budgetNumber});
            editIndex = $('#dg-fAPDetails').datagrid('getRows').length-1;
            $('#dg-fAPDetails').datagrid('selectRow', editIndex).datagrid('beginEdit', editIndex);
           // console.log(data)
            
           
        }
       

    }
</r:script>