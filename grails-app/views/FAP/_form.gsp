<%@ page import="com.smanggin.FAP" %>



			<div class="form-group fieldcontain ${hasErrors(bean: FAPInstance, field: 'country', 'error')} ">
				<label for="country" class="col-sm-3 control-label"><g:message code="FAP.country.label" default="Country" /></label>
				<div class="col-sm-3">
					<g:select id="country" name="country" from="${com.smanggin.Country.list()}" optionKey="name" required="" value="${FAPInstance?.country}" class="many-to-one form-control chosen-select" />
					<span class="help-inline">${hasErrors(bean: FAPInstance, field: 'country', 'error')}</span>
				</div>
			</div>

			<div class="form-group fieldcontain ${hasErrors(bean: FAPInstance, field: 'transactionGroup', 'error')} required">
				<label for="transactionGroup" class="col-sm-3 control-label"><g:message code="FAP.transactionGroup.label" default="Transaction Group" /><span class="required-indicator">*</span></label>
				<div class="col-sm-3">
					<g:select id="transactionGroup" name="transactionGroup.id" from="${com.smanggin.TransactionGroup.list()}" optionKey="id" required="" value="${FAPInstance?.transactionGroup?.id}" class="many-to-one form-control chosen-select"/>
					<span class="help-inline">${hasErrors(bean: FAPInstance, field: 'transactionGroup', 'error')}</span>
				</div>
			</div>

			<div class="form-group fieldcontain ${hasErrors(bean: FAPInstance, field: 'number', 'error')} ">
				<label for="number" class="col-sm-3 control-label"><g:message code="FAP.number.label" default="Number" /></label>
				<div class="col-sm-3">
					<g:textField name="number" class="form-control" value="${FAPInstance?.number}" readonly="true"/>
					<span class="help-inline">${hasErrors(bean: FAPInstance, field: 'number', 'error')}</span>
				</div>
			</div>

			<div class="form-group fieldcontain ${hasErrors(bean: FAPInstance, field: 'fapDate', 'error')} required">
				<label for="fapDate" class="col-sm-3 control-label"><g:message code="FAP.fapDate.label" default="FAP Date" /><span class="required-indicator">*</span></label>
				
					<g:jqDatePicker name="fapDate" precision="day"  value="${FAPInstance?.fapDate}" data-date-format="yyyy-mm-dd" />
					<span class="help-inline">${hasErrors(bean: FAPInstance, field: 'fapDate', 'error')}</span>
				
			</div>

			<div class="form-group fieldcontain ${hasErrors(bean: FAPInstance, field: 'department', 'error')} required">
				<label for="department" class="col-sm-3 control-label"><g:message code="FAP.department.label" default="Department" /><span class="required-indicator">*</span></label>
				<div class="col-sm-3">

					<g:select id="department" name="department.id" from="${com.smanggin.Department.list()}" optionKey="id" optionValue="name" required="" value="${FAPInstance?.department?.id}" class="many-to-one form-control chosen-select"/>
					<span class="help-inline">${hasErrors(bean: FAPInstance, field: 'department', 'error')}</span>
				</div>
			</div>


			<div class="form-group fieldcontain ${hasErrors(bean: FAPInstance, field: 'currency1', 'error')} required">
				<label for="currency1" class="col-sm-3 control-label"><g:message code="FAP.currency1.label" default="Currency1" /><span class="required-indicator">*</span></label>
				<div class="col-sm-3">
					<g:select id="currency1" name="currency" from="${com.smanggin.Currency.list()}" optionKey="code" value="${FAPInstance?.currency1?.code}" class="many-to-one form-control chosen-select" noSelection="['null': '']"/>
					
					<span class="help-inline">${hasErrors(bean: FAPInstance, field: 'currency1', 'error')}</span>
				</div>
			</div>

			<div class="form-group fieldcontain ${hasErrors(bean: FAPInstance, field: 'rate', 'error')} required">
				<label for="rate" class="col-sm-3 control-label"><g:message code="FAP.rate.label" default="Rate" /><span class="required-indicator">*</span></label>
				<div class="col-sm-3">
					<g:field type="number" name="rate" class="form-control" step="any" value="${FAPInstance.rate}"/>
					<g:field type="hidden" id ="rateDetailId" name="rateDetail.id" value="${FAPInstance.rateDetail?.id}"/> 
					<span class="help-inline">${hasErrors(bean: FAPInstance, field: 'rate', 'error')}</span>
				</div>
			</div>

<!-- Js Script --> 
<r:script>
	
	var country = $('#country').val();
    var date = $('#budgetDate_year').val() + "-" + $('#budgetDate_month').val() + "-" + $('#budgetDate_day').val()
    $(document).ready(function () {
		
		<%
		if(actionName=='create') { 
		%>
		$('#transactionGroup').empty();
		<% 
		}
		%>
		$('#transactionGroup').chosen();
		<g:if test="${session.country}" >
			country ='${session.country}';
			$('#country').val(country);	
			$('#country option:not(:selected)').prop('disabled', true).trigger('chosen:updated');
			<%
            if(actionName=='create') { 
            %>
            
		    getCurrency(country);
			getTrGroup(country);
			
            <% 
            }
            %>
            
		</g:if>
		
	});	

	$("#currency1").on('change', function() {
    	
        $.ajax({
            url: "/${meta(name:'app.name')}/currency/jlist?code="+$(this).val()+"&date="+date,
            type: "POST",
            success: function (data) {
				
            	$("#rate").val(data.value);
            },
            error: function (xhr, status, error) {
                alert("fail");
            }
        });
    });
	
	$("#country").on('change', function() {
		country = $(this).val();
		
		getTrGroup(urlGroup);			
		getCurrency(urlCurrency);
		
	});		


	function getTrGroup(country){
        $.ajax({
            url: "/${meta(name:'app.name')}/transactionGroup/jlist?login=${session.user}&country="+country,
        
            type: "POST",
            success: function (data) {
                
                if(data.length > 0){
                    
                    $('#transactionGroup').empty();
                     $('#transactionGroup').prepend("<option value='' >&nbsp;</option>")
                    $.each(data, function(a, b){
                         var opt = "<option value='"+b.id+"'> "+ b.description +" </option>";
                        $('#transactionGroup').append(opt);
                        
                    });

                    $('#transactionGroup').trigger('chosen:updated');
                    $('#transactionGroup').chosen();
                 
                }else{
                    $('#transactionGroup').chosen("destroy");
                    $('#transactionGroup').chosen();    
                }
            },
            error: function (xhr, status, error) {
                alert("fail");
            }
        });
    }/*-- end getTrgroup  --*/

    function getCurrency(country){
      
        //console.log("date");
        //console.log(date);
        $.ajax({
            url: "/${meta(name:'app.name')}/currency/jlist?country="+country+"&date="+date+"&countryCode="+country,
            type: "POST",
            success: function (data) {
                console.log(data);
                $("#currency1").val(data.code);
                $('#currency1').trigger('chosen:updated');
                   
                $("#rate").val(data.value);     
                $("#rateDetailId").val(data.rateDetailId)
            },
            error: function (xhr, status, error) {
                alert("fail");
            }
        });
    }/*-- end getCurrency  --*/


</r:script>





