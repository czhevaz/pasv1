<%@ page import="com.smanggin.FAP" %>

				<%
if(actionName=='edit' || actionName=='show') { 
%>
<div class="easyui-tabs table" style="height:300px">
            
            <div title='<g:message code="FAP.fAPDetails.label" default="FAPD etails" />' style="padding:10px">

            <table id="dg-fAPDetails" class="easyui-datagrid" style="height:240px"
            data-options="singleSelect:true, 
            collapsible:true, 
            onClickRow: fAPDetailsOnClickRow,
            toolbar: '#tb-fAPDetails',
            url:'/${meta(name:'app.name')}/FAPDetail/jlist?masterField.name=fap&masterField.id=${FAPInstance?.id}'">
                <thead>
                    <tr>
                        <th data-options="field:'categoryAssetName',width:500,editor:{type:'textbox',options:{required:true}}">Asset Name</th>
                                    
                        <th data-options="field:'budgetNumber',width:500,editor:{type:'textbox',options:{required:true}}">Budget Number</th>         

                        <th data-options="field:'qty',align:'right',formatter:formatNumber,  width:100,editor:{type:'numberbox',options:{precision:2}}">Qty</th>
                        
                        <th data-options="field:'unitprice',align:'right',formatter:formatNumber,  width:100,editor:{type:'numberbox',options:{precision:2}}">Unit Price</th>

                        <th data-options="field:'total',align:'right',formatter:formatNumber,  width:100,editor:{type:'numberbox',options:{precision:2}}">Total</th>
                        
                        <th data-options="field:'reasonOfInvestment',width:200,editor:'text'">Reason Of Investment</th>
                        
                        <th data-options="field:'reasonOfReplacement',align:'right', width:100,editor:{type:'checkbox',options:{on:'1',off:'0'}}">Reason Of Replacement</th>
                        
                        <th data-options="field:'oldFAPId',width:200,
                            formatter:function(value,row){
                                return row.oldFAPName;
                            },
                            editor:{
                                type:'combobox',
                                options:{
                                    valueField:'id',
                                    textField:'name',
                                    url:'/${meta(name:'app.name')}/fap/jlist',
                                    
                                }
                        }">Old FAP</th>
                                                                                
                        <th data-options="field:'newCareTakerId',width:200,
                            formatter:function(value,row){
                                return row.newCareTakerName;
                            },
                            editor:{
                                type:'combobox',
                                options:{
                                    valueField:'id',
                                    textField:'name',
                                    url:'/${meta(name:'app.name')}/user/jlist',
                                    
                                }
                        }">New Care Taker</th>
                                    
                        
                        <th data-options="field:'remarks',width:200,editor:'text'">Remarks</th>
                        <th data-options="field:'fapId',hidden:true">fap</th>    
                        <th data-options="field:'budgetDetailId',hidden:true">budgetDetail</th>
                    
                    </tr>
                </thead>
            </table>
        </div> 

        
    
</div>

        <div id="tb-fAPDetails" style="height:auto">
            <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:false" onclick="fAPDetailsAppend()">Add</a>
            <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:false" onclick="fAPDetailsRemoveit()">Remove</a>
            <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-save',plain:false" onclick="fAPDetailsAccept()">Save</a>
            <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-reload',plain:false" onclick="fAPDetailsRefresh()">Refresh</a>
        </div>
            
        <r:script>     
            var editIndex = undefined;
            function fAPDetailsEndEditing(){
                if (editIndex == undefined){return true}
                if ($('#dg-fAPDetails').datagrid('validateRow', editIndex)){

        /*
                    //oldFAPName
                    var ed = $('#dg-fAPDetails').datagrid('getEditor', {index:editIndex,field:'oldFAPId'});
                    var oldFAPName = $(ed.target).combobox('getText');
                    $('#dg-fAPDetails').datagrid('getRows')[editIndex]['oldFAPName'] = oldFAPName;
                    
        
                    //newCareTakerName
                    var ed = $('#dg-fAPDetails').datagrid('getEditor', {index:editIndex,field:'newCareTakerId'});
                    var newCareTakerName = $(ed.target).combobox('getText');
                    $('#dg-fAPDetails').datagrid('getRows')[editIndex]['newCareTakerName'] = newCareTakerName;
                    
        
                    //budgetName
                    var ed = $('#dg-fAPDetails').datagrid('getEditor', {index:editIndex,field:'budgetId'});
                    var budgetName = $(ed.target).combobox('getText');
                    $('#dg-fAPDetails').datagrid('getRows')[editIndex]['budgetName'] = budgetName;
                    */
        

                    $('#dg-fAPDetails').datagrid('endEdit', editIndex);
                    var row = $('#dg-fAPDetails').datagrid('getRows')[editIndex]
                    $.ajax({
                      type: "POST",
                      url: "/${meta(name:'app.name')}/FAPDetail/jsave",
                      data: row,
                      success: function(data){ 
                          if(!data.success)
                          {
                            alert(data.messages.errors[0].message)
                          }
                      },
                      dataType: 'json'
                    });
                    editIndex = undefined;
                    return true;
                } else {
                    return false;
                }
            }
            function fAPDetailsOnClickRow(index){
                if (editIndex != index){
                    if (fAPDetailsEndEditing()){
                        $('#dg-fAPDetails').datagrid('selectRow', index)
                                .datagrid('beginEdit', index);
                        editIndex = index;
                    } else {
                        $('#dg-fAPDetails').datagrid('selectRow', editIndex);
                    }
                }
            }
            function fAPDetailsAppend(){
                if (fAPDetailsEndEditing()){
                    $('#searchBudget').modal('show');

                    var country =  "${FAPInstance.country}";
                    var currencyCode = "${FAPInstance.currency1?.code}";
                
                    $('#budgetContent').DataTable().destroy();
                
                    table = $('#budgetContent').DataTable({
                        "bProcessing": true,
                        "sAjaxSource": "/${meta(name:'app.name')}/budgetDetail/jlist",
                        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                          
                          aoData.push( { "name": "searchBudget","value" : "true"});
                          aoData.push( { "name": "countryBudget", "value": country } );
                          aoData.push( { "name": "currencyCode", "value": currencyCode } );
                          

                          oSettings.jqXHR = $.ajax( {
                            "dataType": 'json',
                            "type": "POST",
                            "url": sSource,
                            "data": aoData,
                            "success": fnCallback
                          } );
                        },
                        "aoColumns": [
                          { "mData": "categoryAssetName",
                            "fnRender": function(obj) {
                                var str = obj.aData.categoryAssetName;
                                return str;
                            },
                            sDefaultContent: "n/a" ,
                            
                          },
                          { "mData": "budgetNumber",
                            "fnRender": function(obj) {
                                var str = obj.aData.budgetNumber;
                                return str;
                            },
                            sDefaultContent: "n/a" ,
                            
                          },
                          { "mData": "country",
                            "fnRender": function(obj) {
                                var str = obj.aData.country;
                                return str;
                            },
                            sDefaultContent: "n/a" ,
                            
                          },
                          { "mData": "createdBy",
                            "fnRender": function(obj) {
                                var str = obj.aData.createdBy;
                                return str;
                            },
                            sDefaultContent: "n/a" ,
                            
                          },
                          { "mData": "qty",
                            "fnRender": function(obj) {
                                var str = obj.aData.qty;
                                return str;
                            },
                            sDefaultContent: "n/a" ,
                            
                          },
                          { "mData": "uom",
                            "fnRender": function(obj) {
                                var str = obj.aData.uom;
                                return str;
                            },
                            sDefaultContent: "n/a" ,
                            
                          },
                          { "mData": "currency1",
                            "fnRender": function(obj) {
                                var str = obj.aData.currency1;
                                return str;
                            },
                            sDefaultContent: "n/a" ,
                            
                          },
                          { "mData": "total",
                            "fnRender": function(obj) {
                                var str = obj.aData.total;
                                return str;
                            },
                            sDefaultContent: "n/a" ,
                            
                          },
                          { "mData": "total",
                            "fnRender": function(obj) {
                                var str = obj.aData.total;
                                return str;
                            },
                            sDefaultContent: "n/a" ,
                            
                          },
                          { "mData": "budgetDate",
                            "fnRender": function(obj) {
                                var str = obj.aData.budgetDate;
                                return str;
                            },
                            sDefaultContent: "n/a" ,
                            
                          },

                        ]

                    });                  
                }
            }
            function fAPDetailsRemoveit(){
                if (editIndex == undefined){return}
                if (!confirm('Are you sure to delete this record?')){ return }

                var row = $('#dg-fAPDetails').datagrid('getRows')[editIndex]
                console.log(row)

                $('#dg-fAPDetails').datagrid('cancelEdit', editIndex)
                        .datagrid('deleteRow', editIndex);

                $.ajax({
                  type: "POST",
                  url: "/${meta(name:'app.name')}/FAPDetail/jdelete/" + row['id'],
                  data: row,
                  success: function(data){ 
                      if(!data.success)
                      {
                            alert(data.messages)
                      }
                  },
                  dataType: 'json'
                });             
                editIndex = undefined;
            }
            function fAPDetailsAccept(){
                if (fAPDetailsEndEditing()){
                    $('#dg-fAPDetails').datagrid('acceptChanges');
                }
            }

            function fAPDetailsRefresh(){
                $('#dg-fAPDetails').datagrid('reload');
                editIndex = undefined;
            }
        </r:script>  
    

    

    

    

    

    

    

    

    


<%
}
%>
