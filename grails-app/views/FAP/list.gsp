
<%@ page import="com.smanggin.FAP" %>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'FAP.label', default: 'FAP')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>
	
<section id="list-FAP" class="first">
	<div class="row">
		<div class="col-lg-12">
			<div class="box box-primary">
				
				<div class="box-header with-border">
                  <h3 class="box-title"><g:message code="default.list.label" args="[entityName]" /></h3>
                </div><!--/.box-header with-border -->

				<div class="box-body table-responsive">	
					<table class="table table-bordered margin-top-medium">
						<thead>
							<tr>
							
								<g:sortableColumn property="country" title="${message(code: 'FAP.country.label', default: 'Country')}" />
							
								<g:sortableColumn property="createdBy" title="${message(code: 'FAP.createdBy.label', default: 'Created By')}" />
							
								<th><g:message code="FAP.currency1.label" default="Currency1" /></th>
							
								<th><g:message code="FAP.currency2.label" default="Currency2" /></th>
							
								<g:sortableColumn property="dateCreated" title="${message(code: 'FAP.dateCreated.label', default: 'Date Created')}" />
							
								<th><g:message code="FAP.department.label" default="Department" /></th>
							
							</tr>
						</thead>
						<tbody>
						<g:each in="${FAPInstanceList}" status="i" var="FAPInstance">
							<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
							
								<td><g:link action="show" id="${FAPInstance.id}">${fieldValue(bean: FAPInstance, field: "country")}</g:link></td>
							
								<td>${fieldValue(bean: FAPInstance, field: "createdBy")}</td>
							
								<td>${fieldValue(bean: FAPInstance, field: "currency1")}</td>
							
								<td>${fieldValue(bean: FAPInstance, field: "currency2")}</td>
							
								<td><g:formatDate date="${FAPInstance.dateCreated}" /></td>
							
								<td>${fieldValue(bean: FAPInstance, field: "department")}</td>
							
							</tr>
						</g:each>
						</tbody>
					</table>
				</div><!--/.box-body table-responsive -->

				<div class="box-footer clearfix">
					<bs:paginate total="${FAPInstanceTotal}" />
				</div><!--/.box-footer clearfix -->
			</div><!--/.box box-primary -->	
		</div><!--/.col-lg-12 -->	
	</div><!--/.row -->			

</section>

</body>

</html>
