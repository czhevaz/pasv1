
<%@ page import="com.smanggin.FAP" %>
<!doctype html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'FAP.label', default: 'FAP')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>

<section id="show-FAP" class="first">
	<div class="row">
		<div class="col-lg-12">
			<div class="box box-primary">
				<g:render template="headerTittle"/><!--/.box-header with-border -->	
                <div class="box-body table-responsive">
					<table class="table table-striped">
						<tbody>
						
							<tr class="prop">
								<td valign="top" class="name"><g:message code="FAP.number.label" default="Number" /></td>
								
								<td valign="top" class="value">${fieldValue(bean: FAPInstance, field: "number")}</td>
								
							</tr>
						
							<tr class="prop">
								<td valign="top" class="name"><g:message code="FAP.country.label" default="Country" /></td>
								
								<td valign="top" class="value">${fieldValue(bean: FAPInstance, field: "country")}</td>
								
							</tr>

							<tr class="prop">
								<td valign="top" class="name"><g:message code="FAP.transactionGroup.label" default="Transaction Group" /></td>
								
								<td valign="top" class="value"><g:link controller="transactionGroup" action="show" id="${FAPInstance?.transactionGroup?.id}">${FAPInstance?.transactionGroup?.encodeAsHTML()}</g:link></td>
								
							</tr>
						
							<tr class="prop">
								<td valign="top" class="name"><g:message code="FAP.fAPDate.label" default="FAP Date" /></td>
								
								<td valign="top" class="value"><g:formatDate date="${FAPInstance?.fapDate}" /></td>
								
							</tr>
						
									
							
							<tr class="prop">
								<td valign="top" class="name"><g:message code="FAP.currency1.label" default="Currency1" /></td>
								
								<td valign="top" class="value"><g:link controller="currency" action="show" id="${FAPInstance?.currency1?.id}">${FAPInstance?.currency1?.encodeAsHTML()}</g:link></td>
								
							</tr>
						
							<tr class="prop">
								<td valign="top" class="name"><g:message code="FAP.currency2.label" default="Currency2" /></td>
								
								<td valign="top" class="value"><g:link controller="currency" action="show" id="${FAPInstance?.currency2?.id}">${FAPInstance?.currency2?.encodeAsHTML()}</g:link></td>
								
							</tr>
							
							<tr class="prop">
								<td valign="top" class="name"><g:message code="FAP.rate.label" default="Rate" /></td>
								
								<td valign="top" class="value">${fieldValue(bean: FAPInstance, field: "rate")}</td>
								
							</tr>
						
							<tr class="prop">
								<td valign="top" class="name"><g:message code="FAP.department.label" default="Department" /></td>
								
								<td valign="top" class="value"><g:link controller="department" action="show" id="${FAPInstance?.department?.id}">${FAPInstance?.department?.name?.encodeAsHTML()}</g:link></td>
								
							</tr>
						
							<tr class="prop">
								<td valign="top" class="name"><g:message code="FAP.requestor.label" default="Requestor" /></td>
								
								<td valign="top" class="value"><g:link controller="user" action="show" id="${FAPInstance?.requestor?.id}">${FAPInstance?.requestor?.encodeAsHTML()}</g:link></td>
								
							</tr>
						
							<tr class="prop">
								<td valign="top" class="name"><g:message code="FAP.state.label" default="State" /></td>
								
								<td valign="top" class="value">${fieldValue(bean: FAPInstance, field: "state")}</td>
								
							</tr>
						
							<tr class="prop">
								<td valign="top" class="name"><g:message code="FAP.createdBy.label" default="Created By" /></td>
								
								<td valign="top" class="value">${fieldValue(bean: FAPInstance, field: "createdBy")}</td>
								
							</tr>

							<tr class="prop">
								<td valign="top" class="name"><g:message code="FAP.dateCreated.label" default="Date Created" /></td>
								
								<td valign="top" class="value"><g:formatDate date="${FAPInstance?.dateCreated}" /></td>
								
							</tr>
						
						
							<tr class="prop">
								<td valign="top" class="name"><g:message code="FAP.updatedBy.label" default="Updated By" /></td>
								
								<td valign="top" class="value">${fieldValue(bean: FAPInstance, field: "updatedBy")}</td>
								
							</tr>

								<tr class="prop">
								<td valign="top" class="name"><g:message code="FAP.lastUpdated.label" default="Last Updated" /></td>
								
								<td valign="top" class="value"><g:formatDate date="${FAPInstance?.lastUpdated}" /></td>
								
							</tr>
						
						</tbody>
					</table>
				</div><!--/.row -->
				<div class="box-footer clearfix">
						<g:form method="post" class="form-horizontal" >
						<g:hiddenField name="id" value="${FAPInstance?.id}" />
						<g:hiddenField name="version" value="${FAPInstance?.version}" />
						<g:hiddenField name="updatedBy" value="${session.user}"/>
						<g:hiddenField id ="rejectNotes" name="rejectNotes" value="${FAPInstance?.rejectNotes}" />

						<div class="form-actions">
					
							<g:if test="${FAPInstance?.state=='Draft' }">
								<g:if test="${FAPInstance?.createdBy == session.user}">
									<g:actionSubmit class="btn btn-primary btn-sm" action="actionWaitingApprove" value="${message(code: 'default.button.approve.label', default: 'Send To Approver')}" />
									
								</g:if>	
							</g:if>
							<g:if test="${FAPInstance?.state=='Waiting Approval'}">
								<g:if test="${FAPInstance?.mustApprovedBy == session.user}">
									<g:actionSubmit class="btn btn-primary btn-sm" action="actionApprove" value="${message(code: 'default.button.approve.label', default: 'Approve')}" />
									
									<g:actionSubmit id="reject" class="btn btn-primary btn-sm" action="actionReject" value="${message(code: 'default.button.rejected.label', default: 'Rejected')}" />

								</g:if>	
							</g:if>
							<g:if test="${FAPInstance?.state=='Approved'}">
								<g:if test="${FAPInstance?.mustApprovedBy == session.user}">
									<g:actionSubmit id="void" class="btn btn-primary btn-sm" action="actionVoid" value="${message(code: 'default.button.void.label', default: 'Void')}" />
								</g:if>	
							</g:if>
						</div>
					</g:form>	
				</div><!--/.box-footer clearfix -->
			</div><!--/.box-body table-responsive -->

			<g:render template="detail"/> 
			<g:render template="searchBudgetModal"/>
		</div><!--/.box box-primary -->
	</div><!--/.row -->
</section>

</body>

</html>
