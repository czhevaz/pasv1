package com.smanggin

import java.text.SimpleDateFormat
import org.codehaus.groovy.grails.commons.ApplicationHolder


/**
 * GlobalService
 * A service class encapsulates the core business logic of a Grails application
 */
class GlobalService {
    def emailService
    def grailsApplication = ApplicationHolder.application

    static transactional = true

     /**
    getApprovals
    **/  

    def getApprovals(purchaseOrder){
       
    	def appDetail = ApprovalDetail.createCriteria().list(){
    		country{
               eq('name',purchaseOrder.country)     
            }

            if(purchaseOrder?.transactionGroup?.transactionType?.code in ['POMS','POPF','PONP']){
                eq('lob',purchaseOrder.lob)
                eq('brand',purchaseOrder.brand)    
            }
            
            transactionType{
                eq('code',purchaseOrder?.transactionGroup?.transactionType?.code)
            }
            
            creator{
                eq('login',purchaseOrder?.createdBy)
            }

            eq('inActive',false)
            
    	}

    	return appDetail	
    }

	def getApprovalBySeq(transaction,noseq){
    	def appDetail = ApprovalDetail.createCriteria().list(){
    		country{
               eq('name',transaction.country)     
            }

            eq('transactionType',transaction?.transactionGroup?.transactionType)

            if(transaction?.transactionGroup?.transactionType?.code in ['POMS','POPF','PONP']){
                eq('lob',transaction.lob)
                eq('brand',transaction.brand)
            }

            creator{
                eq('login',transaction?.createdBy)
            }
            
           ne('inActive',true) 
           eq('noSeq',noseq?.toLong())
    	}

        println "firstApprove" +appDetail
    	return appDetail	
    }    

    def getPOApprovalSeq(purchaseOrder,userLogin){
    	def poApprover = PurchaseOrderApprover.findByPurchaseOrderAndApprover(purchaseOrder,userLogin)
    	return poApprover?.noSeq
    }

    def getPOApproverBySeq(purchaseOrder,noSeq){
    	def poApprover = PurchaseOrderApprover.findByPurchaseOrderAndNoSeq(purchaseOrder,noSeq)
    	return poApprover?.approver
    }

    def getNextApprover(purchaseOrder,userLogin){
    	def seq = getPOApprovalSeq(purchaseOrder,userLogin)
    	if(seq){
    		def nextseq = seq +1
    		def approver = getPOApproverBySeq(purchaseOrder,nextseq)
    		return approver
    	}else {
    		return null
    	}   

    }


    /**
    RFP
    **/
    def getRfpApprovalSeq(rfp,userLogin){
        def rfpApprover = RfpApprover.findByRfpAndApprover(rfp,userLogin)
        return rfpApprover?.noSeq
    }

    def getRfpApproverBySeq(rfp,noSeq){
        def rfpApprover = RfpApprover.findByRfpAndNoSeq(rfp,noSeq)
        return rfpApprover?.approver
    }

    def getNextApproverRfp(rfp,userLogin){
        def seq = getRfpApprovalSeq(rfp,userLogin)
        if(seq){
            def nextseq = seq +1
            def approver = getRfpApproverBySeq(rfp,nextseq)
            return approver
        }else {
            return null
        }   
    }

    /**
    approvalStatus
    **/

    def approvalStatus(val) {
	    def result
	    switch (val) {
	        case 1:
	            result = 'Approved'
	            break
	        case 2:
	            result = 'Rejected'
	            break
            case 3:  
                result = 'Void'
                break  
	        default:
	            result = 'Not Approved'
	            break
	    }    
	   return result
	}

    def updateIsNewNotif(notifId){
        def notif = Notif.get(notifId)
        notif.isNew=false
        notif.save()
    }

    def filterDate(startDate,endDate){
        def vd= []
        Calendar cal = Calendar.getInstance();
           cal.setTime(startDate);
           cal.set(Calendar.HOUR_OF_DAY, 0);
           cal.set(Calendar.MINUTE, 0);
           cal.set(Calendar.SECOND, 0);
           Date start = cal.getTime();

        Calendar calx = Calendar.getInstance();
           calx.setTime(endDate);
           calx.set(Calendar.HOUR_OF_DAY, 23);
           calx.set(Calendar.MINUTE, 59);
           calx.set(Calendar.SECOND, 59);
           Date end = calx.getTime();
        return [start:start,end:end] 
    }


    def getAllPoMustApproved(){
        def po = PurchaseOrder.createCriteria().list(){
            eq('state','Waiting Approval')
            projections{
                property('mustApprovedBy')
            }
        }

        return po.unique()
    }

    def stringToDate(String date,String format){
        Date output = new Date()
      //SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy")
          try {
              output= new Date().parse(format, date)
          } 
          catch (Exception e) {
            println e
              // TODO Auto-generated catch block
              //e.printStackTrace()
          }
          return output

    }


    /*bulan*/

    def monthInt(monthName){
        
        Date date = new SimpleDateFormat("MMMM").parse(monthName)
        println "date" +date
        Calendar cal = Calendar.getInstance();
        cal.setTime(date)
        println(cal.get(Calendar.MONTH))

        return cal.get(Calendar.MONTH)
    }

    def getCurrentYear(){
        int year = Calendar.getInstance().get(Calendar.YEAR);
        return year    
    }

     /**
    yearList
    **/  
    def yearList(domainInstance,grailsApplication,country){
        def domainClassName = "com.smanggin."+domainInstance
        def domainClassInstance = grailsApplication.getDomainClass(domainClassName).clazz 

        def years = domainClassInstance.createCriteria().list(){
            if(domainInstance == 'PurchaseOrder' || domainInstance == 'Rfp'){
                println "country "  + country.name
                eq('country',country.name)
                
            }else{
                eq('country',country)
            }

            projections{
                groupProperty('year')
            }
        }
        
        

        return years.sort{ it }

    }   

     /**
    Country List
    **/  
    /* Country List*/
    def countryList(){
        
        def country = Country.createCriteria().list(){
            if(session.isAdmin != 'Yes'){
                eq('name',session.country)
            }
        }

        return country
    }

     /**
    supplierFindALLByCountry
    **/  
    def supplierFindALLByCountry(country){
       def c = Supplier.createCriteria()
        def list = []
        list.push('Indonesia')
        list.push(country)
        list.unique()
        
        def results = c.list {
           countryOwnerID{
                'in'('name',list)
           }
        }
        
        return results
    }

     /**
    trgroupFindALLByCountry
    **/  
    def trgroupFindALLByCountry(country){
        def trGroup = TransactionGroup.findAllByCountry(country)
        return trGroup
    }

     /**
    getAllPOAndRFPWaitingApprove
    **/  
    def getAllPOAndRFPWaitingApprove(){
        def approverPO = PurchaseOrder.createCriteria().list(){
            eq('state','Waiting Approval')
            projections{
               groupProperty('mustApprovedBy') 
            }
        }

        def approverRfp = Rfp.createCriteria().list(){
            eq('state','Waiting Approval')
            projections{
               groupProperty('mustApprovedBy') 
            }
        }
        
        def listAllApprover=[]
        if(approverPO.size() > 0){
            approverPO.each{ po ->
                listAllApprover.push(po)
            }
          
        }
        
        if(approverRfp.size() > 0){
            approverRfp.each{ rfp ->
                listAllApprover.push(rfp)
            }
            
        }

        listAllApprover.unique()
        println listAllApprover
        def list=[]
        listAllApprover.each{ approver ->
            
            def user = User.findByLogin(approver)
            
            def pos = PurchaseOrder.createCriteria().list(){
                 eq('state','Waiting Approval')
                 eq('mustApprovedBy',approver)
            }

            def listPo=[]
            pos.each{ po->
                listPo.push(po.number)
            }
            
            def rfps = Rfp.createCriteria().list(){
                 eq('state','Waiting Approval')
                 eq('mustApprovedBy',approver)
            }

            def listRfp=[]
            rfps.each{ rfp->
                listRfp.push(rfp.number)
            }

            def mapPO = [:]
            mapPO.put('approver',user?.email) 
            mapPO.put('poList',listPo)
            mapPO.put('rfpList',listRfp)

            list.push(mapPO)
        }

        return list
    }


     /**
    populateEmail
    **/  
    def populateEmail(){
        def data = getAllPOAndRFPWaitingApprove()

        if(data.size() > 0 ){
            data.each{ email ->
                def msgDetail = AppSetting.valueDefault('order_approve_email','default [order_approve_email] message, please set at AppSetting')
                def subject = 'PO & RFP Waiting For Approval'
                def receiver= email.approver
                msgDetail =  msgDetail.replaceAll( "\\[po_list\\]", email.poList?.join(','))
                msgDetail =  msgDetail.replaceAll( "\\[rfp_list\\]", email.rfpList?.join(','))    
                def sender = grailsApplication.config.grails.mail.username
                def map = [:]
                map.put('subject',subject)
                map.put('receiver',receiver)
                map.put('message',msgDetail)

                Outbox.newEmail( subject,  msgDetail,  sender,  receiver, null, null, null, null, null)
                //emailService.sendEmailByApprover(map).send()
                //println map                
            }
        }
    }

     /**
    SaveNotif
    **/  
    def saveNotif(objectInstance,forUser,assignBy){
        

        def notif = new Notif()
        notif.docName = objectInstance.getClass().getSimpleName() 
        notif.docId = objectInstance.id
        notif.docNumber = objectInstance.number
        notif.state = objectInstance.state
        notif.forUser = forUser
        notif.createdBy = assignBy
        notif.isNew = true
        notif.save()
        
    }/* SaveNotif */

    /**
    get Next Approval
    **/
    def getObjectApprovalSeq(domainApproverClass,objectInstance,userLogin){
        def className = objectInstance.getClass().getSimpleName() 
        String output = className.substring(0, 1).toLowerCase() + className.substring(1);
        def domainClassInstance = getDomainClass(domainApproverClass)
        def objectApprover = domainClassInstance.createCriteria().list(){
            eq(output,objectInstance)
            eq('approver',userLogin)
        }   

        return objectApprover[0]?.noSeq
        
    }

    def getObjectApproverBySeq(domainApproverClass,objectInstance,noSeq){
        def className = objectInstance.getClass().getSimpleName() 
        String output = className.substring(0, 1).toLowerCase() + className.substring(1);
        def domainClassInstance = getDomainClass(domainApproverClass)
        def objectApprover = domainClassInstance.createCriteria().list(){
            eq(output,objectInstance)
            eq('noSeq',noSeq)
        }   

        return objectApprover[0]?.approver
    }

    def getNextApproverObject(domainApproverClass,objectInstance,userLogin){
        def seq = getObjectApprovalSeq(domainApproverClass,objectInstance,userLogin)
        if(seq){
            def nextseq = seq +1
            def approver = getObjectApproverBySeq(domainApproverClass,objectInstance,nextseq)
            println approver
            return approver
        }else {
            return null
        }   
    }

    def getDomainClass(domainClassName){
        //println "domainClassName > " +domainClassName
        def domainClassInstance = grailsApplication.getDomainClass(domainClassName)?.clazz 
        return domainClassInstance   
    }
    
       
    def getInformation(position){
        
        def date = new Date()
        def filterDate = filterDate(date,date)
        def informationPeriodList = InformationPeriod.createCriteria().list(){
            le('startDate',filterDate.start)
            ge('endDate',filterDate.end)
            eq('position',position)
        }
        
        println " informationPeriodList " + informationPeriodList.id

        def attachment 
        if(informationPeriodList.size() > 0){
            attachment = Attachment.createCriteria().list(){
                eq('className','InformationPeriod')
                
                'in'('classId',informationPeriodList.id)
                
                
            }   
            
        }
        

        println "attachment "+attachment 
        return attachment
    }
}

