package com.smanggin

/**
 * FAP
 * A domain class describes the data object and it's mapping to the database
 */
class FAP {
	def grailsApplication
	/* Default (injected) attributes of GORM */
//	Long	id
//	Long	version
	
	/* Automatic timestamping of GORM */
	Date	dateCreated
	Date	lastUpdated
	
	String 	createdBy 
	String 	updatedBy
	String	country
	TransactionGroup transactionGroup
	Date fapDate
	String number
	Currency currency1 // currency Local
	Currency currency2 // currency convert
	Float   rate
	String  state
	Integer year
	Integer month

	Department department
	User requestor

	RateDetail rateDetail

	String rejectNotes
	Date dateReject
	String rejectedBy

	String mustApprovedBy

	
	static	belongsTo	= [TransactionGroup,Currency]	// tells GORM to cascade commands: e.g., delete this object if the "parent" is deleted.
//	static	hasOne		= []	// tells GORM to associate another domain object as an owner in a 1-1 mapping
	static	hasMany		= [fapDetails:FAPDetail,fAPApprovers:FAPApprover]	// tells GORM to associate other domain objects for a 1-n or n-m mapping
	static	mappedBy	= [fapDetails:'fap']	// specifies which property should be used in a mapping 
	
    static	mapping = {
    	table 'T_PAS_FAP'
    	version true
    	year formula: 'YEAR(fap_date)'
    	month formula: 'MONTH(fap_date)'
    }
    
	static	constraints = {
		month nullable:true
		year nullable:true
		rateDetail nullable:true
		updatedBy nullable:true
		rejectNotes nullable:true
		dateReject nullable:true
		rejectedBy nullable:true
		mustApprovedBy nullable:true
    }
	/**
		before Insert
	**/
	def beforeInsert(){
		//Integer count= PurchaseOrder.countByTransactionGroup(transactionGroup)+1
		
		def bgt = FAP.createCriteria().list(){
            order("dateCreated", "desc")
            eq('transactionGroup',transactionGroup)
            
        }

		Integer count =1
		Integer width= transactionGroup.width
		
		if(bgt){
        	def lastnumber = bgt[0].number.reverse().take(width).reverse().replaceFirst('^0+(?!$)', '')
        	count = lastnumber.toInteger() + 1
        }

		String  prefix = transactionGroup.prefix

		
		String c = sprintf("%0${width}d",count)
		Date now = new Date()
		number = prefix+now.format(transactionGroup.format)+c
	}
}
