package com.smanggin

/**
 * BudgetBalance
 * A domain class describes the data object and it's mapping to the database
 */
class BudgetBalance {

	Date	dateCreated
	Date	lastUpdated
	String country
	Budget budget
	String  description
	Currency currency1
	Float balance1
	Float balance2

	static	belongsTo	= [Budget,Currency]	// tells GORM to cascade commands: e.g., delete this object if the "parent" is deleted.
    static	mapping = {
    	table 'T_PAS_Budget_Balance'
    	version true
    }
    
	static	constraints = {
    }
	
	/*
	 * Methods of the Domain Class
	 */
//	@Override	// Override toString for a nicer / more descriptive UI 
//	public String toString() {
//		return "${name}";
//	}
}
