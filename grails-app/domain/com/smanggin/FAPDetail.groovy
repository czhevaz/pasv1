package com.smanggin

/**
 * FAPDetail
 * A domain class describes the data object and it's mapping to the database
 */
class FAPDetail {

	/* Automatic timestamping of GORM */
	Date		dateCreated
	Date		lastUpdated
	
	FAP         fap
	String 		createdBy 
	String 		updatedBy
	BudgetDetail budgetDetail
	Float   	qty
	Float   	unitprice
	Float   	total
	String   	reasonOfInvestment
	Boolean  	reasonOfReplacement // Broken / Mutation 
	FAP      	oldFAP
	User     	newCareTaker 
	String 		remarks

	
	static	belongsTo	= [BudgetDetail,FAP,User]	// tells GORM to cascade commands: e.g., delete this object if the "parent" is deleted.
//	static	hasOne		= []	// tells GORM to associate another domain object as an owner in a 1-1 mapping
//	static	hasMany		= []	// tells GORM to associate other domain objects for a 1-n or n-m mapping
//	static	mappedBy	= []	// specifies which property should be used in a mapping 
	
    static	mapping = {
    	table 'T_PAS_FAP_Detail'
    	version true
    }
    
	static	constraints = {
		reasonOfInvestment nullable:true
		updatedBy nullable:true
		oldFAP nullable:true
		newCareTaker nullable:true
		remarks nullable:true
    }
	

}
