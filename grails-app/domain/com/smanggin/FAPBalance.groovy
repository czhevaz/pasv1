package com.smanggin

/**
 * FAPBalance
 * A domain class describes the data object and it's mapping to the database
 */
class FAPBalance {

	Date	dateCreated
	Date	lastUpdated
	String country
	FAP fap
	String  description
	Currency currency1
	Float balance1
	Float balance2

	
	static	belongsTo	= [FAP,Currency]	// tells GORM to cascade commands: e.g., delete this object if the "parent" is deleted.
//	static	hasOne		= []	// tells GORM to associate another domain object as an owner in a 1-1 mapping
//	static	hasMany		= []	// tells GORM to associate other domain objects for a 1-n or n-m mapping
//	static	mappedBy	= []	// specifies which property should be used in a mapping 
	
    static	mapping = {
    	table 'T_PAS_FAP_Balance'
    	version true
    }
    
	static	constraints = {
    }
	
	
}
