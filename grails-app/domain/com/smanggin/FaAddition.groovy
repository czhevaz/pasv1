package com.smanggin

/**
 * FaAddition
 * A domain class describes the data object and it's mapping to the database
 */
class FaAddition {

	def grailsApplication
	/* Default (injected) attributes of GORM */
//	Long	id
//	Long	version
	
	/* Automatic timestamping of GORM */
	Date	dateCreated
	Date	lastUpdated
	
	String 	createdBy 
	String 	updatedBy
	String	country
	TransactionGroup transactionGroup
	Date faAddDate
	String number
	PurchaseOrder purchaseOrder
	String state
	Integer year
	Integer month

	static	belongsTo	= [PurchaseOrder ,TransactionGroup]	// tells GORM to cascade commands: e.g., delete this object if the "parent" is deleted.
//	static	hasOne		= []	// tells GORM to associate another domain object as an owner in a 1-1 mapping
	static	hasMany		= [faAdditionDetails:FaAdditionDetail,faAdditionApprovers:FaAdditionApprover]	// tells GORM to associate other domain objects for a 1-n or n-m mapping
//	static	mappedBy	= []	// specifies which property should be used in a mapping 
	
    static	mapping = {
    	table 'T_PAS_FA_Addition'
    	version true
    	year formula: 'YEAR(fa_add_date)'
    	month formula: 'MONTH(fa_add_date)'
    }
    
	static	constraints = {
		month nullable:true
		year nullable:true
		updatedBy nullable:true
    }
	
		
	def beforeInsert(){
		//Integer count= PurchaseOrder.countByTransactionGroup(transactionGroup)+1
		
		def bgt = FaAddition.createCriteria().list(){
            order("dateCreated", "desc")
            eq('transactionGroup',transactionGroup)
            
        }

		Integer count =1
		Integer width= transactionGroup.width
		
		if(bgt){
        	def lastnumber = bgt[0].number.reverse().take(width).reverse().replaceFirst('^0+(?!$)', '')
        	count = lastnumber.toInteger() + 1
        }

		String  prefix = transactionGroup.prefix

		
		String c = sprintf("%0${width}d",count)
		Date now = new Date()
		number = prefix+now.format(transactionGroup.format)+c
	}
}
