package com.smanggin



class SendMailApprovalJob {
    def globalService
    static triggers = {
      cron name:'cronTrigger', startDelay:10000, cronExpression: '59 59 23 * * ?'
    }

    def execute() {
    	globalService.populateEmail()
        // execute job
        println " SendMailApprovalJob "
    }
}
